#define _GNU_SOURCE
#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include "header_parser.h"

#define MODE_NONE    0
#define MODE_CORRECT 1
#define MODE_ERROR   2

int main(int argc, char *argv[]){
	int c, fd, retval;
	off_t len;
	unsigned int mac[6];
	uint8_t *start, *header, t, version_major, version_minor;
	uint32_t load_address;
	int version_major_set, version_minor_set, version_set;
	int offset_mode, length_mode, crc_mode, mac_mode, load_address_mode;
	offset_mode = length_mode = crc_mode = mac_mode = load_address_mode = MODE_NONE;
	version_set = version_major_set = version_minor_set = 0;
	opterr = 0;
	printf("\e[1m\e[32mCRC Calculator v1.2\e[0m\n");
	while((c = getopt(argc, argv, "hoOlLcCMm:v:V:Aa:")) != -1){
		if(c == 'h'){
			printf("HELP!\n");
			return(0);
		}else if(c == 'o'){
			offset_mode = MODE_CORRECT;
		}else if(c == 'O'){
			offset_mode = MODE_ERROR;
		}else if(c == 'l'){
			length_mode = MODE_CORRECT;
		}else if(c == 'L'){
			length_mode = MODE_ERROR;
		}else if(c == 'c'){
			crc_mode = MODE_CORRECT;
		}else if(c == 'C'){
			crc_mode = MODE_ERROR;
		}else if(c == 'M'){
			mac_mode = MODE_ERROR;
		}else if(c == 'm'){
			mac_mode = MODE_CORRECT;
			if(sscanf(optarg, "%02X:%02X:%02X:%02X:%02X:%02X", &mac[0], &mac[1], &mac[2], &mac[3], &mac[4], &mac[5]) != 6){
				fprintf(stderr, "\e[31mError: invalid MAC address\e[0m\n");
				return(1);
			}
		}else if(c == 'V'){
			version_major_set = 1;
			version_major = strtoul(optarg, NULL, 0);
		}else if(c == 'v'){
			version_minor_set = 1;
			version_minor = strtoul(optarg, NULL, 0);
		}else if(c == 'A'){
			load_address_mode = MODE_ERROR;
		}else if(c == 'a'){
			load_address_mode = MODE_CORRECT;
			load_address = strtoull(optarg, NULL, 0);
		}else{
			fprintf(stderr, "\e[31mError: invalid parameters\e[0m\n");
			return(1);
		}
	}
	if(argv[optind] == NULL){
		fprintf(stderr, "\e[31mError: missing file name\e[0m\n");
		return(1);
	}
	fd = open(argv[optind], O_RDWR);
	if(fd < 0){
		fprintf(stderr, "\e[31mError: open: %s\e[0m\n", strerror(errno));
		return(1);
	}
	len = lseek(fd, 0, SEEK_END);
	if(len == (off_t)-1){
		close(fd);
		fprintf(stderr, "\e[31mError: seek: %s\e[0m\n", strerror(errno));
		return(1);
	}
	if(len < 22){
		close(fd);
		fprintf(stderr, "\e[31mError: file too small\e[0m\n");
		return(1);
	}
	start = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if(start == (void *)-1){
		close(fd);
		fprintf(stderr, "\e[31mError: mmap: %s\e[0m\n", strerror(errno));
		return(1);
	}
	retval = 0;
	printf("File length: %llu\n", (unsigned long long int)len);
	header = header_validate(start, start + len);
	if(header == NULL){
		retval = 1;
		fprintf(stderr, "\e[31mError: header not found\e[0m\n");
		goto ready;
	}
	printf("Found header at: %u\n", (unsigned int)(header - start));
	if(header_has(header, HEADER_TYPE))printf("File type: %s\n", header_value(header, HEADER_TYPE) == HEADER_TYPE_BOOTLOADER ? "bootloader" : "firmware");
	if(header_value(header, HEADER_TYPE) == HEADER_TYPE_BOOTLOADER){
		if(header_has(header, HEADER_BOOTLOADER_TYPE)){
			if(header_value(header, HEADER_BOOTLOADER_TYPE) == HEADER_BOOTLOADER_TYPE_DUMMY){
				printf("Bootloader type: dummy\n");
			}else if(header_value(header, HEADER_BOOTLOADER_TYPE) == HEADER_BOOTLOADER_TYPE_TFTP){
				printf("Bootloader type: tftp\n");
			}else{
				printf("Bootloader type: %02X\n", (unsigned int)header_value(header, HEADER_BOOTLOADER_TYPE));
			}
		}
	}
	if(header_has(header, HEADER_VERSION))printf("Header version: %u\n", (unsigned int)header_value(header, HEADER_VERSION));
	if(header_has(header, HEADER_ENDIAN))printf("Header endianness: %s\n", header_value(header, HEADER_ENDIAN) == HEADER_ENDIAN_LITTLE ? "little" : "big");
	if(header_has(header, HEADER_TARGET_PLATFORM))printf("Target platform: 0x%02X\n", (unsigned int)header_value(header, HEADER_TARGET_PLATFORM));
	if(header_has(header, HEADER_LOAD_ADDRESS)){
		if((load_address_mode == MODE_CORRECT) && (header_value(header, HEADER_LOAD_ADDRESS) != load_address)){
			printf("\e[1m\e[33mLoad address: 0x%08X", (unsigned int)header_value(header, HEADER_LOAD_ADDRESS));
			header_value_set(header, HEADER_LOAD_ADDRESS, load_address);
			printf("\e[32m - set: 0x%08X\e[0m\n", (unsigned int)header_value(header, HEADER_LOAD_ADDRESS));
		}else{
			printf("Load address: 0x%08X\n", (unsigned int)header_value(header, HEADER_LOAD_ADDRESS));
		}
	}else if(load_address_mode == MODE_ERROR){
		retval = 1;
		fprintf(stderr, "\e[31mLoad address missing\e[0m\n");
	}
	if(header_has(header, HEADER_VERSION_MAJOR) && version_major_set && (header_value(header, HEADER_VERSION_MAJOR) != version_major)){
		t = header_value(header, HEADER_VERSION_MAJOR);
		header_value_set(header, HEADER_VERSION_MAJOR, version_major);
		version_major = t;
		version_set = 1;
	}
	if(header_has(header, HEADER_VERSION_MINOR) && version_minor_set && (header_value(header, HEADER_VERSION_MINOR) != version_minor)){
		t = header_value(header, HEADER_VERSION_MINOR);
		header_value_set(header, HEADER_VERSION_MINOR, version_minor);
		version_minor = t;
		version_set = 1;
	}
	if(header_has(header, HEADER_VERSION_MAJOR) && header_has(header, HEADER_VERSION_MINOR)){
		if(version_set){
			printf("\e[1m\e[33mSoftware version: %u.%02u", (unsigned int)version_major, (unsigned int)version_minor);
			printf("\e[32m - set: %u.%02u\e[0m\n", (unsigned int)header_value(header, HEADER_VERSION_MAJOR), (unsigned int)header_value(header, HEADER_VERSION_MINOR));
		}else{
			printf("Software version: %u.%02u\n", (unsigned int)header_value(header, HEADER_VERSION_MAJOR), (unsigned int)header_value(header, HEADER_VERSION_MINOR));
		}
	}
	if(header_has(header, HEADER_OFFSET)){
		if((header_value(header, HEADER_OFFSET) != (header - start)) && (offset_mode == MODE_CORRECT)){
			printf("\e[1m\e[33mHeader offset: %u", (unsigned int)header_value(header, HEADER_OFFSET));
			header_value_set(header, HEADER_OFFSET, (header - start));
			printf("\e[32m - set: %u\e[0m\n", (unsigned int)header_value(header, HEADER_OFFSET));
		}else if((header_value(header, HEADER_OFFSET) != (header - start)) && (offset_mode == MODE_ERROR)){
			retval = 1;
			fprintf(stderr, "\e[31mHeader offset: %u - mismatch\e[0m\n", (unsigned int)header_value(header, HEADER_OFFSET));
		}else{
			printf("Header offset: %u\n", (unsigned int)header_value(header, HEADER_OFFSET));
		}
	}else if(offset_mode != MODE_NONE){
		retval = 1;
		fprintf(stderr, "\e[31mHeader offset missing\e[0m\n");
	}
	if(header_has(header, HEADER_NAME))printf("Program name: %s\n", header_value(header, HEADER_NAME));
	if(header_has(header, HEADER_SIZE)){
		if((len != header_value(header, HEADER_SIZE)) && (length_mode == MODE_CORRECT)){
			printf("\e[1m\e[33mProgram length: %llu", (unsigned long long int)header_value(header, HEADER_SIZE));
			header_value_set(header, HEADER_SIZE, len);
			printf("\e[32m - set: %llu\e[0m\n", (unsigned long long int)header_value(header, HEADER_SIZE));
		}else if((len != header_value(header, HEADER_SIZE)) && (length_mode == MODE_ERROR)){
			retval = 1;
			fprintf(stderr, "\e[31mProgram length: %llu - mismatch\e[0m\n", (unsigned long long int)header_value(header, HEADER_SIZE));
		}else{
			printf("Program length: %llu\n", (unsigned long long int)header_value(header, HEADER_SIZE));
		}
	}else if(length_mode != MODE_NONE){
		retval = 1;
		fprintf(stderr, "\e[31mProgram length missing\e[0m\n");
	}
	if(header_has(header, HEADER_IEEE_ADDRESS)){
		start = (uint8_t *)header_value(header, HEADER_IEEE_ADDRESS);
		if((mac_mode == MODE_CORRECT) && ((start[0] != mac[0]) || (start[1] != mac[1]) || (start[2] != mac[2]) || (start[3] != mac[3]) || (start[4] != mac[4]) || (start[5] != mac[5]))){
			printf("\e[1m\e[33mMAC address: %02X:%02X:%02X:%02X:%02X:%02X", (unsigned int)start[0], (unsigned int)start[1], (unsigned int)start[2], (unsigned int)start[3], (unsigned int)start[4], (unsigned int)start[5]);
			start[0] = mac[0]; start[1] = mac[1]; start[2] = mac[2]; start[3] = mac[3]; start[4] = mac[4]; start[5] = mac[5];
			printf("\e[32m - set: %02X:%02X:%02X:%02X:%02X:%02X\e[0m\n", (unsigned int)start[0], (unsigned int)start[1], (unsigned int)start[2], (unsigned int)start[3], (unsigned int)start[4], (unsigned int)start[5]);
		}else{
			printf("MAC address: %02X:%02X:%02X:%02X:%02X:%02X\n", (unsigned int)start[0], (unsigned int)start[1], (unsigned int)start[2], (unsigned int)start[3], (unsigned int)start[4], (unsigned int)start[5]);
		}
	}else if(mac_mode == MODE_ERROR){
		retval = 1;
		fprintf(stderr, "\e[31mMAC address missing\e[0m\n");
	}
	if(header_has(header, HEADER_DEFAULT_IP)){
		start = (uint8_t *)header_value(header, HEADER_DEFAULT_IP);
		printf("Default IP: %u.%u.%u.%u\n", (unsigned int)start[0], (unsigned int)start[1], (unsigned int)start[2], (unsigned int)start[3]);
	}
	/* CRC peaks olema viimane, et saaks üle arvutada kui muutusi oli */
	if(header_has(header, HEADER_CRC)){
		printf("File CRC: 0x%08X\n", (unsigned int)header_crc(header));
		if(!header_crc_ok(header) && (crc_mode == MODE_CORRECT)){
			printf("\e[1m\e[33mHeader CRC: 0x%08X", (unsigned int)header_value(header, HEADER_CRC));
			header_value_set(header, HEADER_CRC, header_crc(header));
			printf("\e[32m - set: 0x%08X\e[0m\n", (unsigned int)header_crc(header));
		}else if(!header_crc_ok(header) && (crc_mode == MODE_ERROR)){
			retval = 1;
			fprintf(stderr, "\e[31mHeader CRC: 0x%08X - mismatch\e[0m\n", (unsigned int)header_crc(header));
		}else{
			printf("Header CRC: 0x%08X\n", (unsigned int)header_value(header, HEADER_CRC));
		}
	}else if(crc_mode != MODE_NONE){
		retval = 1;
		fprintf(stderr, "\e[31mHeader CRC missing\e[0m\n");
	}
ready:
	munmap(start, len);
	close(fd);
	return(retval);
}

