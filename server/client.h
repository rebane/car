#ifndef _CLIENT_H_
#define _CLIENT_H_

void client_init(char *file);
int client_register(char *serial, int volatile *cancel_flag);
void client_unregister(char *serial, int volatile *cancel_flag);

#endif

