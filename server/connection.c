#include "connection.h"
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>
#include "protocol.h"
#include "bjson.h"
#include "client.h"
#include "incbin.h"

#define tohex(c) ((((c) < 10) && ((c) >= 0)) ? ((c) + '0') : ((c) - 10 + 'A'))
#define CONNECTION_READ_BUFFER_LEN    256
#define CONNECTION_COMMAND_BUFFER_LEN 1024
#define CONNECTION_TIMEOUT            900 // 15 min. (for firmware sending)

// INCLUDE_BINARY_FILE(firmware_start, firmware_end, "../firmware/device_car.bin", ".rodata");

extern char *command;

static int bjson_log(FILE *f, uint8_t *bjson, bjson_umax_t len);
static bjson_umax_t bjson_get_size(uint8_t *bjson);
static int bjson_print(FILE *f, uint8_t *bjson);
static int bjson_print_double(FILE *f, double value);
static int bjson_print_string(FILE *f, char *string, bjson_umax_t len);
static int bjson_print_binary(FILE *f, uint8_t *data, bjson_umax_t len);
static int bjson_print_array(FILE *f, uint8_t *bjson, bjson_umax_t len);
static int bjson_print_map(FILE *f, uint8_t *bjson, bjson_umax_t len);

static ssize_t write_wait(int fd, const void *buf, size_t count);

void connection_thread(void *_ptr, size_t _len){
	ssize_t i, l;
	int j, k, s;
	struct timeval tv;
	fd_set fds;
	uint8_t read_buffer[CONNECTION_READ_BUFFER_LEN];
	char serial_buffer[32];
	char command_buffer[CONNECTION_COMMAND_BUFFER_LEN];
	char ip_buffer[32];
	struct connection_packet_buffer packet_buffer;
	uint16_t packet_loc;
	volatile int cancel;
	FILE *p;
	time_t t;
	struct sockaddr addr;
	socklen_t sl;
	s = *(int *)_ptr;
	packet_loc = 0;
	cancel = 0;
	serial_buffer[0] = 0;
	p = NULL;
	t = time(NULL) + CONNECTION_TIMEOUT;
	printf("accept\n");
	while(1){
		if(cancel){
			printf("OOPS KILLED!!!\n");
			goto hangup;
		}
		FD_ZERO(&fds);
		FD_SET(s, &fds);
		tv.tv_sec = 0;
		tv.tv_usec = 1000000L;
		j = select(s + 1, &fds, NULL, NULL, &tv);
		if(j > 0){
			if(FD_ISSET(s, &fds)){
				l = read(s, read_buffer, CONNECTION_READ_BUFFER_LEN);
				if(l < 1)goto hangup;
				for(i = 0; i < l; i++){
					if(packet_loc < (PROTOCOL_HEADER_LEN + CONNECTION_PACKET_DATA_LEN))((uint8_t *)&packet_buffer)[packet_loc] = read_buffer[i];
					packet_loc++;
					if((packet_loc > 4) && (protocol_get_len(&packet_buffer.header) > CONNECTION_PACKET_DATA_LEN))goto hangup;
					if((packet_loc > 4) && (packet_loc >= (PROTOCOL_HEADER_LEN + protocol_get_len(&packet_buffer.header)))){
						if(p == NULL){
							if(protocol_get_command(&packet_buffer.header) != PROTOCOL_COMMAND_IDENT)goto hangup;
							k = protocol_get_status(&packet_buffer.header);
							if(!k || (k > 16))goto hangup;
							for(j = 0; j < k; j++){
								if(!(j & 1)){
									serial_buffer[j] = tohex((((uint8_t *)&packet_buffer.header.uid)[j / 2]) & 0xF);
								}else{
									serial_buffer[j] = tohex((((uint8_t *)&packet_buffer.header.uid)[j / 2]) >> 4);
								}
							}
							serial_buffer[j] = 0;
							sl = sizeof(addr);
							getpeername(s, &addr, &sl);
							ip_buffer[0] = 0;
							snprintf(command_buffer, CONNECTION_COMMAND_BUFFER_LEN - 1, "%s \"%s\" \"%s\" %u", command, serial_buffer, inet_ntop(AF_INET, &((struct sockaddr_in *)&addr)->sin_addr, ip_buffer, 32), (unsigned int)ntohs(((struct sockaddr_in *)&addr)->sin_port));
							command_buffer[CONNECTION_COMMAND_BUFFER_LEN - 1] = 0;
							fflush(stdin);
							fflush(stdout);
							fflush(stderr);
							printf("NEW CLIENT: %s, IP: %s, PORT: %u\n", serial_buffer, ip_buffer, (unsigned int)ntohs(((struct sockaddr_in *)&addr)->sin_port));
							if(!client_register(serial_buffer, &cancel))goto hangup;
							p = popen(command_buffer, "w");
							if(p == NULL)goto hangup;
						}
						if((protocol_get_command(&packet_buffer.header) == PROTOCOL_COMMAND_IDENT) || (protocol_get_command(&packet_buffer.header) == PROTOCOL_COMMAND_DATA)){
							if(protocol_get_len(&packet_buffer.header)){
								j = bjson_get_size(packet_buffer.data);
								if(j > CONNECTION_PACKET_DATA_LEN)goto hangup;
							}else{
								j = 0;
							}
							if(protocol_get_command(&packet_buffer.header) == PROTOCOL_COMMAND_IDENT){
								if(fprintf(p, "{\"command\":\"ident\",\"serial\":\"%s\",\"address\":\"%s\",\"port\":%u,\"bufferSize\":%u,\"data\":", serial_buffer, ip_buffer, (unsigned int)ntohs(((struct sockaddr_in *)&addr)->sin_port), (unsigned int)protocol_get_len2(&packet_buffer.header)) < 0)goto hangup;
							}else if(protocol_get_command(&packet_buffer.header) == PROTOCOL_COMMAND_DATA){
								if(fprintf(p, "{\"command\":\"data\",\"uid\":%llu,\"remain\":%u,\"fresh\":%s,\"data\":", (unsigned long long int)protocol_get_uid(&packet_buffer.header), (unsigned int)protocol_get_len2(&packet_buffer.header), protocol_get_status(&packet_buffer.header) ? "true" : "false") < 0)goto hangup;
							}
							if(j){
								// siin ei anna tala kuna kui on vigane json, siis tuleks ikkagi saata ACK et ära kustuks
								bjson_log(p, packet_buffer.data, j);
							}
							if(fprintf(p, "}\n") < 0)cancel = 1; /**/
						}
						if(fflush(p) == EOF)cancel = 1; /**/
						if(protocol_get_command(&packet_buffer.header) == PROTOCOL_COMMAND_IDENT){
							protocol_set_len(&packet_buffer.header, 0);
							protocol_set_command(&packet_buffer.header, PROTOCOL_COMMAND_DATE);
							protocol_set_uid(&packet_buffer.header, time(NULL));
							write_wait(s, &packet_buffer.header, PROTOCOL_HEADER_LEN);

/*							protocol_set_len(&packet_buffer.header, (&firmware_end - &firmware_start));
							protocol_set_command(&packet_buffer.header, PROTOCOL_COMMAND_FIRMWARE);
							protocol_set_uid(&packet_buffer.header, 666);
							write_wait(s, &packet_buffer.header, PROTOCOL_HEADER_LEN);
							write_wait(s, &firmware_start, (&firmware_end - &firmware_start));*/
						}else if(protocol_get_command(&packet_buffer.header) == PROTOCOL_COMMAND_DATA){
							protocol_set_len(&packet_buffer.header, 0);
							protocol_set_command(&packet_buffer.header, PROTOCOL_COMMAND_DATA_ACK);
							write_wait(s, &packet_buffer.header, PROTOCOL_HEADER_LEN);
						}else if(protocol_get_command(&packet_buffer.header) == PROTOCOL_COMMAND_FIRMWARE_ACK){
							printf("FIRMWARE ACK, STATUS: %u, UID: %llu\n", (unsigned int)protocol_get_status(&packet_buffer.header), (unsigned long long int)protocol_get_uid(&packet_buffer.header));
							protocol_set_len(&packet_buffer.header, 0);
							protocol_set_command(&packet_buffer.header, PROTOCOL_COMMAND_REBOOT);
							protocol_set_uid(&packet_buffer.header, 0);
							write_wait(s, &packet_buffer.header, PROTOCOL_HEADER_LEN);
						}
						t = time(NULL) + CONNECTION_TIMEOUT;
						packet_loc = 0;
					}
				}
			}
		}else if(j < 0){
			goto hangup;
		}
		if(time(NULL) > t)goto hangup;
	}
hangup:
	if(serial_buffer[0])client_unregister(serial_buffer, &cancel);
	if(p != NULL)pclose(p);
	shutdown(s, SHUT_RDWR);
	close(s);
	printf("close\n");
}

static ssize_t write_wait(int fd, const void *buf, size_t count){
	ssize_t ret;
	size_t loc;
	loc = 0;
	while(count){
		ret = write(fd, &((uint8_t *)buf)[loc], count - loc);
		if(ret < 0)return(ret);
		loc += ret;
		count -= ret;
	}
	return(loc);
}

static int bjson_log(FILE *f, uint8_t *bjson, bjson_umax_t len){
	if(bjson[0] == 32)return(bjson_print_array(f, &bjson[2], len - 2));
	if(bjson[0] == 33)return(bjson_print_array(f, &bjson[3], len - 3));
	if(bjson[0] == 34)return(bjson_print_array(f, &bjson[5], len - 5));
	if(bjson[0] == 35)return(bjson_print_array(f, &bjson[9], len - 9));
	if(bjson[0] == 36)return(bjson_print_map(f, &bjson[2], len - 2));
	if(bjson[0] == 37)return(bjson_print_map(f, &bjson[3], len - 3));
	if(bjson[0] == 38)return(bjson_print_map(f, &bjson[5], len - 5));
	if(bjson[0] == 39)return(bjson_print_map(f, &bjson[9], len - 9));
	return(bjson_print(f, bjson));
}

static bjson_umax_t bjson_get_size(uint8_t *bjson){
	if((bjson[0] == 0) || (bjson[0] == 1) || (bjson[0] == 2) || (bjson[0] == 3))return(1);
	if((bjson[0] == 4) || (bjson[0] == 8))return(2);
	if((bjson[0] == 5) || (bjson[0] == 9))return(3);
	if((bjson[0] == 6) || (bjson[0] == 10) || (bjson[0] == 12))return(5);
	if((bjson[0] == 7) || (bjson[0] == 11) || (bjson[0] == 13))return(9);
	if((bjson[0] == 16) || (bjson[0] == 20) || (bjson[0] == 32) || (bjson[0] == 36)){
		return(((((bjson_umax_t)bjson[1]) << 0)) + 2);
	}
	if((bjson[0] == 17) || (bjson[0] == 21) || (bjson[0] == 32) || (bjson[0] == 37)){
		return(((((bjson_umax_t)bjson[1]) << 0) | (((bjson_umax_t)bjson[2]) << 8)) + 3);
	}
	if((bjson[0] == 18) || (bjson[0] == 22) || (bjson[0] == 33) || (bjson[0] == 38)){
		return(((((bjson_umax_t)bjson[1]) << 0) | (((bjson_umax_t)bjson[2]) << 8) | (((bjson_umax_t)bjson[3]) << 16) | (((bjson_umax_t)bjson[4]) << 24)) + 5);
	}
	if((bjson[0] == 19) || (bjson[0] == 23) || (bjson[0] == 34) || (bjson[0] == 39)){
		return(((((bjson_umax_t)bjson[1]) << 0) | (((bjson_umax_t)bjson[2]) << 8) | (((bjson_umax_t)bjson[3]) << 16) | (((bjson_umax_t)bjson[4]) << 24) | (((bjson_umax_t)bjson[5]) << 32) | (((bjson_umax_t)bjson[6]) << 40) | (((bjson_umax_t)bjson[7]) << 48) | (((bjson_umax_t)bjson[8]) << 56)) + 9);
	}
	return(0);
}

static int bjson_print(FILE *f, uint8_t *bjson){
	if(bjson[0] == 0)return(fprintf(f, "null"));
	if(bjson[0] == 1)return(fprintf(f, "false"));
	if(bjson[0] == 2)return(fprintf(f, "\"\""));
	if(bjson[0] == 3)return(fprintf(f, "true"));
	if(bjson[0] == 4)return(fprintf(f, "%u", (unsigned int)bjson[1]));
	if(bjson[0] == 5)return(fprintf(f, "%u", (unsigned int)((((bjson_umax_t)bjson[1]) << 0) | (((bjson_umax_t)bjson[2]) << 8))));
	if(bjson[0] == 6)return(fprintf(f, "%llu", (unsigned long long int)((((bjson_umax_t)bjson[1]) << 0) | (((bjson_umax_t)bjson[2]) << 8) | (((bjson_umax_t)bjson[3]) << 16) | (((bjson_umax_t)bjson[4]) << 24))));
	if(bjson[0] == 7)return(fprintf(f, "%llu", (unsigned long long int)((((bjson_umax_t)bjson[1]) << 0) | (((bjson_umax_t)bjson[2]) << 8) | (((bjson_umax_t)bjson[3]) << 16) | (((bjson_umax_t)bjson[4]) << 24) | (((bjson_umax_t)bjson[5]) << 32) | (((bjson_umax_t)bjson[6]) << 40) | (((bjson_umax_t)bjson[7]) << 48) | (((bjson_umax_t)bjson[8]) << 56))));
	if(bjson[0] == 8)return(fprintf(f, "-%u", (unsigned int)bjson[1]));
	if(bjson[0] == 9)return(fprintf(f, "-%u", (unsigned int)((((bjson_umax_t)bjson[1]) << 0) | (((bjson_umax_t)bjson[2]) << 8))));
	if(bjson[0] == 10)return(fprintf(f, "-%llu", (unsigned long long int)((((bjson_umax_t)bjson[1]) << 0) | (((bjson_umax_t)bjson[2]) << 8) | (((bjson_umax_t)bjson[3]) << 16) | (((bjson_umax_t)bjson[4]) << 24))));
	if(bjson[0] == 11)return(fprintf(f, "-%llu", (unsigned long long int)((((bjson_umax_t)bjson[1]) << 0) | (((bjson_umax_t)bjson[2]) << 8) | (((bjson_umax_t)bjson[3]) << 16) | (((bjson_umax_t)bjson[4]) << 24) | (((bjson_umax_t)bjson[5]) << 32) | (((bjson_umax_t)bjson[6]) << 40) | (((bjson_umax_t)bjson[7]) << 48) | (((bjson_umax_t)bjson[8]) << 56))));
	if(bjson[0] == 12){
		if(sizeof(float) == 4)return(bjson_print_double(f, *(float *)&bjson[1]));
		if(sizeof(double) == 4)return(bjson_print_double(f, *(double *)&bjson[1]));
		return(fprintf(f, "0")); /* error */
	}
	if(bjson[0] == 13){
		if(sizeof(float) == 8)return(bjson_print_double(f, *(float *)&bjson[1]));
		if(sizeof(double) == 8)return(bjson_print_double(f, *(double *)&bjson[1]));
		return(fprintf(f, "0")); /* error */
	}
	if(bjson[0] == 16)return(bjson_print_string(f, (char *)&bjson[2], bjson_get_size(bjson) - 2));
	if(bjson[0] == 17)return(bjson_print_string(f, (char *)&bjson[3], bjson_get_size(bjson) - 3));
	if(bjson[0] == 18)return(bjson_print_string(f, (char *)&bjson[5], bjson_get_size(bjson) - 5));
	if(bjson[0] == 19)return(bjson_print_string(f, (char *)&bjson[9], bjson_get_size(bjson) - 9));
	if(bjson[0] == 20)return(bjson_print_binary(f, &bjson[2], bjson_get_size(bjson) - 2));
	if(bjson[0] == 21)return(bjson_print_binary(f, &bjson[3], bjson_get_size(bjson) - 3));
	if(bjson[0] == 22)return(bjson_print_binary(f, &bjson[5], bjson_get_size(bjson) - 5));
	if(bjson[0] == 23)return(bjson_print_binary(f, &bjson[9], bjson_get_size(bjson) - 9));
	return(-1);
}

static int bjson_print_double(FILE *f, double value){
	long double ld;
	ld = (long double)value;
	if(ld == (long double)((long long int)ld))return(fprintf(f, "%lld", (long long int)ld));
	return(fprintf(f, "%.10Lg", (long double)ld));
}

static int bjson_print_string(FILE *f, char *string, bjson_umax_t len){
	bjson_umax_t i;
	int r, s;
	s = fprintf(f, "\"");
	if(s < 0)return(s);
	for(i = 0; i < len; i++){
		r = fprintf(f, "%c", string[i]);
		if(r < 0)return(r);
		s += r;
	}
	r = fprintf(f, "\"");
	if(r < 0)return(r);
	return(s + r);
}

static int bjson_print_binary(FILE *f, uint8_t *data, bjson_umax_t len){
	bjson_umax_t i;
	int r, s;
	s = fprintf(f, "\"");
	if(s < 0)return(s);
	for(i = 0; i < len; i++){
		r = fprintf(f, "%02X", (unsigned int)data[i]);
		if(r < 0)return(r);
		s += r;
	}
	r = fprintf(f, "\"");
	if(r < 0)return(r);
	return(s + r);
}

static int bjson_print_array(FILE *f, uint8_t *bjson, bjson_umax_t len){
	bjson_umax_t i, l;
	int r, s;
	s = fprintf(f, "[");
	if(s < 0)return(s);
	for(i = 0; i < len; i += l){
		l = bjson_get_size(&bjson[i]);
		if((i + l) > len)return(-1);
		if(i){
			r = fprintf(f, ",");
			if(r < 0)return(r);
			s += r;
		}
		r = bjson_log(f, &bjson[i], l);
		if(r < 0)return(r);
		s += r;
	}
	r = fprintf(f, "]");
	if(r < 0)return(r);
	return(s + r);
}

static int bjson_print_map(FILE *f, uint8_t *bjson, bjson_umax_t len){
	bjson_umax_t i, l;
	int r, s;
	s = fprintf(f, "{");
	if(s < 0)return(s);
	for(i = 0; i < len; i += l){
		l = bjson_get_size(&bjson[i]);
		if((i + l) > len)return(-1);
		if(i){
			r = fprintf(f, ",");
			if(r < 0)return(r);
			s += r;
		}
		r = bjson_log(f, &bjson[i], l);
		if(r < 0)return(r);
		s += r;
		i += l;
		r = fprintf(f, ":");
		if(r < 0)return(r);
		s += r;
		l = bjson_get_size(&bjson[i]);
		if((i + l) > len)return(-1);
		r = bjson_log(f, &bjson[i], l);
		if(r < 0)return(r);
		s += r;
	}
	r = fprintf(f, "}");
	if(r < 0)return(r);
	return(s + r);
}

