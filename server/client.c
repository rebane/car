#include "client.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <pthread.h>
#include <errno.h>

extern char *program_name;

static struct client_struct{
	char *serial;
	int volatile *cancel_flag;
	int used;
}*client;

static int client_len;
static char *client_file;
static pthread_mutex_t client_mutex = PTHREAD_MUTEX_INITIALIZER;

static void client_reload(int signal);
void client_add(char *serial);

void client_init(char *file){
	client_file = file;
	client_len = 0;
	client = NULL;
	client_reload(SIGHUP);
	signal(SIGHUP, client_reload);
}

int client_register(char *serial, int volatile *cancel_flag){
	int i, retval;
	if((serial == NULL) || (cancel_flag == NULL))return(0);
	retval = 0;
	pthread_mutex_lock(&client_mutex);
	for(i = 0; i < client_len; i++){
		if((client[i].serial != NULL) && !strcmp(client[i].serial, serial)){
			if(client[i].used){
				if(client[i].cancel_flag != NULL)*client[i].cancel_flag = 1;
				client[i].cancel_flag = cancel_flag;
				retval = 1;
			}
			break;
		}
	}
	pthread_mutex_unlock(&client_mutex);
	return(retval);
}

void client_unregister(char *serial, int volatile *cancel_flag){
	int i;
	if((serial == NULL) || (cancel_flag == NULL))return;
	pthread_mutex_lock(&client_mutex);
	for(i = 0; i < client_len; i++){
		if((client[i].serial != NULL) && !strcmp(client[i].serial, serial)){
			if(client[i].used && (client[i].cancel_flag == cancel_flag))client[i].cancel_flag = NULL;
			break;
		}
	}
	pthread_mutex_unlock(&client_mutex);
}

static void client_reload(int signal){
	FILE *f;
	char buffer[256], *serial;
	int i, m;
	f = fopen(client_file, "r");
	if(f == NULL){
		fprintf(stderr, "%s: open: %s\n", program_name, strerror(errno));
		return;
	}
	pthread_mutex_lock(&client_mutex);
	for(i = 0; i < client_len; i++){
		if(client[i].used)client[i].used = 2;
	}
	while(!feof(f)){
		if(fgets(buffer, 256, f) == NULL)break;
		buffer[255] = 0;
		m = 0;
		for(i = 0; buffer[i]; i++){
			if(m == 0){
				if(buffer[i] == '#')break;
				if(!isspace(buffer[i])){
					serial = &buffer[i];
					m = 1;
				}
			}else if(m == 1){
				if(isspace(buffer[i]) || (buffer[i] == '#')){
					buffer[i] = 0;
					break;
				}
			}
		}
		if(m == 1)client_add(serial);
	}
	for(i = 0; i < client_len; i++){
		if(client[i].used != 1){
			if(client[i].serial != NULL){
				free(client[i].serial);
				client[i].serial = NULL;
			}
			if(client[i].cancel_flag != NULL){
				*client[i].cancel_flag = 1;
				client[i].cancel_flag = NULL;
			}
			client[i].used = 0;
		}
	}
	pthread_mutex_unlock(&client_mutex);
	printf("ACCEPTED CLIENTS:\n");
	for(i = 0, m = 1; i < client_len; i++){
		if(client[i].used == 1){
			printf("%i) %s\n", m++, client[i].serial);
		}
	}
	fclose(f);
}

void client_add(char *serial){
	int i;
	for(i = 0; i < client_len; i++){
		if((client[i].serial != NULL) && !strcmp(client[i].serial, serial)){
			client[i].used = 1;
			return;
		}
	}
	for(i = 0; i < client_len; i++){
		if(!client[i].used){
			client[i].serial = strdup(serial);
			client[i].cancel_flag = NULL;
			client[i].used = 1;
			return;
		}
	}
	client_len += 10;
	client = realloc(client, (client_len * sizeof(struct client_struct)));
	client[i].serial = strdup(serial);
	client[i].cancel_flag = NULL;
	client[i].used = 1;
	for(i++; i < client_len; i++){
		client[i].serial = NULL;
		client[i].cancel_flag = NULL;
		client[i].used = 0;
	}
}

