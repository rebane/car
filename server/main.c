#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <strings.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include "client.h"
#include "connection.h"

#define DEFAULT_PORT 9999

char *program_name, *command;

static int pthread_detach_new(void (*start_routine)(void *, size_t), void *buf, size_t count);
static void *pthread_detach_startup(void *buf);

int main(int argc, char *argv[]){
	struct sockaddr_in addr;
	int fd, i, s, port;
	char *config_file;
	program_name = argv[0];
	port = DEFAULT_PORT;
	command = NULL;
	config_file = 0;
	opterr = 0;
	while((i = getopt(argc, argv, "p:c:f:h")) != -1){
		if(i == 'p'){
			port = atoi(optarg);
		}else if(i == 'c'){
			command = optarg;
		}else if(i == 'f'){
			config_file = optarg;
		}else if(i == 'h'){
			printf("HELP!!!\n");
			exit(0);
		}
	}
	if(command == NULL){
		fprintf(stderr, "%s: no command\n", program_name);
		exit(1);
	}
	if(config_file == NULL){
		fprintf(stderr, "%s: no config file\n", program_name);
		exit(1);
	}
	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(fd < 0){
		fprintf(stderr, "%s: socket: %s\n", program_name, strerror(errno));
		exit(1);
	}
	i = 1;
	if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i)) < 0){
		fprintf(stderr, "%s: setsockopt: %s\n", program_name, strerror(errno));
		shutdown(fd, SHUT_RDWR);
		close(fd);
		exit(1);
	}
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(port);
	if(bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0){
		fprintf(stderr, "%s: bind: %s\n", program_name, strerror(errno));
		shutdown(fd, SHUT_RDWR);
		close(fd);
		exit(1);
	}
	if(listen(fd, 5) < 0){
		fprintf(stderr, "%s: listen: %s\n", program_name, strerror(errno));
		shutdown(fd, SHUT_RDWR);
		close(fd);
		exit(1);
	}
	signal(SIGCHLD, SIG_IGN);
	signal(SIGPIPE, SIG_IGN);
	client_init(config_file);
	while(1){
		i = sizeof(addr);
		s = accept(fd, (struct sockaddr *)&addr, (socklen_t *)&i);
		if(s < 0)fprintf(stderr, "%s: accept: %s\n", program_name, strerror(errno));
		pthread_detach_new(connection_thread, &s, sizeof(s));
	}
}

static int pthread_detach_new(void (*start_routine)(void *, size_t), void *buf, size_t count){
	unsigned char *p;
	int retval;
	pthread_t pthread;
	if(count){
		p = malloc(sizeof(void (*)(void *, size_t)) + sizeof(size_t) + count);
	}else{
		p = malloc(sizeof(void (*)(void *, size_t)) + sizeof(size_t) + sizeof(void *));
	}
	if(p == NULL)return(ENOMEM);
	*(void (**)(void *, size_t))&p[0] = start_routine;
	*(size_t *)&p[sizeof(void (*)(void *, size_t))] = count;
	if(count){
		memcpy(&p[sizeof(void (*)(void *, size_t)) + sizeof(size_t)], buf, count);
	}else{
		memcpy(&p[sizeof(void (*)(void *, size_t)) + sizeof(size_t)], &buf, sizeof(void *));
	}
	retval = pthread_create(&pthread, NULL, pthread_detach_startup, (void *)p);
	if(retval == 0){
		pthread_detach(pthread);
	}else{
		free(p);
	}
	return(retval);
}

static void *pthread_detach_startup(void *buf){
	pthread_cleanup_push(free, buf);
	if(*(size_t *)&((unsigned char *)buf)[sizeof(void (*)(void *, size_t))]){
		(*(void (**)(void *, size_t))&((unsigned char *)buf)[0])((void *)&((unsigned char *)buf)[sizeof(void (*)(void *, size_t)) + sizeof(size_t)], *(size_t *)&((unsigned char *)buf)[sizeof(void (*)(void *, size_t))]);
	}else{
		(*(void (**)(void *, size_t))&((unsigned char *)buf)[0])(*(void **)&((unsigned char *)buf)[sizeof(void (*)(void *, size_t)) + sizeof(size_t)], 0);
	}
	pthread_cleanup_pop(1);
	return(NULL);
}

