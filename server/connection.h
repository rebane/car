#ifndef _CONNECTION_H_
#define _CONNECTION_H_

#include <stdio.h>
#include <stdint.h>
#include "protocol.h"

#define CONNECTION_PACKET_DATA_LEN 4096

struct connection_packet_buffer{
	struct protocol_header header;
	uint8_t data[CONNECTION_PACKET_DATA_LEN];
} __attribute__((packed));

void connection_thread(void *_ptr, size_t _len);

#endif

