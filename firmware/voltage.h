#ifndef _VOLTAGE_H_
#define _VOLTAGE_H_

#include <stdint.h>

void voltage_init();
uint16_t voltage_read(uint8_t *buffer);

#endif

