#include "led.h"
#include "platform.h"
#include "mtimer.h"

static mtimer_t led_timer[LED_COUNT];
static uint16_t led_timeout1[LED_COUNT], led_timeout2[LED_COUNT];
static uint8_t led_state[LED_COUNT];

void led_init(){
	led_set(0, LED_STATE_OFF, 0, 0);
	led_set(1, LED_STATE_OFF, 0, 0);
}

void led_poll(){
	uint8_t led;
	for(led = 0; led < LED_COUNT; led++){
		if(led_state[led] == LED_STATE_OFF_TEMP){
			if(mtimer_timeout(&led_timer[led])){
				led_state[led] = LED_STATE_ON;
				platform_led_set(led, 1, 0);
			}
		}else if(led_state[led] == LED_STATE_ON_TEMP){
			if(mtimer_timeout(&led_timer[led])){
				led_state[led] = LED_STATE_OFF;
				platform_led_set(led, 0, 0);
			}
		}else if(led_state[led] == LED_STATE_BLINK_ON){
			if(mtimer_timeout(&led_timer[led])){
				led_state[led] = LED_STATE_BLINK_OFF;
				platform_led_set(led, 0, 0);
				mtimer_timeout_set(&led_timer[led], led_timeout2[led]);
			}
		}else if(led_state[led] == LED_STATE_BLINK_OFF){
			if(mtimer_timeout(&led_timer[led])){
				led_state[led] = LED_STATE_BLINK_ON;
				platform_led_set(led, 1, 0);
				mtimer_timeout_set(&led_timer[led], led_timeout1[led]);
			}
		}
	}
}

void led_set(uint8_t led, uint8_t state, uint16_t timeout1, uint16_t timeout2){
	if(led >= LED_COUNT)return;
	if((state == LED_STATE_BLINK) && ((led_state[led] == LED_STATE_BLINK_ON) || (led_state[led] == LED_STATE_BLINK_OFF)) && (led_timeout1[led] == timeout1) && (led_timeout2[led] == timeout2))return;
	led_state[led] = state;
	led_timeout1[led] = timeout1;
	led_timeout2[led] = timeout2;
	if(led_state[led] == LED_STATE_OFF){
		platform_led_set(led, 0, 0);
	}else if(led_state[led] == LED_STATE_ON){
		platform_led_set(led, 1, 0);
	}else if(led_state[led] == LED_STATE_OFF_TEMP){
		platform_led_set(led, 0, 0);
		mtimer_timeout_set(&led_timer[led], led_timeout1[led]);
	}else if(led_state[led] == LED_STATE_ON_TEMP){
		platform_led_set(led, 1, 0);
		mtimer_timeout_set(&led_timer[led], led_timeout1[led]);
	}else if(led_state[led] == LED_STATE_BLINK){
		led_state[led] = LED_STATE_BLINK_ON;
		platform_led_set(led, 1, 0);
		mtimer_timeout_set(&led_timer[led], led_timeout1[led]);
	}else{
		led_state[led] = LED_STATE_OFF;
		platform_led_set(led, 0, 0);
	}
}

