#ifndef _MMA7660_H_
#define _MMA7660_H_

#include <stdint.h>

void mma7660_init();
void mma7660_poll();
uint16_t mma7660_read(uint8_t *buffer);

#endif

