#ifndef _SERIAL_DEBUG_H_
#define _SERIAL_DEBUG_H_

#include <stdint.h>

void serial_debug_init();
int16_t serial_debug_read(void *buf, uint16_t count);
int16_t serial_debug_write(const void *buf, uint16_t count);
int16_t serial_debug_write_space(void);
int16_t serial_debug_read_flush(void);

#endif

