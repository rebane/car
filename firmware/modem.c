#include "modem.h"
#include <stdio.h>
#include "mtimer.h"
#include "serial_gsm.h"
#include "clock.h"
#include "uip.h"

// AT+CPIN="0000"
// AT+CLCK="SC",0,"0000" // disable pin code

// AT+CPBS="SM"
// OK

// AT+CPBW=1,"#00#00",129,"internet.emt.ee"
// AT+CPBW=2,"#01#00",129,"46.22.223.108"
// AT+CPBW=3,"#02#00",129,"9999"
// AT+CPBW=4,"#03#00",129,""
// OK

// AT+CPBR=?
// +CPBR: (1-250),40,18

// AT+CPBR=1,250
// +CPBR: 1,"#00#00",129,"internet.emt.ee"
// +CPBR: 2,"#01#00",129,"195.50.194.62"
// +CPBR: 3,"#02#00",129,"9999"
// +CPBR: 4,"#03#00",129,"904MKL"

#define MODEM_STATE_INIT         0
#define MODEM_STATE_INIT2        1
#define MODEM_STATE_GET_IMEI     2
#define MODEM_STATE_WAIT_IMEI    3
#define MODEM_STATE_GET_IMSI     4
#define MODEM_STATE_WAIT_IMSI    5
#define MODEM_STATE_GET_AB_SIZE  6
#define MODEM_STATE_WAIT_AB_SIZE 7
#define MODEM_STATE_GET_AB       8
#define MODEM_STATE_WAIT_AB      9
#define MODEM_STATE_GET_CLOCK    10
#define MODEM_STATE_WAIT_CLOCK   11
#define MODEM_STATE_DONE         12
#define MODEM_STATE_READY        13
#define MODEM_STATE_FAILED       14

static uint8_t modem_state, modem_try, modem_line_loc, modem_line_len;
static uint8_t modem_line_buffer[64];
static mtimer_t modem_timer;
static uint8_t modem_ab_start[8], modem_ab_end[8];
static char modem_imei_buffer[16], modem_imsi_buffer[32], modem_apn_buffer[32], modem_custom_buffer[32];
static uip_ipaddr_t modem_address_buffer;
static uint8_t modem_address_ok, modem_date_ok;
uint16_t modem_port_buffer;

static uint8_t modem_line_ok();
static void modem_copy_name(char *dest, uint8_t len);
static uint8_t modem_str2ip(char *s);
static uint64_t modem_date(uint8_t *buffer);

void modem_init(){
	modem_state = MODEM_STATE_INIT;
	modem_imei_buffer[0] = modem_imsi_buffer[0] = modem_apn_buffer[0] = modem_custom_buffer[0] = 0;
	modem_address_ok = modem_date_ok = 0;
	modem_port_buffer = 0;
}

void modem_getconfig_reset(){
	if(modem_imei_buffer[0] && modem_imsi_buffer[0] && modem_apn_buffer[0] && modem_address_ok && modem_port_buffer /*&& modem_custom_buffer[0]*/)return;
	modem_state = MODEM_STATE_INIT;
	modem_try = 0;
}

int8_t modem_getconfig(){
	uint8_t i, j;
	uint8_t *s, *e;
	uint64_t t;
	if(modem_state == MODEM_STATE_INIT){
		serial_gsm_read_flush();
		serial_gsm_write("AT+CLTS=1\r", 10);
		mtimer_timeout_set(&modem_timer, 500);
		modem_state = MODEM_STATE_INIT2;
	}
	if(modem_state == MODEM_STATE_INIT2){
		serial_gsm_read_flush();
		serial_gsm_write("AT+IPR=115200\r", 14);
		mtimer_timeout_set(&modem_timer, 500);
		modem_state = MODEM_STATE_GET_IMEI;
	}
	if(modem_state == MODEM_STATE_GET_IMEI){
		if(modem_imei_buffer[0])modem_state = MODEM_STATE_GET_IMSI;
		if(mtimer_timeout(&modem_timer)){
			serial_gsm_read_flush();
			modem_line_loc = 0;
			serial_gsm_write("AT+CGSN\r", 8);
			mtimer_timeout_set(&modem_timer, 1000);
			modem_state = MODEM_STATE_WAIT_IMEI;
		}
	}
	if(modem_state == MODEM_STATE_WAIT_IMEI){
		if(modem_line_ok()){
			if((modem_line_len > 14) && isdigit(modem_line_buffer[0]) && isdigit(modem_line_buffer[1]) && isdigit(modem_line_buffer[2])
					&& isdigit(modem_line_buffer[3]) && isdigit(modem_line_buffer[4]) && isdigit(modem_line_buffer[5]) && isdigit(modem_line_buffer[6])
					&& isdigit(modem_line_buffer[7]) && isdigit(modem_line_buffer[8]) && isdigit(modem_line_buffer[9]) && isdigit(modem_line_buffer[10])
					&& isdigit(modem_line_buffer[11])&& isdigit(modem_line_buffer[12]) && isdigit(modem_line_buffer[13]) && isdigit(modem_line_buffer[14])){
				for(i = 0; (i < (modem_line_len + 1)) && (i < 15); i++){
					modem_imei_buffer[i] = modem_line_buffer[i];
				}
				modem_imei_buffer[15] = 0;
				mtimer_timeout_set(&modem_timer, 0);
				modem_state = MODEM_STATE_GET_IMSI;
				modem_try = 0;
				printf("MODEM: IMEI OK\n");
			}
		}else if(mtimer_timeout(&modem_timer)){
			if(++modem_try > 2){
				modem_state = MODEM_STATE_GET_IMSI;
				modem_try = 0;
			}else{
				modem_state = MODEM_STATE_GET_IMEI;
			}
		}
	}
	if(modem_state == MODEM_STATE_GET_IMSI){
		if(modem_imsi_buffer[0])modem_state = MODEM_STATE_GET_AB_SIZE;
		if(mtimer_timeout(&modem_timer)){
			serial_gsm_read_flush();
			modem_line_loc = 0;
			serial_gsm_write("AT+CIMI\r", 8);
			mtimer_timeout_set(&modem_timer, 1000);
			modem_state = MODEM_STATE_WAIT_IMSI;
		}
	}
	if(modem_state == MODEM_STATE_WAIT_IMSI){
		if(modem_line_ok()){
			if((modem_line_len > 14) && isdigit(modem_line_buffer[0]) && isdigit(modem_line_buffer[1]) && isdigit(modem_line_buffer[2])
					&& isdigit(modem_line_buffer[3]) && isdigit(modem_line_buffer[4]) && isdigit(modem_line_buffer[5]) && isdigit(modem_line_buffer[6])
					&& isdigit(modem_line_buffer[7]) && isdigit(modem_line_buffer[8]) && isdigit(modem_line_buffer[9]) && isdigit(modem_line_buffer[10])
					&& isdigit(modem_line_buffer[11])&& isdigit(modem_line_buffer[12]) && isdigit(modem_line_buffer[13]) && isdigit(modem_line_buffer[14])){
				for(i = 0; (i < (modem_line_len + 1)) && (i < 31); i++){
					modem_imsi_buffer[i] = modem_line_buffer[i];
				}
				modem_imsi_buffer[31] = 0;
				mtimer_timeout_set(&modem_timer, 0);
				modem_state = MODEM_STATE_GET_AB_SIZE;
				modem_try = 0;
				printf("MODEM: IMSI OK\n");
			}
		}else if(mtimer_timeout(&modem_timer)){
			if(++modem_try > 30){
				modem_state = MODEM_STATE_GET_AB_SIZE;
			}else{
				modem_state = MODEM_STATE_GET_IMSI;
			}
		}
	}
	if(modem_state == MODEM_STATE_GET_AB_SIZE){
		if(modem_apn_buffer[0] && modem_address_ok && modem_port_buffer && modem_custom_buffer[0])modem_state = MODEM_STATE_GET_CLOCK;
		if(mtimer_timeout(&modem_timer)){
			serial_gsm_read_flush();
			modem_line_loc = 0;
			serial_gsm_write("AT+CPBS=\"SM\";+CPBR=?\r", 21);
			mtimer_timeout_set(&modem_timer, 1000);
			modem_state = MODEM_STATE_WAIT_AB_SIZE;
		}
	}
	if(modem_state == MODEM_STATE_WAIT_AB_SIZE){
		if(modem_line_ok()){
			if(!memcmp(modem_line_buffer, "+CPBR: ", 7)){
				s = e = NULL;
				j = 0;
				for(i = 7; modem_line_buffer[i]; i++){
					if(!isdigit(modem_line_buffer[i])){
						modem_line_buffer[i] = 0;
						j = 0;
						if(s && e)break;
					}else if(!j){
						if(s == NULL)s = &modem_line_buffer[i]; else e = &modem_line_buffer[i];
						j = 1;
					}
				}
				if(s && e){
					for(i = 0; (i < 7) && s[i]; i++)modem_ab_start[i] = s[i];
					modem_ab_start[7] = 0;
					for(i = 0; (i < 7) && e[i]; i++)modem_ab_end[i] = e[i];
					modem_ab_end[7] = 0;
					mtimer_timeout_set(&modem_timer, 0);
					modem_state = MODEM_STATE_GET_AB;
					modem_try = 0;
				}
			}
		}else if(mtimer_timeout(&modem_timer)){
			if(++modem_try > 30){
				modem_state = MODEM_STATE_GET_CLOCK;
				modem_try = 0;
			}else{
				modem_state = MODEM_STATE_GET_AB_SIZE;
			}
		}
	}
	if(modem_state == MODEM_STATE_GET_AB){
		if(mtimer_timeout(&modem_timer)){
			serial_gsm_read_flush();
			modem_line_loc = 0;
			serial_gsm_write("AT+CPBR=", 8);
			for(i = 0; modem_ab_start[i]; i++)serial_gsm_write(&modem_ab_start[i], 1);
			serial_gsm_write(",", 1);
			for(i = 0; modem_ab_end[i]; i++)serial_gsm_write(&modem_ab_end[i], 1);
			serial_gsm_write("\r", 1);
			mtimer_timeout_set(&modem_timer, 5000);
			modem_state = MODEM_STATE_WAIT_AB;
		}
	}
	if(modem_state == MODEM_STATE_WAIT_AB){
		if(modem_line_ok()){
			if(!memcmp(modem_line_buffer, "+CPBR: ", 7)){
				if(strstr((char *)modem_line_buffer, ",\"#00#00\",")){
					modem_copy_name(modem_apn_buffer, 32);
					if(!strcmp((char *)modem_apn_buffer, "i.e.e")){
						strcpy((char *)modem_apn_buffer, "internet.emt.ee");
					}
				}else if(strstr((char *)modem_line_buffer, ",\"#01#00\",")){
					modem_copy_name((char *)modem_line_buffer, 32);
					modem_address_ok = modem_str2ip((char *)modem_line_buffer);
				}else if(strstr((char *)modem_line_buffer, ",\"#02#00\",")){
					modem_copy_name((char *)modem_line_buffer, 32);
					modem_port_buffer = atoi((char *)modem_line_buffer);
				}else if(strstr((char *)modem_line_buffer, ",\"#03#00\",")){
					modem_copy_name(modem_custom_buffer, 32);
				}
				if(modem_apn_buffer[0] && modem_address_ok && modem_port_buffer && modem_custom_buffer[0]){
					mtimer_timeout_set(&modem_timer, 500);
					modem_state = MODEM_STATE_GET_CLOCK;
					modem_try = 0;
				}
			}
		}else if(mtimer_timeout(&modem_timer)){
			if(++modem_try > 5){
				modem_state = MODEM_STATE_GET_CLOCK;
				modem_try = 0;
			}else{
				if(modem_apn_buffer[0] && modem_address_ok && modem_port_buffer){
					modem_state = MODEM_STATE_GET_CLOCK;
					modem_try = 0;
				}else{
					modem_state = MODEM_STATE_GET_AB;
				}
			}
		}
	}
	if(modem_state == MODEM_STATE_GET_CLOCK){
		if(mtimer_timeout(&modem_timer)){
			serial_gsm_read_flush();
			modem_line_loc = 0;
			serial_gsm_write("AT+CCLK?\r", 9);
			mtimer_timeout_set(&modem_timer, 4000);
			modem_state = MODEM_STATE_WAIT_CLOCK;
		}
	}
	if(modem_state == MODEM_STATE_WAIT_CLOCK){
		if(modem_line_ok()){
			if(!memcmp(modem_line_buffer, "+CCLK: ", 7)){
				t = modem_date(&modem_line_buffer[7]);
				if(t){
					printf("CLOCK OK\n");
					clock_set(t, CLOCK_SOURCE_GSM);
					modem_date_ok = 1;
					modem_state = MODEM_STATE_DONE;
				}
			}
		}else if(mtimer_timeout(&modem_timer)){
			if(modem_date_ok || (++modem_try > 10)){
				modem_state = MODEM_STATE_DONE;
			}else{
				modem_state = MODEM_STATE_GET_CLOCK;
			}
		}
	}
	if(modem_state == MODEM_STATE_DONE){
		if(modem_imei_buffer[0] && modem_apn_buffer[0] && modem_address_ok && modem_port_buffer){
			modem_state = MODEM_STATE_READY;
		}else{
			modem_state = MODEM_STATE_FAILED;
		}
	}
	if(modem_state == MODEM_STATE_READY)return(1);
	if(modem_state == MODEM_STATE_FAILED)return(-1);
	return(0);
}

char *modem_imsi(){
	if(!modem_imsi_buffer[0])return((char *)0);
	return(modem_imsi_buffer);
}

char *modem_imei(){
	if(!modem_imei_buffer[0])return((char *)0);
	return(modem_imei_buffer);
}

char *modem_apn(){
	if(!modem_apn_buffer[0])return((char *)0);
	return(modem_apn_buffer);
}

uip_ipaddr_t *modem_address(){
//	uip_ipaddr(modem_address_buffer, 195, 50, 194, 62);
	return(&modem_address_buffer);
}

uint16_t modem_port(){
	return(modem_port_buffer);
}

char *modem_custom(){
	if(!modem_custom_buffer[0])return((char *)0);
	return(modem_custom_buffer);
}

#define tobin(c) ((((c) >= '0') && ((c) <= '9')) ? ((c) - '0') : ((((c) >= 'A') && ((c) <= 'F')) ? ((c) - 'A' + 10) : ((((c) >= 'a') && ((c) <= 'f')) ? ((c) - 'a' + 10) : (0))))
uint8_t modem_serial(uint8_t *buffer){
	uint8_t i;
	if(!modem_imei_buffer[0])return(0);
	for(i = 0; i < 15; i++){
		if(!(i & 1)){
			buffer[i / 2] = tobin(modem_imei_buffer[i]);
		}else{
			buffer[i / 2] |= (tobin(modem_imei_buffer[i]) << 4);
		}
	}
	return(15);
}

static uint8_t modem_line_ok(){
	uint8_t c, i;
	for(i = 0; i < 32; i++){
		if(serial_gsm_read(&c, 1) < 1)return(0);
		if((c == '\r') || (c == '\n')){
			if(modem_line_loc){
				modem_line_buffer[modem_line_loc] = 0;
				modem_line_len = modem_line_loc;
				modem_line_loc = 0;
				return(1);
			}
		}else{
			if(modem_line_loc < 63)modem_line_buffer[modem_line_loc++] = c;
		}
	}
	return(0);
}

static void modem_copy_name(char *dest, uint8_t len){
	uint8_t i, j;
	modem_line_buffer[strlen((char *)modem_line_buffer) - 1] = 0;
	for(i = strlen((char *)modem_line_buffer) - 1; i > 7; i--){
		if(modem_line_buffer[i] == '\"'){
			for(i++, j = 0; modem_line_buffer[i] && (j < (len - 1)); i++, j++){
				dest[j] = modem_line_buffer[i];
			}
			dest[j] = 0;
			return;
		}
	}
}

static uint8_t modem_str2ip(char *s){
	char t[4];
	uint8_t ip[4];
	char c;
	int i, j, x;
	c = '.';
	for(i = 0; s[i]; i++)if(!isspace(s[i]))break;
	for(j = 0; j < 4; j++){
		if(j == 3)c = 0;
		if(isdigit(s[i]) && (s[i + 1] == c)){
			t[0] = s[i];
			t[1] = 0;
			x = atoi(t);
			if((x < 0) || (x > 255))return(0);
			ip[j] = x;
			i += 2;
		}else if(isdigit(s[i]) && isdigit(s[i + 1]) && (s[i + 2] == c)){
			t[0] = s[i];
			t[1] = s[i + 1];
			t[2] = 0;
			x = atoi(t);
			if((x < 0) || (x > 255))return(0);
			ip[j] = x;
			i += 3;
		}else if(isdigit(s[i]) && isdigit(s[i + 1]) && isdigit(s[i + 2]) && (s[i + 3] == c)){
			t[0] = s[i];
			t[1] = s[i + 1];
			t[2] = s[i + 2];
			t[3] = 0;
			x = atoi(t);
			if((x < 0) || (x > 255))return(0);
			ip[j] = x;
			i += 4;
		}else{
			return(0);
		}
	}
	uip_ipaddr(modem_address_buffer, ip[0], ip[1], ip[2], ip[3]);
	return(1);
}

static uint64_t modem_date(uint8_t *buffer){
	struct tm tm;
	uint64_t t;
	uint8_t i;
	t = 0;
	if(strlen((char *)buffer) < 19)goto date_done;
	if((buffer[0] != '\"') /*|| (buffer[18] != '\"')*/)goto date_done;
	if((buffer[3] != '/') || (buffer[6] != '/'))goto date_done;
	if((buffer[12] != ':') || (buffer[15] != ':'))goto date_done;
	if(buffer[9] != ',')goto date_done;
	if(!isdigit(buffer[1]) || !isdigit(buffer[2]))goto date_done;
	if(!isdigit(buffer[4]) || !isdigit(buffer[5]))goto date_done;
	if(!isdigit(buffer[7]) || !isdigit(buffer[8]))goto date_done;
	if(!isdigit(buffer[10]) || !isdigit(buffer[11]))goto date_done;
	if(!isdigit(buffer[13]) || !isdigit(buffer[14]))goto date_done;
	if(!isdigit(buffer[16]) || !isdigit(buffer[17]))goto date_done;
	tm.tm_sec = ((buffer[16] - '0') * 10) + (buffer[17] - '0');
	tm.tm_min = ((buffer[13] - '0') * 10) + (buffer[14] - '0');
	tm.tm_hour = ((buffer[10] - '0') * 10) + (buffer[11] - '0');
	tm.tm_mday = ((buffer[7] - '0') * 10) + (buffer[8] - '0');
	tm.tm_mon = ((buffer[4] - '0') * 10) + (buffer[5] - '0') - 1;
	tm.tm_year = ((buffer[1] - '0') * 10) + (buffer[2] - '0');
	if(tm.tm_year < 98)tm.tm_year += 100;
	t = mktime(&tm);
	if((strlen((char *)buffer) > 21) && isdigit(buffer[19]) && isdigit(buffer[20])){
		i = ((buffer[19] - '0') * 10) + (buffer[20] - '0');
		if(buffer[18] == '+'){
			t -= (i / 4) * 3600;
		}else if(buffer[18] == '-'){
			t += (i / 4) * 3600;
		}
	}
date_done:
	if(t < 1415301889LLU)return(0); // Your time zone: Thu 06 Nov 2014 21:24:49 EET GMT+2:00
	return(t);
}

