#ifndef _PMEM_H_
#define _PMEM_H_

#include <stdint.h>

#define PMEM_PAGE_SIZE 1536
#define PMEM_ROW_SIZE  128

void *pmem_cpy_p2d(void *dest, uint32_t src, uint32_t n);
uint32_t pmem_cpy_d2p(uint32_t dest, void *src, uint32_t n);
void pmem_erase_page_containing(uint32_t dest);

#endif

