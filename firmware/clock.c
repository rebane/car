#include "clock.h"
#include "mtimer.h"
#include "bjson.h"

static uint64_t clock_gps, clock_gsm, clock_net, clock_can;

void clock_init(){
	clock_gps = clock_gsm = clock_net = clock_can = 0;
}

void clock_set(uint64_t date, uint8_t source){
	if(source == CLOCK_SOURCE_GPS){
		clock_gps = date - mtimer_get((void *)0);
	}else if(source == CLOCK_SOURCE_GSM){
		clock_gsm = date - mtimer_get((void *)0);
	}else if(source == CLOCK_SOURCE_NET){
		clock_net = date - mtimer_get((void *)0);
	}else if(source == CLOCK_SOURCE_CAN){
		clock_can = date - mtimer_get((void *)0);
	}
}

uint16_t clock_read(uint8_t *buffer){
	uint16_t l;
	uint32_t t;
	l = 0;
	t = mtimer_get((void *)0);
	if(clock_gps){
		l += bjson_string(&buffer[l], "gpsDate");
		l += bjson_positive(&buffer[l], t + clock_gps);
	}
	if(clock_gsm){
		l += bjson_string(&buffer[l], "gsmDate");
		l += bjson_positive(&buffer[l], t + clock_gsm);
	}
	if(clock_net){
		l += bjson_string(&buffer[l], "netDate");
		l += bjson_positive(&buffer[l], t + clock_net);
	}
	if(clock_can){
		l += bjson_string(&buffer[l], "canDate");
		l += bjson_positive(&buffer[l], t + clock_can);
	}
	return(l);
}

