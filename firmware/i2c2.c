#include "i2c2.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"
#include "mtimer.h"
#include <stdio.h>

// SDA = RC4
// SCL = RC5

#define i2c2_delay()

static uint8_t i2c2_started;

static void i2c2_start();
static void i2c2_stop();
static void i2c2_write(uint8_t bit);
static uint8_t i2c2_read();
static uint8_t i2c2_write_byte(uint8_t send_start, uint8_t send_stop, uint8_t byte);
static uint8_t i2c2_sda_get();
static void i2c2_sda_low();
static uint8_t i2c2_scl_get();
static void i2c2_scl_low();

void i2c2_init(){
	ANSELCbits.ANSC4 = 0;
	ANSELCbits.ANSC5 = 0;
	i2c2_sda_get();
	i2c2_scl_get();
	i2c2_started = 0;
}

void i2c2_test(){
	uint8_t i;
	while(1){
		printf("I2C 1: %u\n", (unsigned int)i2c2_write_byte(1, 0, 0x4C << 1));
		printf("I2C 2: %u\n", (unsigned int)i2c2_write_byte(0, 0, 0x00));
		printf("I2C 3: %u\n", (unsigned int)i2c2_write_byte(1, 0, (0x4C << 1) | 1));
		for(i = 0; i < 8; i++){
			if(!i2c2_read()){
				printf("I2C GOT Z\n");
				return;
			}
		}
		mtimer_sleep(1000);
	}
}

static void i2c2_start(){
	if(i2c2_started){
		i2c2_sda_get();
		i2c2_delay();
		if(i2c2_scl_get() == 0){
			printf("arb lost 1a\n");
			return;
		}
		i2c2_delay();
	}
	if(i2c2_sda_get() == 0){
		printf("arb lost 1\n");
		return;
	}
	i2c2_sda_low();
	i2c2_delay();
	i2c2_scl_low();
	i2c2_started = 1;
}

static void i2c2_stop(){
	i2c2_sda_low();
	i2c2_delay();
	while(i2c2_scl_get() == 0);
	i2c2_delay();
	if(i2c2_sda_get() == 0){
		printf("arb lost 2\n");
		return;
	}
	i2c2_delay();
	i2c2_started = 0;
}

static void i2c2_write(uint8_t bit){
	if(bit){
		i2c2_sda_get();
	}else{
		i2c2_sda_low();
	}
	i2c2_delay();
	if(i2c2_scl_get() == 0){
		printf("arb lost 3a\n");
		return;
	}
	if(bit && (i2c2_sda_get() == 0)){
		printf("arb lost 3\n");
		return;
	}
	i2c2_delay();
	i2c2_scl_low();
}

static uint8_t i2c2_read(){
	uint8_t bit;
	i2c2_sda_get();
	i2c2_delay();
	if(i2c2_scl_get() == 0){
		printf("arb lost 4\n");
		return(0);
	}
	bit = i2c2_sda_get();
	if(!bit){
		i2c2_scl_get();
		return(bit);
	}
	i2c2_delay();
	i2c2_scl_low();
	return(bit);
}

static uint8_t i2c2_write_byte(uint8_t send_start, uint8_t send_stop, uint8_t byte){
	uint8_t bit, nack;
	if(send_start)i2c2_start();
	for(bit = 0; bit < 8; bit++){
		i2c2_write(byte & 0x80);
		byte <<= 1;
	}
	nack = i2c2_read();
	if(send_stop)i2c2_stop();
	return(nack);
}

static uint8_t i2c2_sda_get(){
	TRISCbits.TRISC4 = 1;
	mtimer_sleep(5);
	return(PORTCbits.RC4 != 0);
}

static void i2c2_sda_low(){
	TRISCbits.TRISC4 = 0;
	PORTCbits.RC4 = 0;
}

static uint8_t i2c2_scl_get(){
	TRISCbits.TRISC5 = 1;
	mtimer_sleep(5);
	return(PORTCbits.RC5 != 0);
}

static void i2c2_scl_low(){
	TRISCbits.TRISC5 = 0;
	PORTCbits.RC5 = 0;
}

