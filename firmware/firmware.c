#include "firmware.h"
#include "pmem.h"
#include "header_parser.h"
#include "crc32.h"

uint8_t firmware_validate(uint32_t start, uint32_t *len, void *buffer){
	void *header;
	uint32_t i, j, l, crc_orig, crc;

	if(*len > PROGRAM_MAX_LEN)return(1);
	crc = 0;
	for(i = 0; i < (*len ? *len : PMEM_PAGE_SIZE); i += PMEM_PAGE_SIZE){
		if(!*len){
			l = PMEM_PAGE_SIZE;
		}else{
			l = (*len - i) > PMEM_PAGE_SIZE ? PMEM_PAGE_SIZE : (*len - i);
		}
		pmem_cpy_p2d(buffer, start + i, l);
		if(!i){
			header = header_validate(buffer, &((uint8_t *)buffer)[l - 1]);
			if(header == ((void *)0))return(2);
			if(header_value(header, HEADER_OFFSET) != ((uintptr_t)header - (uintptr_t)buffer))return(3);
			if(*len && (header_value(header, HEADER_SIZE) > *len))return(4);
			*len = header_value(header, HEADER_SIZE);
			if(*len > PROGRAM_MAX_LEN)return(5);
			if(!*len)return(6);
			if(header_value(header, HEADER_LOAD_ADDRESS) != LOAD_ADDRESS)return(7);
			if(header_value(header, HEADER_TARGET_PLATFORM) != TARGET_PLATFORM)return(8);
			crc_orig = header_value(header, HEADER_CRC);
			header_value_set(header, HEADER_CRC, 0xFFFFFFFF);
		}
		for(j = 0; j < l; j++){
			crc = crc32(crc, ((uint8_t *)buffer)[j]);
		}
	}
	if(crc_orig != crc)return(9);
	return(0);
}

