#ifndef _FIRMWARE_H_
#define _FIRMWARE_H_

#include <stdint.h>

uint8_t firmware_validate(uint32_t start, uint32_t *len, void *buffer); // kasutatav buffer funktsiooni jaoks peab olema vähemalt PMEM_PAGE_SIZE suurune

#endif

