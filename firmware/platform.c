#include "platform.h"
#include <xc.h>
#include <libpic30.h>

#define PLATFORM_LED_MODE_MAX 4

uint32_t PLATFORM_FHS, PLATFORM_FIN, PLATFORM_FPLLI, PLATFORM_FSYS, PLATFORM_FOSC, PLATFORM_FP, PLATFORM_FCY, PLATFORM_FCPU;

static uint8_t platform_leds[PLATFORM_LED_MODE_MAX], platform_led_mode;

void platform_init(){
	uint8_t i;

	INTCON2bits.GIE = 0;
	INTCON1bits.NSTDIS = 1;

	RCONbits.SWDTEN = 0;

	OSCCONbits.NOSC = 7;
	CLKDIVbits.FRCDIV = 0;
	OSCCONbits.OSWEN = 1;
	while(OSCCONbits.OSWEN);

	PLATFORM_FHS = PLATFORM_FIN = 8000000LLU;
	CLKDIVbits.PLLPRE = 0;   // N1=2, FPPLI = 4MHz
	PLATFORM_FPLLI = 4000000LLU;
	PLLFBDbits.PLLDIV = 58;  // M=60, FSYS = 240MHz
	PLATFORM_FSYS = 240000000LLU;
	CLKDIVbits.PLLPOST = 0;  // N2=2, FOSC = 120MHz => FCPU = 60MHz
	PLATFORM_FOSC = 120000000LLU;

	__builtin_write_OSCCONH(0x03);
	__builtin_write_OSCCONL(OSCCON | 0x01);
	while(OSCCONbits.COSC != 0b011);
	while(OSCCONbits.LOCK != 1);
	PLATFORM_FP = PLATFORM_FCY = PLATFORM_FCPU = 60000000LLU;
	SRbits.IPL = 0;
	CORCONbits.IPL3 = 0;
	/********/

	// LED RED = PB12, RPI44
	TRISBbits.TRISB12 = 0;
	PORTBbits.RB12 = 1;

	// LED GREEN = PB13, RPI45
	TRISBbits.TRISB13 = 0;
	PORTBbits.RB13 = 1;

	// LED MODES
	for(i = 0; i < PLATFORM_LED_MODE_MAX; i++){
		platform_leds[i] = 0;
	}
	if(PLATFORM_LED_MODE_MAX > 1)platform_leds[1] = 0xFF;
	platform_led_mode_set(0);
}

void platform_led_set(uint8_t led, uint8_t status, uint8_t mode){
	if((mode >= PLATFORM_LED_MODE_MAX) || (led > 7))return;
	if(status){
		platform_leds[mode] |= (1 << led);
	}else{
		platform_leds[mode] &= ~(1 << led);
	}
	platform_led_mode_set(platform_led_mode);
}

void platform_led_toggle(uint8_t led, uint8_t mode){
	if((mode >= PLATFORM_LED_MODE_MAX) || (led > 7))return;
	if(platform_leds[mode] & (1 << led)){
		platform_led_set(led, 0, mode);
	}else{
		platform_led_set(led, 1, mode);
	}
}

void platform_led_mode_set(uint8_t mode){
	if(mode >= PLATFORM_LED_MODE_MAX)return;
	platform_led_mode = mode;
	if(platform_leds[platform_led_mode] & (1 << 0)){
		PORTBbits.RB12 = 1;
	}else{
		PORTBbits.RB12 = 0;
	}
	if(platform_leds[platform_led_mode] & (1 << 1)){
		PORTBbits.RB13 = 1;
	}else{
		PORTBbits.RB13 = 0;
	}
}

