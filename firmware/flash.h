#ifndef _FLASH_H_
#define _FLASH_H_

#include <stdint.h>

#define FLASH_DATA_LEN (4096 - 16)

void flash_init();
void flash_poll();
uint8_t flash_read_ready();
uint16_t flash_read(void *buf, uint64_t *uid, uint16_t *loc, uint16_t *remain, uint8_t *fresh);
uint8_t flash_write_ready();
void flash_write(const void *buf, uint16_t count);
void flash_delete(uint64_t uid, uint16_t loc);

#endif

