#ifndef _SERIAL_GPS_H_
#define _SERIAL_GPS_H_

#include <stdint.h>

void serial_gps_init();
void serial_gps_rx_invert();
int16_t serial_gps_read(void *buf, uint16_t count);
int16_t serial_gps_write(const void *buf, uint16_t count);
int16_t serial_gps_write_space(void);
int16_t serial_gps_read_flush(void);

#endif

