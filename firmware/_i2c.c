#include "i2c.h"
#include <xc.h>
#include <libpic30.h>
#include <stdio.h>

// SDA = RC4
// SCL = RC5
// INT = RA9

#define I2C_STATE_IDLE        0
#define I2C_STATE_WRITE_START 0

static volatile uint8_t i2c_state;

void i2c_init(){
	IEC1bits.MI2C1IE = 0;
	I2C1CONbits.I2CEN = 0;
	i2c_state = I2C_STATE_IDLE;

	ANSELCbits.ANSC4 = 0;
	ANSELCbits.ANSC5 = 0;
	ODCCbits.ODCC4 = 0;
	ODCCbits.ODCC5 = 0;

	I2C1BRG = 300;
	I2C1CON = 0;
	I2C1ADD = 0;
	I2C1MSK = 0;
	I2C1CONbits.I2CEN = 1;
	IEC1bits.MI2C1IE = 1;
}

void i2c_test(){
}

void i2c_write(uint8_t address, uint8_t *data, uint8_t len){
	I2C1CON = 0;
	i2c_state = I2C_STATE_WRITE_START;
	I2C1CONbits.SEN = 1;
}

int8_t i2c_start_ready(){
	return(!I2C1CONbits.SEN);
}

void i2c_restart(){
	I2C1CONbits.RSEN = 1;
}

int8_t i2c_restart_ready(){
	return(!I2C1CONbits.RSEN);
}

void i2c_send(uint8_t data){
	I2C1TRN = data;
}

int8_t i2c_send_ready(){
	if(I2C1STATbits.TRSTAT)return(0);
	if(I2C1STATbits.ACKSTAT)return(-1);
	return(1);
}

void i2c_recv(uint8_t ack){
	if(ack)I2C1CONbits.ACKDT = 0; else I2C1CONbits.ACKDT = 1;
	ack = I2C1RCV;
	I2C1CONbits.RCEN = 1;
}

int8_t i2c_recv_ready(uint8_t *data){
	if(!I2C1STATbits.RBF)return(0);
	*data = I2C1RCV;
	return(1);
}

void i2c_stop(){
	I2C1CONbits.PEN = 1;
}

int8_t i2c_stop_ready(){
	return(!I2C1CONbits.PEN);
}

void _MI2C1Handler(){
}

