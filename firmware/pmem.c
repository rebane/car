#include "pmem.h"
#include <stdint.h>
#include <xc.h>
#include <libpic30.h>

void *pmem_cpy_p2d(void *dest, uint32_t src, uint32_t n){
	uint32_t i;
	for(i = 0; i < n; i += ((PMEM_ROW_SIZE * 3) / 2)){
		_memcpy_p2d24(&((uint8_t *)dest)[i], (((src + i) * 2) / 3), (n - i) > ((PMEM_ROW_SIZE * 3) / 2) ? ((PMEM_ROW_SIZE * 3) / 2) : (n - i));
	}
	return(dest);
}

uint32_t pmem_cpy_d2p(uint32_t dest, void *src, uint32_t n){
	uint32_t i, d1, d2;
	for(i = 0; i < n; i += 6){
		d1 = ((uint32_t)((uint8_t *)src)[i + 0] << 0) | ((uint32_t)((uint8_t *)src)[i + 1] << 8) | ((uint32_t)((uint8_t *)src)[i + 2] << 16);
		d2 = ((uint32_t)((uint8_t *)src)[i + 3] << 0) | ((uint32_t)((uint8_t *)src)[i + 4] << 8) | ((uint32_t)((uint8_t *)src)[i + 5] << 16);
		_write_flash_word48((((dest + i) * 2) / 3), d1, d2);
	}
	return(dest);
}

void pmem_erase_page_containing(uint32_t dest){
	_erase_flash((dest / PMEM_PAGE_SIZE) * 1024);
}

