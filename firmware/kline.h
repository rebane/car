#ifndef _KLINE_H_
#define _KLINE_H_

#include <stdint.h>

void kline_init();
void kline_send_slow_init(uint8_t address);
int8_t kline_send(uint16_t initial_sleep, uint16_t inter_byte_sleep, void *buf, uint8_t count);
int16_t kline_read(void *buf, uint16_t count);

#endif

