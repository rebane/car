#include "kline.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"
#include "serial_debug.h"
#include <stdio.h>

// K-TXD  = RB9, RP41
// C2IN1+ = RA1, RPI17
// C2IN1- = RB0, RPI32
// http://www.mp3car.com/engine-management-obd-ii-engine-diagnostics-etc/144791-about-the-fuel-remaining-problem.html

#define KLINE_SEND_BUF_LEN     32
#define KLINE_RECV_FIFO_LEN    32

#define KLINE_TIMER_MODE_START 1
#define KLINE_TIMER_MODE_DELAY 2

#define kline_critical_enter() do{ INTCON2bits.GIE = 0; }while(0)
#define kline_critical_exit()  do{ INTCON2bits.GIE = 1; }while(0)

#if KLINE_SEND_BUF_LEN > 127
#error KLINE_SEND_BUF_LEN too big
#endif

static volatile uint8_t kline_send_buf_len, kline_send_buf_loc;

#if KLINE_RECV_FIFO_LEN < 256
static volatile uint8_t kline_recv_fifo_in, kline_recv_fifo_out, kline_recv_fifo_len;
#elif KLINE_RECV_FIFO_LEN < 65536
static volatile uint16_t kline_recv_fifo_in, kline_recv_fifo_out, kline_recv_fifo_len;
#else
#error KLINE_RECV_FIFO_LEN too big
#endif

static volatile uint8_t kline_send_buf[KLINE_SEND_BUF_LEN], kline_recv_fifo[KLINE_RECV_FIFO_LEN];
static volatile uint8_t kline_timer_mode, kline_address;
static volatile uint16_t kline_timer, kline_timeout;

static void kline_tx(uint8_t c);
static void kline_tx_disable();
static void kline_rx_enable();
static void kline_rx_disable();
static void kline_timer_set(uint16_t msec);
static void kline_timer_disable();

void kline_init(){
	IEC0bits.T2IE = 0;
	IEC0bits.U1TXIE = 0;
	IEC0bits.U1RXIE = 0;
	IEC4bits.U1EIE = 0;

	kline_recv_fifo_in = kline_recv_fifo_out = kline_recv_fifo_len = 0;

	U1MODE = 0;

	// K-TXD (initially GPIO)
	ANSELBbits.ANSB9 = 0;
	RPOR3bits.RP41R = 0;
	PORTBbits.RB9 = 0;
	TRISBbits.TRISB9 = 0;

	// RX
	RPINR18bits.U1RXR = 2;

	// C2IN1+
	ANSELAbits.ANSA1 = 1;

	// C2IN1-
	ANSELBbits.ANSB0 = 1;

	// COMPARATOR
	CM2CON = 0;
	CM2CONbits.COE = 1;
	CM2CONbits.CON = 1;

	U1STA = 0;
	U1BRG = (PLATFORM_FCPU / (4LLU * 10400LLU)) - 1LLU;
	U1MODEbits.BRGH = 1;
	U1MODEbits.URXINV = 1; // inverted
	U1STAbits.UTXINV = 1;  // inverted
	U1STAbits.UTXISEL1 = 0;
	U1STAbits.UTXISEL0 = 1;
	U1MODEbits.UARTEN = 1;
	U1STAbits.UTXEN = 1; // Note: The UTXEN bit is set after the UARTEN bit has been set; otherwise, UART transmissions will not be enabled.

	IPC3bits.U1TXIP = 1;
	IPC2bits.U1RXIP = 1;
	IPC1bits.T2IP = 1;

	IFS0bits.U1TXIF = 0;
	IFS0bits.U1RXIF = 0;
	IFS4bits.U1EIF = 0;

	IEC0bits.U1RXIE = 1;
	IEC4bits.U1EIE = 1;
}

void kline_send_slow_init(uint8_t address){
	kline_critical_enter();
	kline_timer_disable();
	kline_rx_disable();
	kline_tx_disable();
	// (60000000/256)/46875
	T2CON = 0;
	T2CONbits.TCKPS = 3;
	TMR2 = 0;
	PR2 = 46874;
	IFS0bits.T2IF = 0;
	// init & write start bit
	kline_address = address;
	kline_send_buf_loc = 0;
	kline_timer_mode = KLINE_TIMER_MODE_START;
	PORTBbits.RB9 = 1;
	// fire timer
	T2CONbits.TON = 1;
	IEC0bits.T2IE = 1;
	kline_critical_exit();
}

int8_t kline_send(uint16_t initial_sleep, uint16_t inter_byte_sleep, void *buf, uint8_t count){
	if(count > KLINE_SEND_BUF_LEN)return(-1);
	if(!count)return(0);
	kline_critical_enter();
	kline_timer_disable();
	kline_rx_disable();
	kline_tx_disable();
	for(kline_send_buf_len = 0; kline_send_buf_len < count; kline_send_buf_len++){
		kline_send_buf[kline_send_buf_len] = ((uint8_t *)buf)[kline_send_buf_len];
	}
	kline_send_buf_loc = 0;
	kline_timer_mode = KLINE_TIMER_MODE_DELAY;
	kline_timeout = inter_byte_sleep;
	if(initial_sleep){
		kline_timer_set(initial_sleep);
	}else{
		kline_tx(kline_send_buf[kline_send_buf_loc]);
	}
	kline_critical_exit();
	return(count);
}

int16_t kline_read(void *buf, uint16_t count){
	uint16_t len;
	if(!kline_recv_fifo_len)return(0);
	kline_critical_enter();
	IEC0bits.U1RXIE = 0;
	for(len = 0; kline_recv_fifo_len && (len < count) && (len < 32767); len++){
		((uint8_t *)buf)[len] = kline_recv_fifo[kline_recv_fifo_out];
		kline_recv_fifo_len--;
		kline_recv_fifo_out = (kline_recv_fifo_out + 1) % KLINE_RECV_FIFO_LEN;
	}
	IEC0bits.U1RXIE = 1;
	kline_critical_exit();
	return(len);
}

static void kline_tx(uint8_t c){
	PORTBbits.RB9 = 0;
	RPOR3bits.RP41R = 1;
	IFS0bits.U1TXIF = 0;
	U1TXREG = c;
	IEC0bits.U1TXIE = 1;
}

static void kline_tx_disable(){
	RPOR3bits.RP41R = 0;
	PORTBbits.RB9 = 0;
	TRISBbits.TRISB9 = 0;
	IEC0bits.U1TXIE = 0;
}

static void kline_rx_enable(){
	RPINR18bits.U1RXR = 2;
	kline_recv_fifo_in = kline_recv_fifo_out = kline_recv_fifo_len = 0;
	IFS0bits.U1RXIF = 0;
	IEC0bits.U1RXIE = 1;
}

static void kline_rx_disable(){
	RPINR18bits.U1RXR = 0;
	IEC0bits.U1RXIE = 0;
	kline_recv_fifo_in = kline_recv_fifo_out = kline_recv_fifo_len = 0;
}

static void kline_timer_set(uint16_t msec){
	kline_timer_disable();
	kline_timer = msec;
	T2CON = 0;
	TMR2 = 0;
	PR2 = (PLATFORM_FCPU / 1000LLU) - 1LLU;;
	IFS0bits.T2IF = 0;
	T2CONbits.TON = 1;
	IEC0bits.T2IE = 1;
}

static void kline_timer_disable(){
	IEC0bits.T2IE = 0;
	T2CONbits.TON = 0;
}

void _U1ErrHandler(){
	U1STAbits.OERR = 0;
	U1STAbits.PERR = 0;
	U1STAbits.FERR = 0;
	IFS4bits.U1EIF = 0;
}

void _U1RXHandler(){
	uint16_t c;
	while(U1STAbits.URXDA){
		c = U1RXREG;
		if(kline_recv_fifo_len < KLINE_RECV_FIFO_LEN){
			kline_recv_fifo[kline_recv_fifo_in] = (c & 0xFF);
			kline_recv_fifo_in = (kline_recv_fifo_in + 1) % KLINE_RECV_FIFO_LEN;
			kline_recv_fifo_len++;
		}
	}
	IFS0bits.U1RXIF = 0;
}

void _U1TXHandler(){
	kline_tx_disable();
	kline_send_buf_loc++;
	if(kline_send_buf_loc < kline_send_buf_len){
		kline_timer_set(kline_timeout);
	}else{
		kline_timer_disable();
		kline_rx_enable();
	}
	IFS0bits.U1TXIF = 0;
}

void _T2Handler(void){
	if(kline_timer_mode == KLINE_TIMER_MODE_START){
		if(kline_send_buf_loc < 8){
			PORTBbits.RB9 = !((kline_address >> kline_send_buf_loc) & 0x01);
			kline_send_buf_loc++;
		}else{
			PORTBbits.RB9 = 0;
			kline_timer_disable();
			kline_rx_enable();
		}
	}else if(kline_timer_mode == KLINE_TIMER_MODE_DELAY){
		if(!kline_timer--){
			kline_timer_disable();
			kline_tx(kline_send_buf[kline_send_buf_loc]);
		}
	}
	IFS0bits.T2IF = 0;
}

