#include "serial_gsm.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"

// TX = RC1, RP49
// RX = RB1, RPI33
// RTS = RC0, RP48
// CTS = RB15, RPI47
// DTR = RB14, RPI46
// NRESET = RC2, RPI50
// PWRKEY = RA7
// STATUS = RA10
// http://www.edaboard.com/thread306862.html
// AT+CGDCONT=1,"IP","internet.emt.ee"
// ATD*99***1#

// AT+CMGF=0
// AT+CNMI=1,1,2,1,1
// AT+CSCB=0,"50",""


#define SERIAL_GSM_SEND_FIFO_LEN 128
#define SERIAL_GSM_RECV_FIFO_LEN 250

#define serial_gsm_critical_enter() do{ INTCON2bits.GIE = 0; }while(0)
#define serial_gsm_critical_exit()  do{ INTCON2bits.GIE = 1; }while(0)

#if SERIAL_GSM_SEND_FIFO_LEN < 256
static volatile uint8_t serial_gsm_send_fifo_in, serial_gsm_send_fifo_out, serial_gsm_send_fifo_len;
#elif SERIAL_GSM_SEND_FIFO_LEN < 65536
static volatile uint16_t serial_gsm_send_fifo_in, serial_gsm_send_fifo_out, serial_gsm_send_fifo_len;
#else
#error SERIAL_GSM_SEND_FIFO_LEN too big
#endif

#if SERIAL_GSM_RECV_FIFO_LEN < 256
static volatile uint8_t serial_gsm_recv_fifo_in, serial_gsm_recv_fifo_out, serial_gsm_recv_fifo_len;
#elif SERIAL_GSM_RECV_FIFO_LEN < 65536
static volatile uint16_t serial_gsm_recv_fifo_in, serial_gsm_recv_fifo_out, serial_gsm_recv_fifo_len;
#else
#error SERIAL_GSM_RECV_FIFO_LEN too big
#endif

static volatile uint8_t serial_gsm_send_fifo[SERIAL_GSM_SEND_FIFO_LEN], serial_gsm_recv_fifo[SERIAL_GSM_RECV_FIFO_LEN];

void serial_gsm_init(){
	IEC5bits.U3TXIE = 0;
	IEC5bits.U3RXIE = 0;
	IEC5bits.U3EIE = 0;

	serial_gsm_send_fifo_in = serial_gsm_send_fifo_out = serial_gsm_send_fifo_len = 0;
	serial_gsm_recv_fifo_in = serial_gsm_recv_fifo_out = serial_gsm_recv_fifo_len = 0;

	U3MODE = 0;

	// TX
	ANSELCbits.ANSC1 = 0;
	RPOR5bits.RP49R = 27;

	// RTS
	ANSELCbits.ANSC0 = 0;
	RPOR5bits.RP48R = 28;

	// RX
	ANSELBbits.ANSB1 = 0;
	CNPUBbits.CNPUB1 = 1;
	RPINR27bits.U3RXR = 33;

	// CTS
	RPINR27bits.U3CTSR = 47;

	U3STA = 0;
	U3BRG = (PLATFORM_FCPU / (4LLU * 115200LLU)) - 1LLU;
	U3MODEbits.BRGH = 1;
	U3STAbits.UTXISEL1 = 0;
	U3STAbits.UTXISEL0 = 0;
	U3MODEbits.UEN = 2;
	U3MODEbits.UARTEN = 1;
	U3STAbits.UTXEN = 1; // Note: The UTXEN bit is set after the UARTEN bit has been set; otherwise, UART transmissions will not be enabled.

	IPC20bits.U3TXIP = 1;
	IPC20bits.U3RXIP = 1;

	IFS5bits.U3TXIF = 0;
	IFS5bits.U3RXIF = 0;
	IFS5bits.U3EIF = 0;

	IEC5bits.U3RXIE = 1;
	IEC5bits.U3EIE = 1;

	// DTR
	TRISBbits.TRISB14 = 1;

	// NRESET
	ANSELCbits.ANSC2 = 0;
	TRISCbits.TRISC2 = 0;
	PORTCbits.RC2 = 1;

	// PWRKEY
	ODCAbits.ODCA7 = 1;
	PORTAbits.RA7 = 1;
	TRISAbits.TRISA7 = 0;

	// STATUS
	TRISAbits.TRISA10 = 1;
}

int16_t serial_gsm_read(void *buf, uint16_t count){
	uint16_t len;
	if(!serial_gsm_recv_fifo_len)return(0);
	serial_gsm_critical_enter();
	IEC5bits.U3RXIE = 0;
	for(len = 0; serial_gsm_recv_fifo_len && (len < count) && (len < 32767); len++){
		((uint8_t *)buf)[len] = serial_gsm_recv_fifo[serial_gsm_recv_fifo_out];
		serial_gsm_recv_fifo_len--;
		serial_gsm_recv_fifo_out = (serial_gsm_recv_fifo_out + 1) % SERIAL_GSM_RECV_FIFO_LEN;
	}
	IEC5bits.U3RXIE = 1;
	serial_gsm_critical_exit();
	return(len);
}

int16_t serial_gsm_write(const void *buf, uint16_t count){
	uint16_t len;
	serial_gsm_critical_enter();
	IEC5bits.U3TXIE = 0;
	for(len = 0; (serial_gsm_send_fifo_len < SERIAL_GSM_SEND_FIFO_LEN) && (len < count) && (len < 32767); len++){
		serial_gsm_send_fifo[serial_gsm_send_fifo_in] = ((uint8_t *)buf)[len];
		serial_gsm_send_fifo_len++;
		serial_gsm_send_fifo_in = (serial_gsm_send_fifo_in + 1) % SERIAL_GSM_SEND_FIFO_LEN;
	}
	if(serial_gsm_send_fifo_len){
		IFS5bits.U3TXIF = 1;
		IEC5bits.U3TXIE = 1;
	}
	serial_gsm_critical_exit();
	return(len);
}

int16_t serial_gsm_write_space(void){
	int16_t len;
	serial_gsm_critical_enter();
	IEC5bits.U3TXIE = 0;
	len = (SERIAL_GSM_SEND_FIFO_LEN - serial_gsm_send_fifo_len);
	if(serial_gsm_send_fifo_len)IEC5bits.U3TXIE = 1;
	serial_gsm_critical_exit();
	return(len);
}

int16_t serial_gsm_read_flush(void){
	serial_gsm_critical_enter();
	IEC5bits.U3RXIE = 0;
	serial_gsm_recv_fifo_len = 0;
	serial_gsm_recv_fifo_out = serial_gsm_recv_fifo_in;
	IEC5bits.U3RXIE = 1;
	serial_gsm_critical_exit();
	return(0);
}

void _U3ErrHandler(){
	U3STAbits.OERR = 0;
	U3STAbits.PERR = 0;
	U3STAbits.FERR = 0;
	IFS5bits.U3EIF = 0;
}

void _U3RXHandler(){
	uint16_t c;
	while(U3STAbits.URXDA){
		c = U3RXREG;
		if(serial_gsm_recv_fifo_len < SERIAL_GSM_RECV_FIFO_LEN){
			serial_gsm_recv_fifo[serial_gsm_recv_fifo_in] = (c & 0xFF);
			serial_gsm_recv_fifo_in = (serial_gsm_recv_fifo_in + 1) % SERIAL_GSM_RECV_FIFO_LEN;
			serial_gsm_recv_fifo_len++;
		}
	}
	IFS5bits.U3RXIF = 0;
}

void _U3TXHandler(){
	while(!U3STAbits.UTXBF && serial_gsm_send_fifo_len){
		U3TXREG = serial_gsm_send_fifo[serial_gsm_send_fifo_out];
		serial_gsm_send_fifo_out = (serial_gsm_send_fifo_out + 1) % SERIAL_GSM_SEND_FIFO_LEN;
		serial_gsm_send_fifo_len--;
	}
	if(!serial_gsm_send_fifo_len)IEC5bits.U3TXIE = 0;
	IFS5bits.U3TXIF = 0;
}

