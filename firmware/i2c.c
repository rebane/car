#include "i2c.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"
#include <stdio.h>

// SDA = RC4
// SCL = RC5
// INT = RA9

#define I2C_BUSY_READY          0
#define I2C_BUSY_WAIT           1
#define I2C_BUSY_STOP           2
#define I2C_BUSY_ERROR_STOP     3
#define I2C_BUSY_ERROR          4
#define I2C_BUSY_RECOVER        5

#define I2C_STATE_WRITE_ADDRESS 0
#define I2C_STATE_WRITE_OFFSET  1
#define I2C_STATE_WRITE_DATA    2
#define I2C_STATE_READ_ADDRESS  3
#define I2C_STATE_READ_OFFSET   4
#define I2C_STATE_READ_RESTART  5
#define I2C_STATE_READ_ADDRESS2 6
#define I2C_STATE_READ_QUERY    7
#define I2C_STATE_READ_DATA     8
#define I2C_STATE_READ_ACK      9

static volatile uint8_t i2c_scl, i2c_busy, i2c_state, i2c_address, i2c_offset, i2c_loc, i2c_len, *i2c_data;

static void i2c_recover();

void i2c_debug(){
	printf("I2C DEBUG: %u, %u, %u, %04X\n", (unsigned int)i2c_busy, (unsigned int)i2c_state, (unsigned int)i2c_loc, (unsigned int)I2C1STAT);
}

void i2c_init(){
	IEC1bits.MI2C1IE = 0;
	I2C1CONbits.I2CEN = 0;

	i2c_busy = I2C_BUSY_READY;

	ANSELAbits.ANSA9 = 0;
	TRISAbits.TRISA9 = 1;
	ANSELCbits.ANSC4 = 0;
	ANSELCbits.ANSC5 = 0;
	ODCCbits.ODCC4 = 0;
	ODCCbits.ODCC5 = 0;

	I2C1BRG = 300; // TODO: calculation, using PLATFORM_FCY
	I2C1CON = 0;
	I2C1ADD = 0;
	I2C1MSK = 0;
	IPC4bits.MI2C1IP = 1;
	I2C1CON = (((uint16_t)1) << 15); // same as I2C1CONbits.I2CEN = 1;

	IEC0bits.T3IE = 0;
	T3CON = 0;
	PR3 = (PLATFORM_FCPU / 1000LLU) - 1LLU;
	IPC2bits.T3IP = 1;
}

void i2c_read(uint8_t address, uint8_t offset, void *data, uint8_t len){
	if(I2C1STATbits.BCL){
		i2c_recover();
	}else{
		IEC1bits.MI2C1IE = 0;
		I2C1CON = (((uint16_t)1) << 15);
		i2c_address = address;
		i2c_offset = offset;
		i2c_data = (uint8_t *)data;
		i2c_len = len;
		i2c_loc = 0;
		i2c_busy = I2C_BUSY_WAIT;
		i2c_state = I2C_STATE_READ_ADDRESS;
		IFS1bits.MI2C1IF = 0;
		IEC1bits.MI2C1IE = 1;
		I2C1CONbits.SEN = 1;
	}
}

void i2c_write(uint8_t address, uint8_t offset, void *data, uint8_t len){
	if(I2C1STATbits.BCL){
		i2c_recover();
	}else{
		IEC1bits.MI2C1IE = 0;
		I2C1CON = (((uint16_t)1) << 15);
		i2c_address = address;
		i2c_offset = offset;
		i2c_data = (uint8_t *)data;
		i2c_len = len;
		i2c_loc = 0;
		i2c_busy = I2C_BUSY_WAIT;
		i2c_state = I2C_STATE_WRITE_ADDRESS;
		IFS1bits.MI2C1IF = 0;
		IEC1bits.MI2C1IE = 1;
		I2C1CONbits.SEN = 1;
	}
}

int8_t i2c_ready(){
	IEC1bits.MI2C1IE = 0;
	if(i2c_busy == I2C_BUSY_READY)return(1);
	if(i2c_busy == I2C_BUSY_ERROR)return(-1);
	IEC1bits.MI2C1IE = 1;
	return(0);
}

static void i2c_recover(){
	IEC1bits.MI2C1IE = 0;
	I2C1CON = 0;
	TRISCbits.TRISC4 = 1;
	TRISCbits.TRISC5 = 1;
	i2c_scl = 1;
	TMR3 = 0;
	IFS0bits.T3IF = 0;
	T3CONbits.TON = 1;
	IEC0bits.T3IE = 1;
	i2c_busy = I2C_BUSY_RECOVER;
}

void _MI2C1Handler(){
	uint8_t data;
	if(I2C1STATbits.BCL){
		i2c_recover();
	// ENDING
	}else if(i2c_busy == I2C_BUSY_STOP){
		if(!I2C1CONbits.PEN){
			I2C1CON = (((uint16_t)1) << 15);
			IEC1bits.MI2C1IE = 0;
			i2c_busy = I2C_BUSY_READY;
		}
	}else if(i2c_busy == I2C_BUSY_ERROR_STOP){
		if(!I2C1CONbits.PEN){
			I2C1CON = (((uint16_t)1) << 15);
			IEC1bits.MI2C1IE = 0;
			i2c_busy = I2C_BUSY_ERROR;
		}
	// READ
	}else if(i2c_state == I2C_STATE_READ_ADDRESS){
		if(!I2C1CONbits.SEN){
			I2C1CON = (((uint16_t)1) << 15);
			I2C1TRN = (i2c_address << 1);
			i2c_state = I2C_STATE_READ_OFFSET;
		}
	}else if(i2c_state == I2C_STATE_READ_OFFSET){
		if(!I2C1STATbits.TRSTAT){
			I2C1CON = (((uint16_t)1) << 15);
			if(I2C1STATbits.ACKSTAT){
				I2C1CONbits.PEN = 1;
				i2c_busy = I2C_BUSY_ERROR_STOP;
			}else{
				I2C1TRN = i2c_offset;
				i2c_state = I2C_STATE_READ_RESTART;
			}
		}
	}else if(i2c_state == I2C_STATE_READ_RESTART){
		if(!I2C1STATbits.TRSTAT){
			I2C1CON = (((uint16_t)1) << 15);
			if(I2C1STATbits.ACKSTAT){
				I2C1CONbits.PEN = 1;
				i2c_busy = I2C_BUSY_ERROR_STOP;
			}else{
				I2C1CONbits.RSEN = 1;
				i2c_state = I2C_STATE_READ_ADDRESS2;
			}
		}
	}else if(i2c_state == I2C_STATE_READ_ADDRESS2){
		if(!I2C1CONbits.RSEN){
			I2C1CON = (((uint16_t)1) << 15);
			I2C1TRN = (i2c_address << 1) | 1;
			i2c_state = I2C_STATE_READ_QUERY;
		}
	}else if(i2c_state == I2C_STATE_READ_QUERY){
		if(!I2C1STATbits.TRSTAT){
			I2C1CON = (((uint16_t)1) << 15);
			if(I2C1STATbits.ACKSTAT){
				I2C1CONbits.PEN = 1;
				i2c_busy = I2C_BUSY_ERROR_STOP;
			}else{
				if(i2c_loc >= i2c_len){
					I2C1CONbits.PEN = 1;
					i2c_busy = I2C_BUSY_STOP;
				}else{
					data = I2C1RCV;
					I2C1CONbits.RCEN = 1;
					i2c_state = I2C_STATE_READ_DATA;
				}
			}
		}
	}else if(i2c_state == I2C_STATE_READ_DATA){
		if(I2C1STATbits.RBF){
			I2C1CON = (((uint16_t)1) << 15);
			i2c_data[i2c_loc++] = I2C1RCV;
			if(i2c_loc >= i2c_len)I2C1CONbits.ACKDT = 1;
			I2C1CONbits.ACKEN = 1;
			i2c_state = I2C_STATE_READ_ACK;
		}
	}else if(i2c_state == I2C_STATE_READ_ACK){
		if(!I2C1CONbits.ACKEN){
			if(i2c_loc >= i2c_len){
				I2C1CONbits.PEN = 1;
				i2c_busy = I2C_BUSY_STOP;
			}else{
				data = I2C1RCV;
				I2C1CONbits.RCEN = 1;
				i2c_state = I2C_STATE_READ_DATA;
			}
		}
		
	// WRITE
	}else if(i2c_state == I2C_STATE_WRITE_ADDRESS){
		if(!I2C1CONbits.SEN){
			I2C1CON = (((uint16_t)1) << 15);
			i2c_state = I2C_STATE_WRITE_OFFSET;
			I2C1TRN = (i2c_address << 1);
		}
	}else if(i2c_state == I2C_STATE_WRITE_OFFSET){
		if(!I2C1STATbits.TRSTAT){
			I2C1CON = (((uint16_t)1) << 15);
			if(I2C1STATbits.ACKSTAT){
				i2c_busy = I2C_BUSY_ERROR_STOP;
				I2C1CONbits.PEN = 1;
			}
			i2c_state = I2C_STATE_WRITE_DATA;
			I2C1TRN = i2c_offset;
		}
	}else if(i2c_state == I2C_STATE_WRITE_DATA){
		if(!I2C1STATbits.TRSTAT){
			I2C1CON = (((uint16_t)1) << 15);
			if(I2C1STATbits.ACKSTAT){
				i2c_busy = I2C_BUSY_ERROR_STOP;
				I2C1CONbits.PEN = 1;
			}else{
				if(i2c_loc == i2c_len){
					i2c_busy = I2C_BUSY_STOP;
					I2C1CONbits.PEN = 1;
				}else{
					I2C1TRN = i2c_data[i2c_loc++];
				}
			}
		}
	}
	IFS1bits.MI2C1IF = 0;
}

void _T3Handler(void){
	if(PORTCbits.RC4){
		TRISCbits.TRISC5 = 1;
		T3CONbits.TON = 0;
		IEC0bits.T3IE = 0;
		IEC1bits.MI2C1IE = 0;
		I2C1CON = (((uint16_t)1) << 15);
		i2c_busy = I2C_BUSY_ERROR;
	}else{
		if(i2c_scl){
			TRISCbits.TRISC5 = 0;
			PORTCbits.RC5 = 0;
			i2c_scl = 0;
		}else{
			TRISCbits.TRISC5 = 1;
			i2c_scl = 1;
		}
	}
	IFS0bits.T3IF = 0;
}

