#include "voltage.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"
#include "bjson.h"

// ADC = RA0, RPI16

static uint16_t voltage_adc;

void voltage_init(){
	ANSELAbits.ANSA0 = 1;
	AD1CON1 = 0;
	AD1CON2 = 0;
	AD1CON3 = 0;
	AD1CON4 = 0;
	AD1CHS0 = 0;
	AD1CHS123 = 0;
	AD1CSSH = 0;
	AD1CSSL = 0;

	AD1CON1bits.AD12B = 1;
	AD1CON1bits.SSRC = 7;
	AD1CON3bits.ADCS = 0x0F;
	AD1CHS0bits.CH0SA0 = 1;
	AD1CON1bits.ADON = 1;
	AD1CON1bits.SAMP = 1;
	while(!(AD1CON1bits.DONE));
	voltage_adc = ADC1BUF0;
	AD1CON1bits.DONE = 0;
	AD1CON1bits.SAMP = 1;
}

uint16_t voltage_read(uint8_t *buffer){
	uint16_t l;
	if(AD1CON1bits.DONE){
		voltage_adc = ADC1BUF0;
		AD1CON1bits.DONE = 0;
		AD1CON1bits.SAMP = 1;
	}
	l = 0;
	// (adc * 34500 / 4096) / 107 <-- 34500 = süsteemi pinge, 4096 = max adc väärtus, 107 = pingejaguri kordaja = 120 / (1k + 120)
	l += bjson_string(&buffer[l], "voltage");
	l += bjson_positive(&buffer[l], ((((uint32_t)voltage_adc * 34500LU) / 4096LU) / 107LU));
	return(l);
}

