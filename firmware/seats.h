#ifndef _SEATS_H_
#define _SEATS_H_

#include <stdint.h>

void seats_init();
void seats_poll();
uint16_t seats_read(uint8_t *buffer);

#endif

