#ifndef _SERIAL_485_H_
#define _SERIAL_485_H_

#include <stdint.h>

void serial_485_init();
int16_t serial_485_read(void *buf, uint16_t count);
int16_t serial_485_write(const void *buf, uint16_t count);
int16_t serial_485_write_space(void);
int16_t serial_485_read_flush(void);

#endif

