#ifndef _SPI_FLASH_H_
#define _SPI_FLASH_H_

#include <stdint.h>

void spi_flash_init(uint8_t speed);
void spi_flash_read_start(uint32_t offset, void *buf, uint16_t count);
void spi_flash_write_start(uint32_t offset, const void *buf, uint16_t count);
void spi_flash_erase_start(uint32_t offset);
void spi_flash_mass_erase_start();
uint8_t spi_flash_ready();

#endif

