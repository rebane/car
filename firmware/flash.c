#include "flash.h"
#include "spi_flash.h"
#include <string.h> // memcpy
#include "mtimer.h"

#define FLASH_STATE_READ_START    1
#define FLASH_STATE_READ          2
#define FLASH_STATE_WRITE         3
#define FLASH_STATE_WRITE_DONE    4
#define FLASH_STATE_DELETE_READ   5
#define FLASH_STATE_DELETE        6
#define FLASH_STATE_DELETE_DONE   7

#define FLASH_BUFFER_STATE_EMPTY  0
#define FLASH_BUFFER_STATE_BUSY   1
#define FLASH_BUFFER_STATE_READY1 2
#define FLASH_BUFFER_STATE_READY2 3
#define FLASH_BUFFER_STATE_WAIT1  4
#define FLASH_BUFFER_STATE_WAIT2  5

#define FLASH_TIMEOUT             500
// #define FLASH_FIFO_LEN            (1792)
#define FLASH_FIFO_LEN            (2048)
// #define flash_ptr(fifo)           (1048576LLU + (4096LLU * (fifo)))
#define flash_ptr(fifo)           (4096LLU * (fifo))

static struct{
	uint8_t status[4];
	uint64_t uid;
}__attribute__((packed)) flash_header_buffer;

static struct{
	uint8_t status[4];
	uint64_t uid;
	uint16_t len;
	uint16_t crc;
	uint8_t data[FLASH_DATA_LEN];
}__attribute__((packed)) flash_buffer;

static uint8_t flash_state, flash_buffer_state;
static uint16_t flash_count, flash_loc, flash_remain, flash_delete_loc;
static uint16_t flash_fifo_in, flash_fifo_out, flash_fifo_len;
static uint64_t flash_uid, flash_delete_uid;
static mtimer_t flash_timer;

static uint16_t flash_crc(const void *data, uint16_t len);

void flash_init(){
	uint16_t i;
	uint64_t lowest_correct_uid;
	flash_uid = 0;
	lowest_correct_uid = 0xFFFFFFFFFFFFFFFFLLU;
	flash_remain = 0;
	for(i = 0; i < FLASH_FIFO_LEN; i++){
		spi_flash_read_start(flash_ptr(i), &flash_buffer, 14);
		mtimer_timeout_set(&flash_timer, 2000);
		while(!spi_flash_ready()){
			if(mtimer_timeout(&flash_timer))goto read_done;
		}
		if((flash_buffer.status[0] == 0x66) && (flash_buffer.status[1] == 0x60) && (flash_buffer.uid != 0xFFFFFFFFFFFFFFFFLLU) && (flash_buffer.uid != 0) && (flash_buffer.len <= FLASH_DATA_LEN)){
			if(flash_uid < flash_buffer.uid){
				flash_uid = flash_buffer.uid;
				flash_fifo_in = i;
			}
			if(flash_buffer.status[2]){
				flash_remain++;
				if(lowest_correct_uid > flash_buffer.uid){
					lowest_correct_uid = flash_buffer.uid;
					flash_fifo_out = i;
				}
			}
		}
	}
read_done:
	if(flash_uid){
		flash_fifo_in = (flash_fifo_in + 1) % FLASH_FIFO_LEN;
		if(lowest_correct_uid == 0xFFFFFFFFFFFFFFFFLLU){
			flash_fifo_out = flash_fifo_in;
			flash_fifo_len = 0;
		}else{
			if(flash_fifo_in > flash_fifo_out){
				flash_fifo_len = flash_fifo_in - flash_fifo_out;
			}else{
				flash_fifo_len = FLASH_FIFO_LEN - (flash_fifo_out - flash_fifo_in);
			}
		}
	}else{
		flash_fifo_in = flash_fifo_out = 0;
		flash_fifo_len = 0;
	}
	flash_uid++;
	if(flash_uid == 0xFFFFFFFFFFFFFFFFLLU)flash_uid = 1;
	if(flash_remain > flash_fifo_len)flash_remain = flash_fifo_len;
	spi_flash_init(0);
	flash_buffer_state = FLASH_BUFFER_STATE_EMPTY;
	flash_state = FLASH_STATE_READ_START;
	printf("FLASH, IN: %u, OUT: %u, LEN: %u, REMAIN: %u, UID: %llu\n", (unsigned int)flash_fifo_in, (unsigned int)flash_fifo_out, (unsigned int)flash_fifo_len, (unsigned int)flash_remain, (unsigned long long int)flash_uid);
}

uint8_t flash_read_ready(){
	return((flash_buffer_state == FLASH_BUFFER_STATE_READY1) || (flash_buffer_state == FLASH_BUFFER_STATE_READY2));
}

uint16_t flash_read(void *buf, uint64_t *uid, uint16_t *loc, uint16_t *remain, uint8_t *fresh){
	if((flash_buffer_state != FLASH_BUFFER_STATE_READY1) && (flash_buffer_state != FLASH_BUFFER_STATE_READY2))return(0);
	memcpy(buf, flash_buffer.data, flash_buffer.len);
	*uid = flash_buffer.uid;
	*loc = flash_loc;
	*remain = flash_remain;
	if(flash_buffer_state == FLASH_BUFFER_STATE_READY1){
		*fresh = 0;
		flash_buffer_state = FLASH_BUFFER_STATE_WAIT1;
	}else if(flash_buffer_state == FLASH_BUFFER_STATE_READY2){
		*fresh = 1;
		flash_buffer_state = FLASH_BUFFER_STATE_WAIT2;
	}
	return(flash_buffer.len);
}

uint8_t flash_write_ready(){
	return((flash_state != FLASH_STATE_WRITE) && (flash_state != FLASH_STATE_DELETE_READ) && (flash_state != FLASH_STATE_DELETE));
}

void flash_delete(uint64_t uid, uint16_t loc){
	if(!uid || (uid == 0xFFFFFFFFFFFFFFFFLLU))return;
	flash_delete_loc = loc;
	flash_delete_uid = uid;
	spi_flash_read_start(flash_ptr(flash_delete_loc), &flash_header_buffer, 12);
	mtimer_timeout_set(&flash_timer, FLASH_TIMEOUT);
	if(flash_buffer_state == FLASH_BUFFER_STATE_READY1){
		flash_buffer_state = FLASH_BUFFER_STATE_WAIT1;
	}else if(flash_buffer_state == FLASH_BUFFER_STATE_READY2){
		flash_buffer_state = FLASH_BUFFER_STATE_WAIT2;
	}
	flash_state = FLASH_STATE_DELETE_READ;
}

void flash_write(const void *buf, uint16_t count){
	if(count > FLASH_DATA_LEN)return;
	spi_flash_erase_start(flash_ptr(flash_fifo_in));
	flash_buffer_state = FLASH_BUFFER_STATE_BUSY;
	flash_buffer.status[0] = 0x66;
	flash_buffer.status[1] = 0x60;
	flash_buffer.status[2] = 0xFF;
	flash_buffer.status[3] = 0xFF;
	flash_buffer.uid = flash_uid++;
	if(flash_uid == 0xFFFFFFFFFFFFFFFFLLU)flash_uid = 1;
	flash_buffer.len = count;
	flash_buffer.crc = flash_crc(buf, count);
	memcpy(flash_buffer.data, buf, count);
	flash_count = ((count + 16 - 1) >> 8) + 1;
	flash_loc = 0;
	mtimer_timeout_set(&flash_timer, FLASH_TIMEOUT);
	flash_state = FLASH_STATE_WRITE;
}

void flash_poll(){
	if(flash_state == FLASH_STATE_READ_START){
		if(flash_fifo_len && (flash_buffer_state == FLASH_BUFFER_STATE_EMPTY)){
			spi_flash_read_start(flash_ptr(flash_fifo_out), &flash_buffer, 4096);
			mtimer_timeout_set(&flash_timer, FLASH_TIMEOUT);
			flash_state = FLASH_STATE_READ;
		}
	}
	if(flash_state == FLASH_STATE_READ){
		if(spi_flash_ready()){
			if((flash_buffer.status[0] == 0x66) && (flash_buffer.status[1] == 0x60) && flash_buffer.status[2] && (flash_buffer.uid != 0xFFFFFFFFFFFFFFFFLLU) && (flash_buffer.uid != 0) && (flash_buffer.len <= FLASH_DATA_LEN) && (flash_buffer.crc == flash_crc(flash_buffer.data, flash_buffer.len))){
				flash_loc = flash_fifo_out;
				flash_buffer_state = FLASH_BUFFER_STATE_READY1;
			}else{
				flash_fifo_out = (flash_fifo_out + 1) % FLASH_FIFO_LEN;
				if(flash_fifo_len)flash_fifo_len--;
				if(!flash_fifo_len)flash_fifo_out = flash_fifo_in;
			}
			flash_state = FLASH_STATE_READ_START;
		}
		if(mtimer_timeout(&flash_timer)){
			flash_fifo_out = (flash_fifo_out + 1) % FLASH_FIFO_LEN;
			if(flash_fifo_len)flash_fifo_len--;
			if(!flash_fifo_len)flash_fifo_out = flash_fifo_in;
			flash_state = FLASH_STATE_READ_START;
		}
	}
	// WRITE
	if(flash_state == FLASH_STATE_WRITE){
		if(spi_flash_ready()){
			if(flash_loc >= flash_count){
				flash_state = FLASH_STATE_WRITE_DONE;
			}else{
				spi_flash_write_start((flash_ptr(flash_fifo_in) + (256 * flash_loc)), (uint8_t *)&flash_buffer + (256 * flash_loc), 256);
				flash_loc++;
			}
		}
		if(mtimer_timeout(&flash_timer)){
			flash_state = FLASH_STATE_WRITE_DONE;
		}
	}
	if(flash_state == FLASH_STATE_WRITE_DONE){
		flash_loc = flash_fifo_in;
		flash_fifo_in = (flash_fifo_in + 1) % FLASH_FIFO_LEN;
		flash_fifo_len++;
		if(flash_fifo_len > FLASH_FIFO_LEN){
			flash_fifo_out = flash_fifo_in;
			flash_fifo_len = FLASH_FIFO_LEN;
		}
		flash_remain++;
		if(flash_remain > flash_fifo_len)flash_remain = flash_fifo_len;
		flash_buffer_state = FLASH_BUFFER_STATE_READY2;
		flash_state = FLASH_STATE_READ_START;
	}
	// DELETE
	if(flash_state == FLASH_STATE_DELETE_READ){
		if(spi_flash_ready()){
			if(flash_header_buffer.uid == flash_delete_uid){
				spi_flash_write_start(flash_ptr(flash_delete_loc) + 2, "", 1);
				mtimer_timeout_set(&flash_timer, FLASH_TIMEOUT);
				flash_state = FLASH_STATE_DELETE;
			}else{
				flash_state = FLASH_STATE_DELETE_DONE;
			}
		}
		if(mtimer_timeout(&flash_timer)){
			flash_state = FLASH_STATE_DELETE_DONE;
		}
	}
	if(flash_state == FLASH_STATE_DELETE){
		if(spi_flash_ready()){
			if(flash_remain)flash_remain--;
			flash_state = FLASH_STATE_DELETE_DONE;
		}
		if(mtimer_timeout(&flash_timer)){
			flash_state = FLASH_STATE_DELETE_DONE;
		}
	}
	if(flash_state == FLASH_STATE_DELETE_DONE){
		if(flash_buffer.uid == flash_delete_uid){
			if(flash_buffer_state == FLASH_BUFFER_STATE_WAIT1){
				flash_fifo_out = (flash_fifo_out + 1) % FLASH_FIFO_LEN;
				if(flash_fifo_len)flash_fifo_len--;
				if(!flash_fifo_len)flash_fifo_out = flash_fifo_in;
				flash_buffer_state = FLASH_BUFFER_STATE_EMPTY;
			}else if(flash_buffer_state == FLASH_BUFFER_STATE_WAIT2){
				flash_buffer_state = FLASH_BUFFER_STATE_EMPTY;
			}
		}else{
			if(flash_buffer_state == FLASH_BUFFER_STATE_WAIT1){
				flash_buffer_state = FLASH_BUFFER_STATE_READY1;
			}else if(flash_buffer_state == FLASH_BUFFER_STATE_WAIT2){
				flash_buffer_state = FLASH_BUFFER_STATE_READY2;
			}
		}
		flash_state = FLASH_STATE_READ_START;
	}
}

static uint16_t flash_crc(const void *data, uint16_t len){
	uint16_t i, crc;
	uint8_t j, d;
	crc = 0;
	for(i = 0; i < len; i++){
		d = ((uint8_t *)data)[i];
		for(j = 0; j < 8; j++){
			if((crc ^ d) & 0x01){
				crc ^= 0x4002;
				crc >>= 1;
				crc |= 0x8000;
			}else{
				crc >>= 1;
			}
			d >>= 1;
		}
	}
	return(crc);
}

