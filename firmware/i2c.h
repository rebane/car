#ifndef _I2C_H_
#define _I2C_H_

#include <stdint.h>

void i2c_init();
void i2c_read(uint8_t address, uint8_t offset, void *data, uint8_t len);
void i2c_write(uint8_t address, uint8_t offset, void *data, uint8_t len);
int8_t i2c_ready();

void i2c_test();

#endif

