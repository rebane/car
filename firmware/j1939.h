#ifndef _J1939_H_
#define _J1939_H_

#include <stdint.h>

void j1939_init();
void j1939_poll();
uint16_t j1939_read(uint8_t *buffer);

#endif

