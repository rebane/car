#include "mtimer.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"

volatile uint32_t mtimer_sec;
volatile uint16_t mtimer_msec;

void mtimer_init(){
	__asm__("disi #64");
	IEC0bits.T1IE = 0;
	mtimer_sec = 0;
	mtimer_msec = 0;
	T1CON = 0;
	TMR1 = 0;
	PR1 = (PLATFORM_FCPU / 1000LLU) - 1LLU;
	T1CONbits.TON = 1;
	IPC0bits.T1IP = 1;
	IFS0bits.T1IF = 0;
	IEC0bits.T1IE = 1;
}

uint32_t mtimer_get(mtimer_t *mtimer){
	uint32_t _sec;
	uint16_t _msec;
	// IEC0bits.T1IE = 0;
	INTCON2bits.GIE = 0;
	_sec = mtimer_sec;
	_msec = mtimer_msec;
	// IEC0bits.T1IE = 1;
	INTCON2bits.GIE = 1;
	if(mtimer != ((void *)0)){
		mtimer->sec = _sec;
		mtimer->msec = _msec;
	}
	return(_sec);
}

int8_t mtimer_compare(mtimer_t *mtimer1, mtimer_t *mtimer2){ // 1 if mtimer1 > mtimer2, 0 if =, -1 if mtimer1 < mtimer2
	if(mtimer1->sec > mtimer2->sec)return(1);
	if(mtimer1->sec < mtimer2->sec)return(-1);
	if(mtimer1->msec > mtimer2->msec)return(1);
	if(mtimer1->msec < mtimer2->msec)return(-1);
	return(0);
}

void mtimer_timeout_set(mtimer_t *mtimer, uint32_t msec){
	mtimer_get(mtimer);
	mtimer->sec += (msec / 1000);
	mtimer->msec += (msec % 1000);
	if(mtimer->msec >= 1000){
		mtimer->msec -= 1000;
		mtimer->sec++;
	}
}

uint8_t mtimer_timeout(mtimer_t *mtimer){
	mtimer_t t;
	mtimer_get(&t);
	if(mtimer_compare(&t, mtimer) >= 0)return(1);
	return(0);
}

void mtimer_sleep(uint32_t msec){
	volatile uint8_t i;
	mtimer_t mtimer;
	mtimer_timeout_set(&mtimer, msec);
	while(1){
		if(mtimer_timeout(&mtimer))return;
		for(i = 0; i < 100; i++)__asm__("nop");
	}
}

void _T1Handler(void){
	mtimer_msec++;
	if(mtimer_msec == 1000){
		mtimer_sec++;
		mtimer_msec = 0;
	}
	IFS0bits.T1IF = 0;
}

