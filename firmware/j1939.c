#include "j1939.h"
#include <stdio.h>
#include "mtimer.h"
#include "can.h"
#include "bjson.h"

#define J1939_UNUSED_PGN 666

static struct{
	uint16_t pgn;
	uint8_t data[8];
	mtimer_t timer;
}j1939_status[] = {
//	{ 0 },     // Torque/Speed Control 1
//	{ 60160 }, // * Transport Protocol - Data Transfer
//	{ 60416 }, // * Transport Protocol - Connection Mgmt
//	{ 61184 }, // Proprietary A
//	{ 61440 }, // Electronic Retarder Controller 1
//	{ 61441 }, // Electronic Brake Controller 1
//	{ 61443 }, // Expanded Freeze Frame
//	{ 61444 }, // Electronic Engine Controller 1
//	{ 65132 }, // Tachograph
//	{ 65215 }, // Wheel Speed Information (relative)
	{ 65217 }, // High Resolution Vehicle Distance
//	{ 65247 }, // Aftertreatment 2 Intake Gas 1
//	{ 65254 }, // Time/Date
	{ 65257 }, // Fuel Consumption (Liquid)
	{ 65262 }, // Engine Temperature 1
	{ 65265 }, // Cruise Control/Vehicle Speed
	{ 65266 }, // Fuel Economy (Liquid)
//	{ 65270 }, // Inlet/Exhaust Conditions 1
//	{ 65344 }, // Proprietary B
	{ J1939_UNUSED_PGN }
};

static void j1939_set(uint16_t pgn, uint8_t *data);

void j1939_init(){
	uint8_t i;
	for(i = 0; j1939_status[i].pgn != J1939_UNUSED_PGN; i++){
		mtimer_timeout_set(&j1939_status[i].timer, 0);
	}
}

void j1939_poll(){
	struct can_frame frame;
	uint32_t /*p, edp,*/ dp, pf, ps, /*sa,*/ pgn;
	if(can_read(&frame) > 0){
		if((frame.can_id & CAN_EFF_FLAG) && (frame.can_dlc == 8)){
			// p = (frame.can_id >> 26) & 0x07;
			// edp = (frame.can_id >> 25) & 0x01;
			dp = (frame.can_id >> 24) & 0x01;
			pf = (frame.can_id >> 16) & 0xFF;
			ps = (frame.can_id >> 8) & 0xFF;
			// sa = (frame.can_id >> 0) & 0xFF;
			if(pf < 240){
				pgn = (dp << 9) + (pf << 8);
			}else{
				pgn = (dp << 9) + (pf << 8) + ps;
			}
			j1939_set(pgn, frame.data);
		}
	}
}

uint16_t j1939_read(uint8_t *buffer){
	char name[16];
	uint8_t i;
	uint16_t l;
	l = 0;
	for(i = 0; j1939_status[i].pgn != J1939_UNUSED_PGN; i++){
		if(!mtimer_timeout(&j1939_status[i].timer)){
			snprintf(name, 15, "j1939_pgn%u", (unsigned int)j1939_status[i].pgn);
			name[15] = 0;
			l += bjson_string(&buffer[l], name);
			l += bjson_binary(&buffer[l], j1939_status[i].data, 8);
		}
	}
	return(l);
}

static void j1939_set(uint16_t pgn, uint8_t *data){
	uint8_t i;
	for(i = 0; j1939_status[i].pgn != J1939_UNUSED_PGN; i++){
		if(j1939_status[i].pgn == pgn){
			j1939_status[i].data[0] = data[0];
			j1939_status[i].data[1] = data[1];
			j1939_status[i].data[2] = data[2];
			j1939_status[i].data[3] = data[3];
			j1939_status[i].data[4] = data[4];
			j1939_status[i].data[5] = data[5];
			j1939_status[i].data[6] = data[6];
			j1939_status[i].data[7] = data[7];
			mtimer_timeout_set(&j1939_status[i].timer, 30000);
			return;
		}
	}
}

