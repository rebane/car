#ifndef _MTIMER_H_
#define _MTIMER_H_

#include <stdint.h>

struct mtimer_t{
	uint32_t sec;
	uint16_t msec;
};

typedef struct mtimer_t mtimer_t;

extern volatile uint32_t mtimer_sec;
extern volatile uint16_t mtimer_msec;

void mtimer_init();
uint32_t mtimer_get(mtimer_t *mtimer);
int8_t mtimer_compare(mtimer_t *mtimer1, mtimer_t *mtimer2);
void mtimer_timeout_set(mtimer_t *mtimer, uint32_t msec);
uint8_t mtimer_timeout(mtimer_t *mtimer);
void mtimer_sleep(uint32_t msec);

#endif

