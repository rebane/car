#ifndef _DATA_H_
#define _DATA_H_

#include <stdint.h>

void data_init();
void data_poll();

uint16_t data_read(uint8_t *buffer);

#endif

