#include "lcp.h"
#include <stdint.h>
#include <stdio.h>
#include "mtimer.h"
#include "ppp.h"
#include "ahdlc.h"

// #define LCP_DEBUG

static uint8_t *lcp_write_buffer, lcp_local_identifier, lcp_code, lcp_auth;
static uint8_t lcp_ok_remote, lcp_ok_local;
static uint16_t lcp_write_len;
static mtimer_t lcp_timer;

static void lcp_parse(uint8_t code, uint8_t identifier, uint8_t *data, uint16_t len);
static uint8_t lcp_check_configuration(uint8_t type, uint8_t *data, uint8_t len);
static void lcp_parse_configuration(uint8_t *data, uint16_t len, uint8_t(*callback)(uint8_t type, uint8_t *data, uint8_t len));

#ifdef LCP_DEBUG
#include "ansi_colors.h"
static void lcp_debug(uint8_t tofrom, uint8_t *data, uint16_t len);
static uint8_t lcp_debug_configuration(uint8_t type, uint8_t *data, uint8_t len);
#else
#define lcp_debug(...)
#define lcp_debug_configuration(...)
#endif

void lcp_init(){
	lcp_write_buffer = &ahdlc_write_buffer[2];
}

void lcp_reset(){
	lcp_ok_remote = 0;
	lcp_ok_local = 0;
	lcp_local_identifier = 0;
	mtimer_timeout_set(&lcp_timer, 0);
	lcp_auth = 0;
}

void lcp_poll(){
	uint16_t i;
	if((!lcp_ok_local || !lcp_ok_remote) && mtimer_timeout(&lcp_timer)){
		mtimer_timeout_set(&lcp_timer, 5000);
		i = 4;
		if(lcp_auth){
			lcp_write_buffer[i++] = 3;
			lcp_write_buffer[i++] = 4;
			lcp_write_buffer[i++] = 0xC0;
			lcp_write_buffer[i++] = 0x23;
		}
		lcp_write(LCP_CODE_CONFIGURE_REQUEST, lcp_local_identifier, &lcp_write_buffer[4], i - 4);
	}
}

void lcp_read(uint8_t *data, uint16_t len){
	uint16_t i;
	lcp_debug(0, data, len);
	if(len < 4)return;
	i = (((uint16_t)data[2] << 8) | data[3]);
	if((i < 4) || (i > len))return;
	lcp_code = data[0];
	lcp_parse(data[0], data[1], &data[4], i - 4);
}

void lcp_write(uint8_t code, uint8_t identifier, uint8_t *data, uint16_t len){
	uint16_t i;
	lcp_write_buffer[0] = code;
	lcp_write_buffer[1] = identifier;
	if(data != &lcp_write_buffer[4])for(i = 0; i < len; i++)lcp_write_buffer[i + 4] = data[i];
	i = len + 4;
	lcp_write_buffer[2] = i >> 8;
	lcp_write_buffer[3] = i & 0xFF;
	lcp_debug(1, lcp_write_buffer, i);
	ppp_write(0xC021, lcp_write_buffer, i);
}

void lcp_set_auth(){
	lcp_auth = 1;
}

uint8_t lcp_get_auth(){
	return(lcp_auth);
}

uint8_t lcp_ok(){
	return(lcp_ok_remote && lcp_ok_local);
}

static void lcp_parse(uint8_t code, uint8_t identifier, uint8_t *data, uint16_t len){
	if(code == LCP_CODE_CONFIGURE_REQUEST){
		lcp_write_len = 0;
		lcp_parse_configuration(data, len, lcp_check_configuration);
		if(lcp_write_len){
			lcp_write(LCP_CODE_CONFIGURE_REJECT, identifier, &lcp_write_buffer[4], lcp_write_len);
		}else{
			lcp_write(LCP_CODE_CONFIGURE_ACK, identifier, data, len);
			lcp_ok_remote = 1;
		}
	}else if(code == LCP_CODE_CONFIGURE_ACK){
		if(identifier == lcp_local_identifier)lcp_ok_local = 1;
	}else if(code == LCP_CODE_ECHO_REQUEST){
		lcp_write(LCP_CODE_ECHO_REPLY, identifier, data, len);
	}
}

static uint8_t lcp_check_configuration(uint8_t type, uint8_t *data, uint8_t len){
	uint8_t i;
	if(lcp_code == LCP_CODE_CONFIGURE_REQUEST){
		if((type == 3) && (len == 2)){
			lcp_auth = 1;
		}else{
			lcp_write_buffer[4 + lcp_write_len] = type;
			lcp_write_buffer[5 + lcp_write_len] = len + 2;
			for(i = 0; i < len; i++)lcp_write_buffer[6 + lcp_write_len + i] = data[i];
			lcp_write_len += len + 2;
		}
	}
	return(0);
}

static void lcp_parse_configuration(uint8_t *data, uint16_t len, uint8_t(*callback)(uint8_t type, uint8_t *data, uint8_t len)){
	while((len > 1) && (data[1] > 1) && (len >= data[1])){
		if(callback(data[0], &data[2], data[1] - 2))break;
		len -= data[1];
		data += data[1];
	}
}

#ifdef LCP_DEBUG
static void lcp_debug(uint8_t tofrom, uint8_t *data, uint16_t len){
	uint16_t i;
	if(!tofrom){
		printf("%s    LCP: GOT PACKET:", ANSI_GREEN);
	}else{
		printf("%s    LCP: SENT PACKET:", ANSI_RED);
	}
	for(i = 0; i < len; i++){
		printf(" %02X", (unsigned int)data[i]);
	}
	printf("\n");
	if(len < 4){
		printf("    INVALID PACKET\n");
		goto cleanup;
	}
	if(data[0] == 1){
		printf("    CONFIGURE/REQUEST\n");
	}else if(data[0] == 2){
		printf("    CONFIGURE/ACK\n");
	}else if(data[0] == 3){
		printf("    CONFIGURE/NAK\n");
	}else if(data[0] == 4){
		printf("    CONFIGURE/REJECT\n");
	}else if(data[0] == 5){
		printf("    TERMINATE/REQUEST\n");
	}else if(data[0] == 6){
		printf("    TERMINATE/ACK\n");
	}else if(data[0] == 7){
		printf("    CODE/REJECT\n");
	}else if(data[0] == 8){
		printf("    PROTOCOL/REJECT\n");
	}else if(data[0] == 9){
		printf("    ECHO/REQUEST\n");
	}else if(data[0] == 10){
		printf("    ECHO/REPLY\n");
	}else if(data[0] == 11){
		printf("    DISCARD/REQUEST\n");
	}
	printf("    IDENTIFIER: %u\n", (unsigned int)data[1]);
	i = (((uint16_t)data[2] << 8) | data[3]);
	printf("    LENGTH: %u\n", (unsigned int)i);
	if(i > len){
		printf("    INVALID PACKET\n");
		goto cleanup;
	}
	lcp_parse_configuration(&data[4], len - 4, lcp_debug_configuration);
cleanup:
	printf("%s", ANSI_RESET);
}

static uint8_t lcp_debug_configuration(uint8_t type, uint8_t *data, uint8_t len){
	uint8_t i;
	printf("      TYPE: %u\n", (unsigned int)type);
	if(len){
		printf("        DATA:");
		for(i = 0; i < len; i++){
			printf(" %02X", data[i]);
		}
		printf("\n");
	}
	return(0);
}
#endif

