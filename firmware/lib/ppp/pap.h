#ifndef _PAP_H_
#define _PAP_H_

#include <stdint.h>

void pap_init();
void pap_reset();
void pap_poll(char *user, char *pass);
void pap_read(uint8_t *data, uint16_t len);
void pap_set_auth();
uint8_t pap_ok();

#endif

