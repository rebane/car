#ifndef _LCP_H_
#define _LCP_H_

#include <stdint.h>

#define LCP_CODE_VENDOR            0
#define LCP_CODE_CONFIGURE_REQUEST 1
#define LCP_CODE_CONFIGURE_ACK     2
#define LCP_CODE_CONFIGURE_NAK     3
#define LCP_CODE_CONFIGURE_REJECT  4
#define LCP_CODE_TERMINATE_REQUEST 5
#define LCP_CODE_TERMINATE_ACK     6
#define LCP_CODE_CODE_REJECT       7
#define LCP_CODE_PROTOCOL_REJECT   8
#define LCP_CODE_ECHO_REQUEST      9
#define LCP_CODE_ECHO_REPLY        10
#define LCP_CODE_DISCARD_REQUEST   11

void lcp_init();
void lcp_reset();
void lcp_poll();
void lcp_read(uint8_t *data, uint16_t len);
void lcp_write(uint8_t code, uint8_t identifier, uint8_t *data, uint16_t len);
void lcp_set_auth();
uint8_t lcp_get_auth();
uint8_t lcp_ok();

#endif

