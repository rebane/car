#ifndef _IPCP_H_
#define _IPCP_H_

#include <stdint.h>

#define IPCP_CODE_VENDOR            0
#define IPCP_CODE_CONFIGURE_REQUEST 1
#define IPCP_CODE_CONFIGURE_ACK     2
#define IPCP_CODE_CONFIGURE_NAK     3
#define IPCP_CODE_CONFIGURE_REJECT  4
#define IPCP_CODE_TERMINATE_REQUEST 5
#define IPCP_CODE_TERMINATE_ACK     6
#define IPCP_CODE_CODE_REJECT       7

extern uint8_t ipcp_ip_remote[4], ipcp_ip_local[4], ipcp_dns1[4], ipcp_dns2[4];

void ipcp_init();
void ipcp_reset();
void ipcp_poll();
void ipcp_read(uint8_t *data, uint16_t len);
void ipcp_write(uint8_t code, uint8_t identifier, uint8_t *data, uint16_t len);
uint8_t ipcp_ok();

#endif

