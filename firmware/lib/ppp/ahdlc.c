#include "ahdlc.h"
#include <stdint.h>
#include "ppp.h"
#include <stdio.h>

// #define AHDLC_DEBUG

void ppp_arch_putchar(uint8_t c);

uint8_t ahdlc_write_buffer[AHDLC_WRITE_LEN] /*__attribute__((section(".ccm")))*/;

static uint8_t ahdlc_read_buffer[AHDLC_READ_LEN] /*__attribute__((section(".ccm")))*/;
static uint16_t ahdlc_read_loc, ahdlc_read_crc, ahdlc_write_crc;
static uint8_t ahdlc_read_esc;

static void ahdlc_read_init();
static void ahdlc_write_byte(uint8_t c);
static uint16_t ahdlc_crc(uint16_t crc, uint8_t c);

#ifdef AHDLC_DEBUG
#include "ansi_colors.h"
static void ahdlc_debug(uint8_t tofrom, uint8_t *data, uint16_t len);
#else
#define ahdlc_debug(...)
#endif

void ahdlc_init(){
	ahdlc_read_init();
}

void ahdlc_reset(){
	ahdlc_read_init();
}

void ahdlc_poll(){
}

static void ahdlc_read_init(){
	ahdlc_read_loc = 0;
	ahdlc_read_esc = 0;
	ahdlc_read_crc = 0xFFFF;
}

void ahdlc_read_byte(uint8_t c){
//	uint16_t i;
	if(ahdlc_read_esc){
		if(c == 0x7E){
			ahdlc_read_init();
			return;
		}
		ahdlc_read_esc = 0;
		c = c ^ 0x20;
	}else if(c == 0x7E){
/*		if((ahdlc_read_loc > 1) && (ahdlc_read_crc == 0xF0B8)){
			printf("GOT AHDLC PACKET (%u):", (unsigned int)ahdlc_read_loc);
			for(i = 0; i < ahdlc_read_loc; i++){
				printf(" %02X", (unsigned int)ahdlc_read_buffer[i]);
			}
			printf("\n");
		}*/
		if((ahdlc_read_loc < 2) || (ahdlc_read_crc != 0xF0B8)){
			ahdlc_read_init();
//			if(ahdlc_read_loc > 1)printf("WRONG AHDLC PACKET\n");
			return;
		}
//		printf("AHDLC CRC OK\n");
		ahdlc_debug(0, ahdlc_read_buffer, ahdlc_read_loc);
		if((ahdlc_read_buffer[0] == 0xFF) && (ahdlc_read_buffer[1] == 0x03)){
			ppp_read(&ahdlc_read_buffer[2], ahdlc_read_loc - 4);
		}else{
			ppp_read(&ahdlc_read_buffer[0], ahdlc_read_loc - 2);
		}
		ahdlc_read_init();
		return;
	}else if(c == 0x7D){
		ahdlc_read_esc = 1;
		return;
	}
	if(ahdlc_read_loc < AHDLC_READ_LEN){
		ahdlc_read_buffer[ahdlc_read_loc++] = c;
		ahdlc_read_crc = ahdlc_crc(ahdlc_read_crc, c);
	}else{
		ahdlc_read_init();
	}
}

void ahdlc_write(uint8_t *data, uint16_t len){
	uint16_t i;
	ppp_arch_putchar(0x7E);
	ahdlc_write_crc = 0xFFFF;
	ahdlc_write_byte(0xFF);
	ahdlc_write_byte(0x03);
	for(i = 0; i < len; i++) {
		ahdlc_write_byte(data[i]);
	}
	i = ahdlc_write_crc ^ 0xFFFF;
	ahdlc_write_byte((uint8_t)(i & 0xFF));
	ahdlc_write_byte((uint8_t)((i >> 8) & 0xFF));
	ppp_arch_putchar(0x7E);
#ifdef AHDLC_DEBUG
	printf("%sAHDLC: SENT\n%s", ANSI_RED, ANSI_RESET);
#endif
}

void ahdlc_send(uint8_t *data, uint16_t len){
	uint16_t i;
	ppp_arch_putchar(0x7E);
	ahdlc_write_crc = 0xFFFF;
	ahdlc_write_byte(0xFF);
	ahdlc_write_byte(0x03);
	ahdlc_write_byte(0x00);
	ahdlc_write_byte(0x21);
	for(i = 0; i < len; i++) {
		ahdlc_write_byte(data[i]);
	}
	i = ahdlc_write_crc ^ 0xFFFF;
	ahdlc_write_byte((uint8_t)(i & 0xFF));
	ahdlc_write_byte((uint8_t)((i >> 8) & 0xFF));
	ppp_arch_putchar(0x7E);
#ifdef AHDLC_DEBUG
	printf("%sAHDLC: SENT\n%s", ANSI_RED, ANSI_RESET);
#endif
}

static void ahdlc_write_byte(uint8_t c){
	ahdlc_write_crc = ahdlc_crc(ahdlc_write_crc, c);
	if((c == 0x7D) || (c == 0x7E) || (c < 0x20)){
		ppp_arch_putchar(0x7D);
		c ^= 0x20;
	}
	ppp_arch_putchar(c);
}

static uint16_t ahdlc_crc(uint16_t crc, uint8_t c){
	uint16_t b;
	b = (crc ^ c) & 0xFF;
	b = (b ^ (b << 4)) & 0xFF;
	b = (b << 8) ^ (b << 3) ^ (b >> 4);
	return((crc >> 8) ^ b);
}

#ifdef AHDLC_DEBUG
static void ahdlc_debug(uint8_t tofrom, uint8_t *data, uint16_t len){
	uint16_t i;
	if(!tofrom){
		printf("%sAHDLC: GOT PACKET:", ANSI_GREEN);
	}else{
		printf("%sAHDLC: SENT PACKET:", ANSI_RED);
	}
	for(i = 0; i < len; i++){
		printf(" %02X", (unsigned int)data[i]);
	}
	printf("\n%s", ANSI_RESET);
}
#endif

