#include "ipcp.h"
#include <stdint.h>
#include <stdio.h>
#include "mtimer.h"
#include "ppp.h"
#include "ahdlc.h"

// #define IPCP_DEBUG

uint8_t ipcp_ip_remote[4], ipcp_ip_local[4], ipcp_dns1[4], ipcp_dns2[4];

static uint8_t *ipcp_write_buffer, ipcp_local_identifier, ipcp_code;
static uint8_t ipcp_ok_remote, ipcp_ok_local;
static uint8_t ipcp_dns1_ask, ipcp_dns2_ask;
static uint16_t ipcp_write_len;
static mtimer_t ipcp_timer;

static void ipcp_parse(uint8_t code, uint8_t identifier, uint8_t *data, uint16_t len);
static uint8_t ipcp_check_configuration(uint8_t type, uint8_t *data, uint8_t len);
static void ipcp_parse_configuration(uint8_t *data, uint16_t len, uint8_t(*callback)(uint8_t type, uint8_t *data, uint8_t len));

#ifdef IPCP_DEBUG
#include "ansi_colors.h"
static void ipcp_debug(uint8_t tofrom, uint8_t *data, uint16_t len);
static uint8_t ipcp_debug_configuration(uint8_t type, uint8_t *data, uint8_t len);
#else
#define ipcp_debug(...)
#define ipcp_debug_configuration(...)
#endif

void ipcp_init(){
	ipcp_write_buffer = &ahdlc_write_buffer[2];
}

void ipcp_reset(){
	uint8_t i;
	ipcp_ok_remote = 0;
	ipcp_ok_local = 0;
	for(i = 0; i < 4; i++)ipcp_ip_remote[i] = ipcp_ip_local[i] = ipcp_dns1[i] = ipcp_dns2[i] = 0;
	ipcp_local_identifier = 0;
	mtimer_timeout_set(&ipcp_timer, 0);
	ipcp_dns1_ask = 1;
	ipcp_dns2_ask = 1;
}

void ipcp_poll(){
	uint16_t i;
	if((!ipcp_ok_local || !ipcp_ok_remote) && mtimer_timeout(&ipcp_timer)){
		mtimer_timeout_set(&ipcp_timer, 5000);
		i = 4;
		ipcp_write_buffer[i++] = 3;
		ipcp_write_buffer[i++] = 6;
		ipcp_write_buffer[i++] = ipcp_ip_local[0];
		ipcp_write_buffer[i++] = ipcp_ip_local[1];
		ipcp_write_buffer[i++] = ipcp_ip_local[2];
		ipcp_write_buffer[i++] = ipcp_ip_local[3];
		if(ipcp_dns1_ask){
			ipcp_write_buffer[i++] = 129;
			ipcp_write_buffer[i++] = 6;
			ipcp_write_buffer[i++] = ipcp_dns1[0];
			ipcp_write_buffer[i++] = ipcp_dns1[1];
			ipcp_write_buffer[i++] = ipcp_dns1[2];
			ipcp_write_buffer[i++] = ipcp_dns1[3];
		}
		if(ipcp_dns2_ask){
			ipcp_write_buffer[i++] = 131;
			ipcp_write_buffer[i++] = 6;
			ipcp_write_buffer[i++] = ipcp_dns2[0];
			ipcp_write_buffer[i++] = ipcp_dns2[1];
			ipcp_write_buffer[i++] = ipcp_dns2[2];
			ipcp_write_buffer[i++] = ipcp_dns2[3];
		}
		ipcp_write(IPCP_CODE_CONFIGURE_REQUEST, ipcp_local_identifier, &ipcp_write_buffer[4], i - 4);
	}
}

void ipcp_read(uint8_t *data, uint16_t len){
	uint16_t i;
	ipcp_debug(0, data, len);
	if(len < 4)return;
	i = (((uint16_t)data[2] << 8) | data[3]);
	if((i < 4) || (i > len))return;
	ipcp_code = data[0];
	ipcp_parse(data[0], data[1], &data[4], i - 4);
}

void ipcp_write(uint8_t code, uint8_t identifier, uint8_t *data, uint16_t len){
	uint16_t i;
	ipcp_write_buffer[0] = code;
	ipcp_write_buffer[1] = identifier;
	if(data != &ipcp_write_buffer[4])for(i = 0; i < len; i++)ipcp_write_buffer[i + 4] = data[i];
	i = len + 4;
	ipcp_write_buffer[2] = i >> 8;
	ipcp_write_buffer[3] = i & 0xFF;
	ipcp_debug(1, ipcp_write_buffer, i);
	ppp_write(0x8021, ipcp_write_buffer, i);
}

uint8_t ipcp_ok(){
	return(ipcp_ok_remote && ipcp_ok_local);
}

static void ipcp_parse(uint8_t code, uint8_t identifier, uint8_t *data, uint16_t len){
	if(code == IPCP_CODE_CONFIGURE_REQUEST){
		ipcp_write_len = 0;
		ipcp_parse_configuration(data, len, ipcp_check_configuration);
		if(ipcp_write_len){
			ipcp_write(IPCP_CODE_CONFIGURE_REJECT, identifier, &ipcp_write_buffer[4], ipcp_write_len);
		}else{
			ipcp_write(IPCP_CODE_CONFIGURE_ACK, identifier, data, len);
			ipcp_ok_remote = 1;
		}
	}else if(code == IPCP_CODE_CONFIGURE_NAK){
		ipcp_parse_configuration(data, len, ipcp_check_configuration);
	}else if(code == IPCP_CODE_CONFIGURE_REJECT){
		ipcp_parse_configuration(data, len, ipcp_check_configuration);
	}else if(code == IPCP_CODE_CONFIGURE_ACK){
		if(identifier == ipcp_local_identifier)ipcp_ok_local = 1;
	}
}

static uint8_t ipcp_check_configuration(uint8_t type, uint8_t *data, uint8_t len){
	uint8_t i;
	if(ipcp_code == IPCP_CODE_CONFIGURE_REQUEST){
		if((type == 3) && (len == 4)){
			ipcp_ip_remote[0] = data[0];
			ipcp_ip_remote[1] = data[1];
			ipcp_ip_remote[2] = data[2];
			ipcp_ip_remote[3] = data[3];
		}else{
			ipcp_write_buffer[4 + ipcp_write_len] = type;
			ipcp_write_buffer[5 + ipcp_write_len] = len + 2;
			for(i = 0; i < len; i++)ipcp_write_buffer[6 + ipcp_write_len + i] = data[i];
			ipcp_write_len += len + 2;
		}
	}else if(ipcp_code == IPCP_CODE_CONFIGURE_NAK){
		if((type == 3) && (len == 4)){
			ipcp_ip_local[0] = data[0];
			ipcp_ip_local[1] = data[1];
			ipcp_ip_local[2] = data[2];
			ipcp_ip_local[3] = data[3];
			mtimer_timeout_set(&ipcp_timer, 0);
		}else if((type == 129) && (len == 4)){
			ipcp_dns1[0] = data[0];
			ipcp_dns1[1] = data[1];
			ipcp_dns1[2] = data[2];
			ipcp_dns1[3] = data[3];
			mtimer_timeout_set(&ipcp_timer, 0);
		}else if((type == 131) && (len == 4)){
			ipcp_dns2[0] = data[0];
			ipcp_dns2[1] = data[1];
			ipcp_dns2[2] = data[2];
			ipcp_dns2[3] = data[3];
			mtimer_timeout_set(&ipcp_timer, 0);
		}
	}else if(ipcp_code == IPCP_CODE_CONFIGURE_REJECT){
		if(type == 129){
			ipcp_dns1_ask = 0;
			mtimer_timeout_set(&ipcp_timer, 0);
		}else if(type == 131){
			ipcp_dns2_ask = 0;
			mtimer_timeout_set(&ipcp_timer, 0);
		}
	}
	return(0);
}

static void ipcp_parse_configuration(uint8_t *data, uint16_t len, uint8_t(*callback)(uint8_t type, uint8_t *data, uint8_t len)){
	while((len > 1) && (data[1] > 1) && (len >= data[1])){
		if(callback(data[0], &data[2], data[1] - 2))break;
		len -= data[1];
		data += data[1];
	}
}

#ifdef IPCP_DEBUG
static void ipcp_debug(uint8_t tofrom, uint8_t *data, uint16_t len){
	uint16_t i;
	if(!tofrom){
		printf("%s    IPCP: GOT PACKET:", ANSI_GREEN);
	}else{
		printf("%s    IPCP: SENT PACKET:", ANSI_RED);
	}
	for(i = 0; i < len; i++){
		printf(" %02X", (unsigned int)data[i]);
	}
	printf("\r\n");
	if(len < 4){
		printf("    INVALID PACKET\n");
		goto cleanup;
	}
	if(data[0] == 1){
		printf("    CONFIGURE/REQUEST\n");
	}else if(data[0] == 2){
		printf("    CONFIGURE/ACK\n");
	}else if(data[0] == 3){
		printf("    CONFIGURE/NAK\n");
	}else if(data[0] == 4){
		printf("    CONFIGURE/REJECT\n");
	}else if(data[0] == 5){
		printf("    TERMINATE/REQUEST\n");
	}else if(data[0] == 6){
		printf("    TERMINATE/ACK\n");
	}else if(data[0] == 7){
		printf("    CODE/REJECT\n");
	}
	printf("    IDENTIFIER: %u\n", (unsigned int)data[1]);
	i = (((uint16_t)data[2] << 8) | data[3]);
	printf("    LENGTH: %u\n", (unsigned int)i);
	if(i > len){
		printf("    INVALID PACKET\n");
		goto cleanup;
	}
	ipcp_parse_configuration(&data[4], len - 4, ipcp_debug_configuration);
cleanup:
	printf("%s", ANSI_RESET);
}

static uint8_t ipcp_debug_configuration(uint8_t type, uint8_t *data, uint8_t len){
	uint8_t i;
	printf("      TYPE: %u\n", (unsigned int)type);
	if(len){
		printf("        DATA:");
		for(i = 0; i < len; i++){
			printf(" %02X", data[i]);
		}
		printf("\n");
	}
	return(0);
}
#endif

