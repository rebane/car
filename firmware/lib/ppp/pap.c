#include "pap.h"
#include <stdint.h>
#include <stdio.h>
#include "mtimer.h"
#include "ppp.h"
#include "ahdlc.h"

uint8_t *pap_write_buffer, pap_identifier, pap_auth, pap_auth_ok;
static mtimer_t pap_timer;

void pap_init(){
	pap_write_buffer = &ahdlc_write_buffer[2];
}

void pap_reset(){
	pap_auth = 0;
	pap_auth_ok = 0;
	pap_identifier = 0;
	mtimer_timeout_set(&pap_timer, 0);
}

void pap_poll(char *user, char *pass){
	uint16_t i, l;
	if(!pap_auth)return;
	if(!pap_auth_ok && mtimer_timeout(&pap_timer)){
		mtimer_timeout_set(&pap_timer, 5000);
		l = 4;
		pap_write_buffer[0] = 1;
		pap_write_buffer[1] = pap_identifier;
		for(i = 0; user[i]; i++){
			pap_write_buffer[l + 1 + i] = user[i];
		}
		pap_write_buffer[l] = i;
		l += i + 1;
		for(i = 0; pass[i]; i++){
			pap_write_buffer[l + 1 + i] = pass[i];
		}
		pap_write_buffer[l] = i;
		l += i + 1;
		pap_write_buffer[2] = l >> 8;
		pap_write_buffer[3] = l & 0xFF;
		ppp_write(0xC023, pap_write_buffer, l);
	}
}

void pap_read(uint8_t *data, uint16_t len){
	uint16_t i;
	if(len < 4)return;
	i = (((uint16_t)data[2] << 8) | data[3]);
	if((i < 4) || (i > len))return;
	if(data[0] == 1){
		for(i = 0; i < len; i++){
			pap_write_buffer[i] = data[i];
		}
		pap_write_buffer[0] = 2;
		ppp_write(0xC023, pap_write_buffer, len);
	}else if(data[0] == 2){
		pap_auth_ok = 1;
	}
}

void pap_set_auth(){
	pap_auth = 1;
}

uint8_t pap_ok(){
	return(!pap_auth || pap_auth_ok);
}

