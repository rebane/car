#include "ppp.h"
#include <stdint.h>
#include <stdio.h>
#include "ahdlc.h"
#include "lcp.h"
#include "pap.h"
#include "ipcp.h"

// #define PPP_DEBUG

uint8_t *ppp_buf;
uint16_t ppp_len;

static uint8_t *ppp_write_buffer, ppp_protocol_reject_counter;
static uint8_t ppp_lcp_ok, ppp_pap_ok, ppp_ipcp_ok;

#ifdef PPP_DEBUG
#include "ansi_colors.h"
static void ppp_debug(uint8_t tofrom, uint8_t *data, uint16_t len);
#else
#define ppp_debug(...)
#endif

void ppp_init(){
	ahdlc_init();
	ppp_write_buffer = &ahdlc_write_buffer[0];
	ppp_protocol_reject_counter = 0;
	lcp_init();
	pap_init();
	ipcp_init();
	ppp_reset();
}

void ppp_reset(){
	ahdlc_reset();
	lcp_reset();
	pap_reset();
	ipcp_reset();
	ppp_lcp_ok = ppp_pap_ok = ppp_ipcp_ok = 0;
}

void ppp_poll(){
	ahdlc_poll();
	lcp_poll();
	if(lcp_ok()){
		if(!ppp_lcp_ok){
			ppp_lcp_ok = 1;
			printf("LCP OK\n");
		}
		if(lcp_get_auth()){
			pap_set_auth();
		}
		pap_poll("", "");
	}
	if(lcp_ok() && pap_ok()){
		if(!ppp_pap_ok){
			ppp_pap_ok = 1;
			printf("PAP OK\n");
		}
		ipcp_poll();
	}
	if(lcp_ok() && pap_ok() && ipcp_ok()){
		if(!ppp_ipcp_ok){
			ppp_ipcp_ok = 1;
			printf("IPCP OK\n");
			printf("IPCP IP REMOTE: %u.%u.%u.%u\n", (unsigned int)ipcp_ip_remote[0], (unsigned int)ipcp_ip_remote[1], (unsigned int)ipcp_ip_remote[2], (unsigned int)ipcp_ip_remote[3]);
			printf("IPCP IP LOCAL: %u.%u.%u.%u\n", (unsigned int)ipcp_ip_local[0], (unsigned int)ipcp_ip_local[1], (unsigned int)ipcp_ip_local[2], (unsigned int)ipcp_ip_local[3]);
			printf("IPCP DNS1: %u.%u.%u.%u\n", (unsigned int)ipcp_dns1[0], (unsigned int)ipcp_dns1[1], (unsigned int)ipcp_dns1[2], (unsigned int)ipcp_dns1[3]);
			printf("IPCP DNS2: %u.%u.%u.%u\n", (unsigned int)ipcp_dns2[0], (unsigned int)ipcp_dns2[1], (unsigned int)ipcp_dns2[2], (unsigned int)ipcp_dns2[3]);
		}
	}
}

void ppp_read(uint8_t *data, uint16_t len){
	uint16_t protocol;
	ppp_debug(0, data, len);
	if((data[0] & 0x01) && (len > 1)){
		protocol = data[0];
		data++;
		len--;
	}else if(len > 2){
		protocol = ((((uint16_t)data[0]) << 8) | data[1]);
		data += 2;
		len -= 2;
	}else{
		return;
	}
	if(protocol == 0xC021){
		lcp_read(data, len);
	}else if(protocol == 0xC023){
		pap_read(data, len);
	}else if(protocol == 0x8021){
		ipcp_read(data, len);
	}else if(protocol == 0x0021){
		ppp_buf = data;
		ppp_len = len;
	}else{
		data[-2] = protocol >> 8;
		data[-1] = protocol & 0xFF;
		lcp_write(LCP_CODE_PROTOCOL_REJECT, ppp_protocol_reject_counter++, &data[-2], len + 2);
	}
}

void ppp_write(uint16_t protocol, uint8_t *data, uint16_t len){
	ppp_write_buffer[0] = protocol >> 8;
	ppp_write_buffer[1] = protocol & 0xFF;
	len += 2;
	ppp_debug(1, ppp_write_buffer, len);
	ahdlc_write(ppp_write_buffer, len);
}

uint8_t ppp_carrier(){
	return(ppp_lcp_ok && ppp_pap_ok && ppp_ipcp_ok);
}

#ifdef PPP_DEBUG
static void ppp_debug(uint8_t tofrom, uint8_t *data, uint16_t len){
	uint16_t protocol, i;
	if(!tofrom){
		printf("%s  PPP: GOT PACKET:", ANSI_GREEN);
	}else{
		printf("%s  PPP: SENT PACKET:", ANSI_RED);
	}
	for(i = 0; i < len; i++){
		printf(" %02X", (unsigned int)data[i]);
	}
	printf("\n");
	if((data[0] & 0x01) && (len > 1)){
		protocol = data[0];
		data++;
		len--;
	}else if(len > 2){
		protocol = ((((uint16_t)data[0]) << 8) | data[1]);
		data += 2;
		len -= 2;
	}else{
		printf("  INVALID PACKET\n");
		goto cleanup;
	}
	printf("  PROTOCOL: %04X", (unsigned int)protocol);
	if(protocol == 0xC021){
		printf(" (LCP)\n");
	}else if(protocol == 0x8021){
		printf(" (IPCP)\n");
	}else if(protocol == 0x80FD){
		printf(" (Compression Control Protocol)\n");
	}else{
		printf("\n");
	}
cleanup:
	printf("%s", ANSI_RESET);
}
#endif

