#ifndef _AHDLC_H_
#define _AHDLC_H_

#include <stdint.h>

#define AHDLC_WRITE_LEN 2048 // 1516
#define AHDLC_READ_LEN  2048 // 1516

extern uint8_t ahdlc_write_buffer[AHDLC_WRITE_LEN];

void ahdlc_init();
void ahdlc_reset();
void ahdlc_poll();
void ahdlc_read_byte(uint8_t c);
void ahdlc_write(uint8_t *data, uint16_t len);
void ahdlc_send(uint8_t *data, uint16_t len);

#endif

