#ifndef _PPP_H_
#define _PPP_H_

#include <stdint.h>
#include "ahdlc.h"
#include "ipcp.h"

#define ppp_read_byte(c)    ahdlc_read_byte((c))
#define ppp_send(data, len) ahdlc_send((data), (len))

#define ppp_ip_remote       (ipcp_ip_remote)
#define ppp_ip_local        (ipcp_ip_local)
#define ppp_dns1            (ipcp_dns1)
#define ppp_dns2            (ipcp_dns2)

extern uint8_t *ppp_buf;
extern uint16_t ppp_len;

void ppp_init();
void ppp_reset();
void ppp_poll();
void ppp_read(uint8_t *data, uint16_t len);
void ppp_write(uint16_t protocol, uint8_t *data, uint16_t len);
uint8_t ppp_carrier();

#endif

