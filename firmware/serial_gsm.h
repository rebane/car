#ifndef _SERIAL_GSM_H_
#define _SERIAL_GSM_H_

#include <stdint.h>

void serial_gsm_init();
int16_t serial_gsm_read(void *buf, uint16_t count);
int16_t serial_gsm_write(const void *buf, uint16_t count);
int16_t serial_gsm_write_space(void);
int16_t serial_gsm_read_flush(void);

#endif

