#include "serial_gps.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"

// TX = RB11, RP43
// RX = RB10, RP42
// NRESET = RC5, RPI53

#define SERIAL_GPS_SEND_FIFO_LEN    8
#define SERIAL_GPS_RECV_FIFO_LEN    64

#define serial_gps_critical_enter() do{ INTCON2bits.GIE = 0; }while(0)
#define serial_gps_critical_exit()  do{ INTCON2bits.GIE = 1; }while(0)

#if SERIAL_GPS_SEND_FIFO_LEN < 256
static volatile uint8_t serial_gps_send_fifo_in, serial_gps_send_fifo_out, serial_gps_send_fifo_len;
#elif SERIAL_GPS_SEND_FIFO_LEN < 65536
static volatile uint16_t serial_gps_send_fifo_in, serial_gps_send_fifo_out, serial_gps_send_fifo_len;
#else
#error SERIAL_GPS_SEND_FIFO_LEN too big
#endif

#if SERIAL_GPS_RECV_FIFO_LEN < 256
static volatile uint8_t serial_gps_recv_fifo_in, serial_gps_recv_fifo_out, serial_gps_recv_fifo_len;
#elif SERIAL_GPS_RECV_FIFO_LEN < 65536
static volatile uint16_t serial_gps_recv_fifo_in, serial_gps_recv_fifo_out, serial_gps_recv_fifo_len;
#else
#error SERIAL_GPS_RECV_FIFO_LEN too big
#endif

static volatile uint8_t serial_gps_send_fifo[SERIAL_GPS_SEND_FIFO_LEN], serial_gps_recv_fifo[SERIAL_GPS_RECV_FIFO_LEN];

void serial_gps_init(){
	IEC1bits.U2TXIE = 0;
	IEC1bits.U2RXIE = 0;
	IEC4bits.U2EIE = 0;

	serial_gps_send_fifo_in = serial_gps_send_fifo_out = serial_gps_send_fifo_len = 0;
	serial_gps_recv_fifo_in = serial_gps_recv_fifo_out = serial_gps_recv_fifo_len = 0;

	U2MODE = 0;

	// TX
	// RPOR4bits.RP43R = 3; // väga vist pole vaja GPS TX'i

	// RX
	RPINR19bits.U2RXR = 42;

	U2STA = 0;
	U2BRG = (PLATFORM_FCPU / (4LLU * 9600LLU)) - 1LLU;
	U2MODEbits.BRGH = 1;
	U2STAbits.UTXISEL1 = 0;
	U2STAbits.UTXISEL0 = 0;
	U2MODEbits.UARTEN = 1;
	U2STAbits.UTXEN = 1; // Note: The UTXEN bit is set after the UARTEN bit has been set; otherwise, UART transmissions will not be enabled.

	IPC7bits.U2TXIP = 1;
	IPC7bits.U2RXIP = 1;

	IFS1bits.U2TXIF = 0;
	IFS1bits.U2RXIF = 0;
	IFS4bits.U2EIF = 0;

	IEC1bits.U2RXIE = 1;
	IEC4bits.U2EIE = 1;

	// NRESET
	ANSELCbits.ANSC5 = 0;
	PORTCbits.RC5 = 1;
	TRISCbits.TRISC5 = 0;
}

void serial_gps_rx_invert(){
	U2MODEbits.URXINV = !U2MODEbits.URXINV;
}

int16_t serial_gps_read(void *buf, uint16_t count){
	uint16_t len;
	if(!serial_gps_recv_fifo_len)return(0);
	serial_gps_critical_enter();
	IEC1bits.U2RXIE = 0;
	for(len = 0; serial_gps_recv_fifo_len && (len < count) && (len < 32767); len++){
		((uint8_t *)buf)[len] = serial_gps_recv_fifo[serial_gps_recv_fifo_out];
		serial_gps_recv_fifo_len--;
		serial_gps_recv_fifo_out = (serial_gps_recv_fifo_out + 1) % SERIAL_GPS_RECV_FIFO_LEN;
	}
	IEC1bits.U2RXIE = 1;
	serial_gps_critical_exit();
	return(len);
}

int16_t serial_gps_write(const void *buf, uint16_t count){
	uint16_t len;
	serial_gps_critical_enter();
	IEC1bits.U2TXIE = 0;
	for(len = 0; (serial_gps_send_fifo_len < SERIAL_GPS_SEND_FIFO_LEN) && (len < count) && (len < 32767); len++){
		serial_gps_send_fifo[serial_gps_send_fifo_in] = ((uint8_t *)buf)[len];
		serial_gps_send_fifo_len++;
		serial_gps_send_fifo_in = (serial_gps_send_fifo_in + 1) % SERIAL_GPS_SEND_FIFO_LEN;
	}
	if(serial_gps_send_fifo_len){
		IFS1bits.U2TXIF = 1;
		IEC1bits.U2TXIE = 1;
	}
	serial_gps_critical_exit();
	return(len);
}

int16_t serial_gps_write_space(void){
	int16_t len;
	serial_gps_critical_enter();
	IEC1bits.U2TXIE = 0;
	len = (SERIAL_GPS_SEND_FIFO_LEN - serial_gps_send_fifo_len);
	if(serial_gps_send_fifo_len)IEC1bits.U2TXIE = 1;
	serial_gps_critical_exit();
	return(len);
}

int16_t serial_gps_read_flush(void){
	serial_gps_critical_enter();
	IEC1bits.U2RXIE = 0;
	serial_gps_recv_fifo_len = 0;
	serial_gps_recv_fifo_out = serial_gps_recv_fifo_in;
	IEC1bits.U2RXIE = 1;
	serial_gps_critical_exit();
	return(0);
}

void _U2ErrHandler(){
	U2STAbits.OERR = 0;
	U2STAbits.PERR = 0;
	U2STAbits.FERR = 0;
	IFS4bits.U2EIF = 0;
}

void _U2RXHandler(){
	uint16_t c;
	while(U2STAbits.URXDA){
		c = U2RXREG;
		if(serial_gps_recv_fifo_len < SERIAL_GPS_RECV_FIFO_LEN){
			serial_gps_recv_fifo[serial_gps_recv_fifo_in] = (c & 0xFF);
			serial_gps_recv_fifo_in = (serial_gps_recv_fifo_in + 1) % SERIAL_GPS_RECV_FIFO_LEN;
			serial_gps_recv_fifo_len++;
		}
	}
	IFS1bits.U2RXIF = 0;
}

void _U2TXHandler(){
	while(!U2STAbits.UTXBF && serial_gps_send_fifo_len){
		U2TXREG = serial_gps_send_fifo[serial_gps_send_fifo_out];
		serial_gps_send_fifo_out = (serial_gps_send_fifo_out + 1) % SERIAL_GPS_SEND_FIFO_LEN;
		serial_gps_send_fifo_len--;
	}
	if(!serial_gps_send_fifo_len)IEC1bits.U2TXIE = 0;
	IFS1bits.U2TXIF = 0;
}

