#ifndef _PLATFORM_H_
#define _PLATFORM_H_

#include <stdint.h>

extern uint32_t PLATFORM_FHS, PLATFORM_FIN, PLATFORM_FPLLI, PLATFORM_FSYS, PLATFORM_FOSC, PLATFORM_FP, PLATFORM_FCY, PLATFORM_FCPU;

void platform_init();
void platform_led_set(uint8_t led, uint8_t status, uint8_t mode);
void platform_led_toggle(uint8_t led, uint8_t mode);
void platform_led_mode_set(uint8_t mode);

#endif

