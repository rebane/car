#include "serial_485.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"

// TX = RB8, RP40
// RX = RB5, RP37
// DE = RB7, RP39
// RE = RB6, RP38

#define SERIAL_485_SEND_FIFO_LEN 4
#define SERIAL_485_RECV_FIFO_LEN 32

#define serial_485_critical_enter() do{ INTCON2bits.GIE = 0; }while(0)
#define serial_485_critical_exit()  do{ INTCON2bits.GIE = 1; }while(0)

#if SERIAL_485_SEND_FIFO_LEN < 256
static volatile uint8_t serial_485_send_fifo_in, serial_485_send_fifo_out, serial_485_send_fifo_len;
#elif SERIAL_485_SEND_FIFO_LEN < 65536
static volatile uint16_t serial_485_send_fifo_in, serial_485_send_fifo_out, serial_485_send_fifo_len;
#else
#error SERIAL_485_SEND_FIFO_LEN too big
#endif

#if SERIAL_485_RECV_FIFO_LEN < 256
static volatile uint8_t serial_485_recv_fifo_in, serial_485_recv_fifo_out, serial_485_recv_fifo_len;
#elif SERIAL_485_RECV_FIFO_LEN < 65536
static volatile uint16_t serial_485_recv_fifo_in, serial_485_recv_fifo_out, serial_485_recv_fifo_len;
#else
#error SERIAL_485_RECV_FIFO_LEN too big
#endif

static volatile uint8_t serial_485_send_fifo[SERIAL_485_SEND_FIFO_LEN], serial_485_recv_fifo[SERIAL_485_RECV_FIFO_LEN];

void serial_485_init(){
	IEC0bits.U1TXIE = 0;
	IEC0bits.U1RXIE = 0;
	IEC4bits.U1EIE = 0;

	serial_485_send_fifo_in = serial_485_send_fifo_out = serial_485_send_fifo_len = 0;
	serial_485_recv_fifo_in = serial_485_recv_fifo_out = serial_485_recv_fifo_len = 0;

	U1MODE = 0;

	// TX
	ANSELBbits.ANSB8 = 0;
	RPOR3bits.RP40R = 1;

	// RX
	RPINR18bits.U1RXR = 37;

	// DE
	ANSELBbits.ANSB7 = 0;
	TRISBbits.TRISB7 = 0;
	PORTBbits.RB7 = 0;
	// RE
	TRISBbits.TRISB6 = 0;
	PORTBbits.RB6 = 0;

	U1STA = 0;
	U1BRG = (PLATFORM_FCPU / (4LLU * 9600LLU)) - 1LLU;
	U1MODEbits.BRGH = 1;
	U1MODEbits.STSEL = 1; // 2 stop bits
	U1MODEbits.PDSEL = 1; // 8 bit, even parity
	U1STAbits.UTXISEL1 = 0;
	U1STAbits.UTXISEL0 = 0;
	U1MODEbits.UARTEN = 1;
	U1STAbits.UTXEN = 1; // Note: The UTXEN bit is set after the UARTEN bit has been set; otherwise, UART transmissions will not be enabled.

	IPC3bits.U1TXIP = 1;
	IPC2bits.U1RXIP = 1;

	IFS0bits.U1TXIF = 0;
	IFS0bits.U1RXIF = 0;
	IFS4bits.U1EIF = 0;

	IEC0bits.U1RXIE = 1;
	IEC4bits.U1EIE = 1;
}

int16_t serial_485_read(void *buf, uint16_t count){
	uint16_t len;
	if(!serial_485_recv_fifo_len)return(0);
	serial_485_critical_enter();
	IEC0bits.U1RXIE = 0;
	for(len = 0; serial_485_recv_fifo_len && (len < count) && (len < 32767); len++){
		((uint8_t *)buf)[len] = serial_485_recv_fifo[serial_485_recv_fifo_out];
		serial_485_recv_fifo_len--;
		serial_485_recv_fifo_out = (serial_485_recv_fifo_out + 1) % SERIAL_485_RECV_FIFO_LEN;
	}
	IEC0bits.U1RXIE = 1;
	serial_485_critical_exit();
	return(len);
}

int16_t serial_485_write(const void *buf, uint16_t count){
	uint16_t len;
	serial_485_critical_enter();
	IEC0bits.U1TXIE = 0;
	for(len = 0; (serial_485_send_fifo_len < SERIAL_485_SEND_FIFO_LEN) && (len < count) && (len < 32767); len++){
		serial_485_send_fifo[serial_485_send_fifo_in] = ((uint8_t *)buf)[len];
		serial_485_send_fifo_len++;
		serial_485_send_fifo_in = (serial_485_send_fifo_in + 1) % SERIAL_485_SEND_FIFO_LEN;
	}
	if(serial_485_send_fifo_len){
		IFS0bits.U1TXIF = 1;
		IEC0bits.U1TXIE = 1;
	}
	serial_485_critical_exit();
	return(len);
}

int16_t serial_485_write_space(void){
	int16_t len;
	serial_485_critical_enter();
	IEC0bits.U1TXIE = 0;
	len = (SERIAL_485_SEND_FIFO_LEN - serial_485_send_fifo_len);
	if(serial_485_send_fifo_len)IEC0bits.U1TXIE = 1;
	serial_485_critical_exit();
	return(len);
}

int16_t serial_485_read_flush(void){
	serial_485_critical_enter();
	IEC0bits.U1RXIE = 0;
	serial_485_recv_fifo_len = 0;
	serial_485_recv_fifo_out = serial_485_recv_fifo_in;
	IEC0bits.U1RXIE = 1;
	serial_485_critical_exit();
	return(0);
}

void _U1ErrHandler(){
	U1STAbits.OERR = 0;
	U1STAbits.PERR = 0;
	U1STAbits.FERR = 0;
	IFS4bits.U1EIF = 0;
}

void _U1RXHandler(){
	uint16_t c;
	while(U1STAbits.URXDA){
		c = U1RXREG;
		if(serial_485_recv_fifo_len < SERIAL_485_RECV_FIFO_LEN){
			serial_485_recv_fifo[serial_485_recv_fifo_in] = (c & 0xFF);
			serial_485_recv_fifo_in = (serial_485_recv_fifo_in + 1) % SERIAL_485_RECV_FIFO_LEN;
			serial_485_recv_fifo_len++;
		}
	}
	IFS0bits.U1RXIF = 0;
}

void _U1TXHandler(){
	while(!U1STAbits.UTXBF && serial_485_send_fifo_len){
		U1TXREG = serial_485_send_fifo[serial_485_send_fifo_out];
		serial_485_send_fifo_out = (serial_485_send_fifo_out + 1) % SERIAL_485_SEND_FIFO_LEN;
		serial_485_send_fifo_len--;
	}
	if(!serial_485_send_fifo_len)IEC0bits.U1TXIE = 0;
	IFS0bits.U1TXIF = 0;
}

