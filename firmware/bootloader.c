#include <stdint.h>
#include <xc.h>
#include <libpic30.h>
#include "pmem.h"
#include "firmware.h"

#define __xstr(s) __str(s)
#define __str(s)  #s

_FICD(ICS_PGD1 & JTAGEN_OFF); // JTAG tuleb disableda muidu vastavad IO'd ei tööta (RA7, RA10, RB8, RB9)
_FPOR(BOREN_ON);
_FWDT(WDTPOST_PS8192 & WDTPRE_PR32 & WINDIS_OFF & FWDTEN_ON);
_FOSC(FCKSM_CSECMD & OSCIOFNC_OFF & POSCMD_HS);
_FOSCSEL(FNOSC_FRCDIVN & PWMLOCK_OFF);

static uint8_t buffer[PMEM_PAGE_SIZE * 2];

static uint32_t memcmp_p2p(uint32_t s1, uint32_t s2, uint32_t n);
static uint32_t memcpy_p2p(uint32_t dest, uint32_t src, uint32_t n);

int main(){
	uint32_t l;
	INTCON2bits.GIE = 0;

	l = 0;
	ClrWdt();
	if(!firmware_validate(LOAD_NEW_ADDRESS, &l, buffer)){
		ClrWdt();
		if(memcmp_p2p(LOAD_ADDRESS, LOAD_NEW_ADDRESS, l)){
			ClrWdt();
			memcpy_p2p(LOAD_ADDRESS, LOAD_NEW_ADDRESS, l);
		}
	}
	ClrWdt();
	goto *(((uint16_t)LOAD_ADDRESS * 2) / 3);
}

void __attribute__((__interrupt__, no_auto_psv, naked)) _DefaultInterrupt(void){
	__asm__("mov.d w4,[w15++]");
	__asm__("mov _INTTREG, w4");  // loeme INTTREG'i
	__asm__("push _RCOUNT");
	__asm__("push _TBLPAG");
	__asm__("mov.d w0,[w15++]");
	__asm__("mov.d w2,[w15++]");
	__asm__("mov.d w6,[w15++]");
	__asm__("lnk #0");
	/**/
	__asm__("mov #255, w5");      // arvutame VECNUM'i (INTTREG & 0xFF)
	__asm__("and w4, w5, w5");

	__asm__("add w5, #2, w5");    // liidame 2 (GOTO ja start aadress)
	__asm__("add w5, w5, w5");    // korrutame kahega, et saada word aadress

	__asm__("mov #((" __xstr(LOAD_ADDRESS) " * 2) / 3), w4"); // liidame softi algusaadressi
	__asm__("add w5, w4, w5");

	__asm__("clr w4");
	__asm__("mov w4, _TBLPAG");   // TBLPAG nulliks
	__asm__("tblrdl.w [w5], w4"); // loeme alumise aadressi W4 registrisse
	__asm__("tblrdh.w [w5], w5"); // loeme ülemise aadressi W5 registrisse

	__asm__("call.l w4");         // call W4/W5
	/**/
	__asm__("ulnk");
	__asm__("mov.d [--w15],w6");
	__asm__("mov.d [--w15],w2");
	__asm__("mov.d [--w15],w0");
	__asm__("pop _TBLPAG");
	__asm__("pop _RCOUNT");
	__asm__("mov.d [--w15],w4");
}

static uint32_t memcmp_p2p(uint32_t s1, uint32_t s2, uint32_t n){
	uint32_t i, l, j;
	for(i = 0; i < n; i += PMEM_PAGE_SIZE){
		l = (n - i) > PMEM_PAGE_SIZE ? PMEM_PAGE_SIZE : (n - i);
		pmem_cpy_p2d(&buffer[0], s1 + i, l);
		pmem_cpy_p2d(&buffer[PMEM_PAGE_SIZE], s2 + i, l);
		for(j = 0; j < l; j++){
			if(buffer[0 + j] != buffer[PMEM_PAGE_SIZE + j])return(1);
		}
	}
	return(0);
}

static uint32_t memcpy_p2p(uint32_t dest, uint32_t src, uint32_t n){
	uint32_t i;
	for(i = 0; i < n; i += PMEM_PAGE_SIZE){
		pmem_cpy_p2d(&buffer[0], src + i, PMEM_PAGE_SIZE);
		pmem_erase_page_containing(dest + i);
		pmem_cpy_d2p(dest + i, &buffer[0], PMEM_PAGE_SIZE);
	}
	return(dest);
}

