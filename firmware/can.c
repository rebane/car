#include "can.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"
#include <stdio.h>

// TX = RB4, RP36
// RX = RA4, RP20

#define CAN_FIFO_LEN         8
#define CAN_BUF_LEN          4

#define can_critical_enter() do{ INTCON2bits.GIE = 0; }while(0)
#define can_critical_exit()  do{ INTCON2bits.GIE = 1; }while(0)

static volatile struct can_frame can_fifo[CAN_FIFO_LEN];
static volatile uint8_t can_fifo_in, can_fifo_out, can_fifo_len;
static volatile uint16_t can_buffer[CAN_BUF_LEN][8] __attribute__((aligned(CAN_BUF_LEN * 16)));

void can_init(void){
	IEC2bits.C1IE = 0;

	/* put the module in configuration mode */
	// C1CTRL1bits.REQOP = 4;
	C1CTRL1 = (((uint16_t)4) << 8);
	while(C1CTRL1bits.OPMODE != 4);

	can_fifo_in = can_fifo_out = can_fifo_len = 0;

	// TX
	RPOR1bits.RP36R = 14;

	// RX
	ANSELAbits.ANSA4 = 0;
	RPINR26bits.C1RXR = 20;

	C1CTRL1bits.WIN = 1;

	C1CTRL2 = 0;
	C1CFG1 = 0;
	C1CFG2 = 0;
	C1FCTRL = 0;

	/* Set up the CAN module for 250kbps speed with 10 Tq per bit. */
	C1CFG1bits.BRP = 11;     // 2 x 12 x 1 / Fcan => 250 kbps if 10Tq and 60MHz 
	C1CFG1bits.SJW = 1;      // 2
	C1CFG2bits.PRSEG = 2;
	C1CFG2bits.SEG1PH = 2;   // 3
	C1CFG2bits.SAM = 1;
	C1CFG2bits.SEG2PHTS = 1; // 2
	C1CFG2bits.SEG2PH = 2;   // 3
	C1CFG2bits.WAKFIL = 0;
	C1FCTRLbits.FSA = 0x1F;  // No FIFO
	C1FCTRLbits.DMABS = 0;   // 4 buffers

	C1INTE = 0;
	C1INTF = 0;
	C1INTEbits.RBIE = 1;

	DMA0CONbits.CHEN = 0;
	DMAPWC = 0;
	DMA0CONbits.SIZE = 0;
	DMA0CONbits.DIR = 0;
	DMA0CONbits.AMODE = 2;
	DMA0CONbits.MODE = 0;
	DMA0REQ = 34;
	DMA0CNT = 7;
	DMA0PAD = (volatile uint16_t)&C1RXD;
	DMA0STAH = __builtin_dmapage(can_buffer);
	DMA0STAL = __builtin_dmaoffset(can_buffer);
	DMA0CONbits.CHEN = 1;

	C1FMSKSEL1 = 0;
	C1FEN1 = 0;
	C1RXM0SID = 0;
	C1BUFPNT1 = 0;
	C1BUFPNT1bits.F0BP = 0;
	C1FEN1bits.FLTEN0 = 1;
	C1CTRL1bits.WIN = 0;
	C1TR01CON = 0;

	C1CTRL1bits.REQOP = 3;
	while(C1CTRL1bits.OPMODE != 3);

	IPC8bits.C1IP = 1;
	IFS2bits.C1IF = 0;
	IEC2bits.C1IE = 1;
}

int8_t can_read(struct can_frame *frame){
	int8_t len;
	if(!can_fifo_len)return(0);
	len = 0;
	can_critical_enter();
	IEC2bits.C1IE = 0;
	if(can_fifo_len){
		*frame = can_fifo[can_fifo_out];
		can_fifo_len--;
		can_fifo_out = (can_fifo_out + 1) % CAN_FIFO_LEN;
		len = 1;
	}
	IEC2bits.C1IE = 1;
	can_critical_exit();
	return(len);
}

void _C1Handler(){
	if(C1RXFUL1bits.RXFUL0){
		if(can_fifo_len < CAN_FIFO_LEN){
			if(can_buffer[0][0] & (((uint16_t)1) << 0)){
				can_fifo[can_fifo_in].can_id = (((uint32_t)(can_buffer[0][0] & 0x1FFC) << 16)
						| ((uint32_t)(can_buffer[0][1] & 0x0FFF) << 6) | (can_buffer[0][2] >> 10)) & CAN_EFF_MASK;
				if(can_buffer[0][2] & (((uint16_t)1) << 9))can_fifo[can_fifo_in].can_id |= CAN_RTR_FLAG;
				can_fifo[can_fifo_in].can_id |= CAN_EFF_FLAG;
			}else{
				can_fifo[can_fifo_in].can_id = (can_buffer[0][0] >> 2) & CAN_SFF_MASK;
			}
			can_fifo[can_fifo_in].can_dlc = can_buffer[0][2] & 0x0F;
			can_fifo[can_fifo_in].data[0] = (can_buffer[0][3] >> 0) & 0xFF;
			can_fifo[can_fifo_in].data[1] = (can_buffer[0][3] >> 8) & 0xFF;
			can_fifo[can_fifo_in].data[2] = (can_buffer[0][4] >> 0) & 0xFF;
			can_fifo[can_fifo_in].data[3] = (can_buffer[0][4] >> 8) & 0xFF;
			can_fifo[can_fifo_in].data[4] = (can_buffer[0][5] >> 0) & 0xFF;
			can_fifo[can_fifo_in].data[5] = (can_buffer[0][5] >> 8) & 0xFF;
			can_fifo[can_fifo_in].data[6] = (can_buffer[0][6] >> 0) & 0xFF;
			can_fifo[can_fifo_in].data[7] = (can_buffer[0][6] >> 8) & 0xFF;
			can_fifo_in = (can_fifo_in + 1) % CAN_FIFO_LEN;
			can_fifo_len++;
		}
		C1RXFUL1bits.RXFUL0 = 0;
	}
	C1INTF = 0;
	IFS2bits.C1IF = 0;
}

/*
http://www.cs.fsu.edu/~baker/devices/lxr/http/source/linux/include/linux/can.h
CAN READ: 1ABF 037A B408 4241 4443 4645 4847 0000

CAN READ: 1158 037A B408 3231 3433 3635 3837 0000

uint8_t can_send_packet(uint32_t id, uint8_t attr, uint8_t len, void *data){

can_send_packet(0x1ABCDEAD, CAN_ATTR_EXT, 8, "ABCDEFGH");
can_send_packet(0x0456, 0, 8, "12345678");
*/

