#ifndef _NMEA_H_
#define _NMEA_H_

#include <stdint.h>

void nmea_init();
void nmea_poll();
uint16_t nmea_read(uint8_t *buffer);

#endif

