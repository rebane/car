#include <stdint.h>
#include <xc.h>
#include <libpic30.h>
#include <stdio.h>
#include "platform.h"
#include "mtimer.h"
#include "serial_debug.h"
#include "serial_gps.h"
#include "serial_gsm.h"
#include "serial_485.h"
#include "spi_flash.h"
#include "kline.h"
#include "can.h"
#include "clock.h"
#include "led.h"
#include "voltage.h"
#include "i2c.h"
#include "nmea.h"
#include "iso9141.h"
#include "j1939.h"
#include "mma7660.h"
#include "seats.h"
#include "flash.h"
#include "net.h"
#include "modem.h"
#include "data.h"

// kasutamata pinid tuleks panna väljundiks ja mättasse näiteks
// https://github.com/openxc/uds-c
// http://hackaday.com/2013/10/29/can-hacking-protocols/

int main(){
	mtimer_t t;
	uint8_t s, c;
	platform_init();
	mtimer_init();

/*	ANSELCbits.ANSC1 = 0;
	TRISCbits.TRISC1 = 1;
	ANSELBbits.ANSB1 = 0;
	TRISBbits.TRISB1 = 1;
	// NRESET
	ANSELCbits.ANSC2 = 0;
	TRISCbits.TRISC2 = 0;
	PORTCbits.RC2 = 1;
	// PWRKEY
	TRISAbits.TRISA7 = 1;
	while(1); */

	serial_debug_init();
	serial_gps_init();
	serial_gsm_init();
#ifdef SEATS
	serial_485_init();
#endif
	ClrWdt();
	spi_flash_init(1);
	ClrWdt();
#ifdef ISO9141
	kline_init();
#endif
// #ifdef J1939
	can_init();
// #endif
	voltage_init();
	i2c_init();
	INTCON2bits.GIE = 1;

	printf("STARTUP\n");
	clock_init();
	led_init();
	mma7660_init();
	nmea_init();
#ifdef ISO9141
	iso9141_init();
#endif
#ifdef J1939
	j1939_init();
#endif
#ifdef SEATS
	seats_init();
#endif
	flash_init();
	net_init();
	modem_init();
	data_init();
	mtimer_timeout_set(&t, 0);
	s = 0;
	i2c_init();
	while(1){
		if(mtimer_timeout(&t)){
			printf("UPTIME: %llu\n", (unsigned long long int)mtimer_get(NULL));
			mtimer_timeout_set(&t, 2000);
		}
		led_poll();
		mma7660_poll();
		nmea_poll();
#ifdef ISO9141
		iso9141_poll();
#endif
#ifdef J1939
		j1939_poll();
#endif
#ifdef SEATS
		seats_poll();
#endif
		if(s == 0){
			flash_poll();
			net_poll();
			data_poll();
		}
		if(serial_debug_read(&c, 1) == 1){
			if(c == 'r'){
				socket_reset();
			}else if(c == 'R'){
				__asm__("RESET");
			}else if(c == 's'){
				printf("STOP\n");
				s = 1;
			}
		}
		ClrWdt();
	}
}

uint16_t data_read(uint8_t *buffer){
	uint16_t l;
	l = 0;
	l += voltage_read(&buffer[l]);
	l += mma7660_read(&buffer[l]);
	l += nmea_read(&buffer[l]);
#ifdef ISO9141
	l += iso9141_read(&buffer[l]);
#endif
#ifdef J1939
	l += j1939_read(&buffer[l]);
#endif
#ifdef SEATS
	l += seats_read(&buffer[l]);
#endif
	return(l);
}

ssize_t write_stdout(const void *buf, size_t count){
	size_t i;
	for(i = 0; i < count; i++){
		if((((uint8_t *)buf)[i]) == '\n')while(serial_debug_write("\r", 1) != 1);
		while(serial_debug_write(&(((uint8_t *)buf)[i]), 1) != 1);
//		if((((uint8_t *)buf)[i]) == '\r')while(serial_debug_write("\n", 1) != 1);
	}
	return(count);
}

