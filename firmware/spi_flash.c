#include "spi_flash.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"
#include "mtimer.h"
#include <stdio.h>

// http://www.microchip.com/forums/m772800.aspx

// CS   = RC6, RP54
// SCK  = RC8, RP56
// MOSI = RC9, RP57
// MISO = RC7, RP55

#define spi_flash_critical_enter()          do{ INTCON2bits.GIE = 0; }while(0)
#define spi_flash_critical_exit()           do{ INTCON2bits.GIE = 1; }while(0)

#define SPI_FLASH_STATE_DONE                0

#define SPI_FLASH_STATE_READ_INIT0          1
#define SPI_FLASH_STATE_READ_STATUS0        2
#define SPI_FLASH_STATE_READ_ADDRESS1       3
#define SPI_FLASH_STATE_READ_ADDRESS2       4
#define SPI_FLASH_STATE_READ_ADDRESS3       5
#define SPI_FLASH_STATE_READ_START          6
#define SPI_FLASH_STATE_READ_DATA           7

#define SPI_FLASH_STATE_WRITE_INIT0         8
#define SPI_FLASH_STATE_WRITE_STATUS0       9
#define SPI_FLASH_STATE_WRITE_UNLOCK        10
#define SPI_FLASH_STATE_WRITE_INIT1         11
#define SPI_FLASH_STATE_WRITE_STATUS1       12
#define SPI_FLASH_STATE_WRITE_ADDRESS1      13
#define SPI_FLASH_STATE_WRITE_ADDRESS2      14
#define SPI_FLASH_STATE_WRITE_ADDRESS3      15
#define SPI_FLASH_STATE_WRITE_DATA          16
#define SPI_FLASH_STATE_WRITE_STOP          17
#define SPI_FLASH_STATE_WRITE_INIT2         18
#define SPI_FLASH_STATE_WRITE_STATUS2       19

#define SPI_FLASH_STATE_ERASE_INIT0         20
#define SPI_FLASH_STATE_ERASE_STATUS0       21
#define SPI_FLASH_STATE_ERASE_UNLOCK        22
#define SPI_FLASH_STATE_ERASE_INIT1         23
#define SPI_FLASH_STATE_ERASE_STATUS1       24
#define SPI_FLASH_STATE_ERASE_ADDRESS1      25
#define SPI_FLASH_STATE_ERASE_ADDRESS2      26
#define SPI_FLASH_STATE_ERASE_ADDRESS3      27
#define SPI_FLASH_STATE_ERASE_STOP          28
#define SPI_FLASH_STATE_ERASE_INIT2         29
#define SPI_FLASH_STATE_ERASE_STATUS2       30

#define SPI_FLASH_STATE_MASS_ERASE_INIT0    31
#define SPI_FLASH_STATE_MASS_ERASE_STATUS0  32
#define SPI_FLASH_STATE_MASS_ERASE_UNLOCK   33
#define SPI_FLASH_STATE_MASS_ERASE_INIT1    34
#define SPI_FLASH_STATE_MASS_ERASE_STATUS1  35
#define SPI_FLASH_STATE_MASS_ERASE_STOP     36
#define SPI_FLASH_STATE_MASS_ERASE_INIT2    37
#define SPI_FLASH_STATE_MASS_ERASE_STATUS2  38

static volatile uint8_t spi_flash_state, spi_flash_try, spi_flash_debug;
static uint32_t spi_flash_address;
static uint16_t spi_flash_loc, spi_flash_len;
static uint8_t *spi_flash_buffer;

static void spi_flash_start(uint8_t data);
static void spi_flash_stop();

void spi_flash_init(uint8_t speed){
	IEC2bits.SPI2IE = 0;
	IEC2bits.SPI2EIE = 0;

	spi_flash_state = SPI_FLASH_STATE_DONE;

	// CS
	TRISCbits.TRISC6 = 0;
	PORTCbits.RC6 = 1;

	// SCK - see tuleb kindlasti mäppida nii outputiks kui inputiks!
	RPOR7bits.RP56R = 9;
	RPINR22bits.SCK2R = 56;

	// MOSI
	RPOR7bits.RP57R = 8;

	// MISO
	RPINR22bits.SDI2R = 55;

	SPI2STAT = 0;
	SPI2CON1 = 0;
	SPI2CON2 = 0;
	SPI2CON1bits.MSTEN = 1;
	SPI2CON1bits.CKE = 1;
	if(speed){
		SPI2CON1bits.SPRE = 6;
		SPI2CON1bits.PPRE = 3;
	}
	SPI2STATbits.SPIEN = 1;

	IPC8bits.SPI2IP = 1;
	IPC8bits.SPI2EIP = 1;
}

void spi_flash_read_start(uint32_t offset, void *buf, uint16_t count){
	spi_flash_stop();
	if(!count)return;
	spi_flash_address = offset;
	spi_flash_len = count;
	spi_flash_loc = 0;
	spi_flash_buffer = (uint8_t *)buf;
	spi_flash_try = 0;
	spi_flash_state = SPI_FLASH_STATE_READ_INIT0;
	spi_flash_start(0x05);
}

void spi_flash_write_start(uint32_t offset, const void *buf, uint16_t count){
	spi_flash_stop();
	if(!count)return;
	spi_flash_address = offset;
	spi_flash_len = count;
	spi_flash_loc = 0;
	spi_flash_buffer = (uint8_t *)buf;
	spi_flash_try = 0;
	spi_flash_state = SPI_FLASH_STATE_WRITE_INIT0;
	spi_flash_start(0x05);
}

void spi_flash_erase_start(uint32_t offset){
	spi_flash_stop();
	spi_flash_address = offset;
	spi_flash_try = 0;
	spi_flash_state = SPI_FLASH_STATE_ERASE_INIT0;
	spi_flash_start(0x05);
}

void spi_flash_mass_erase_start(){
	spi_flash_stop();
	spi_flash_try = 0;
	spi_flash_state = SPI_FLASH_STATE_MASS_ERASE_INIT0;
	spi_flash_start(0x05);
}

uint8_t spi_flash_ready(){
	uint8_t ready;
	spi_flash_critical_enter();
	IEC2bits.SPI2IE = 0;
	ready = (spi_flash_state == SPI_FLASH_STATE_DONE);
	IEC2bits.SPI2IE = 1;
	spi_flash_critical_exit();
	return(ready);
}

static void spi_flash_start(uint8_t data){
	uint8_t t;
	PORTCbits.RC6 = 0;
	t = SPI2BUF;
	SPI2STATbits.SPIROV = 0;
	IFS2bits.SPI2IF = 0;
	IFS2bits.SPI2EIF = 0;
	IEC2bits.SPI2IE = 1;
	IEC2bits.SPI2EIE = 1;
	SPI2BUF = data;
}

static void spi_flash_stop(){
	IEC2bits.SPI2IE = 0;
	IEC2bits.SPI2EIE = 0;
	PORTCbits.RC6 = 1;
	spi_flash_state = SPI_FLASH_STATE_DONE;
}

void _SPI2Handler(){
	uint8_t t;
	t = SPI2BUF;
	IFS2bits.SPI2IF = 0;
	spi_flash_debug = t;
	/* READ */
	if(spi_flash_state == SPI_FLASH_STATE_READ_INIT0){
		SPI2BUF = 0x00;
		spi_flash_state = SPI_FLASH_STATE_READ_STATUS0;
	}else if(spi_flash_state == SPI_FLASH_STATE_READ_STATUS0){
		if((t & 0x01) == 0){
			spi_flash_stop();
			spi_flash_state = SPI_FLASH_STATE_READ_ADDRESS1;
			spi_flash_start(0x03);
		}else{
			if(spi_flash_try++ > 250){
				spi_flash_stop();
				spi_flash_try = 0;
				spi_flash_state = SPI_FLASH_STATE_READ_INIT0;
				spi_flash_start(0x05);
			}else{
				SPI2BUF = 0x00;
			}
		}
	}else if(spi_flash_state == SPI_FLASH_STATE_READ_ADDRESS1){
		SPI2BUF = ((spi_flash_address >> 16) & 0xFF);
		spi_flash_state = SPI_FLASH_STATE_READ_ADDRESS2;
	}else if(spi_flash_state == SPI_FLASH_STATE_READ_ADDRESS2){
		SPI2BUF = ((spi_flash_address >> 8) & 0xFF);
		spi_flash_state = SPI_FLASH_STATE_READ_ADDRESS3;
	}else if(spi_flash_state == SPI_FLASH_STATE_READ_ADDRESS3){
		SPI2BUF = ((spi_flash_address >> 0) & 0xFF);
		spi_flash_state = SPI_FLASH_STATE_READ_START;
	}else if(spi_flash_state == SPI_FLASH_STATE_READ_START){
		SPI2BUF = 0x00;
		spi_flash_state = SPI_FLASH_STATE_READ_DATA;
	}else if(spi_flash_state == SPI_FLASH_STATE_READ_DATA){
		spi_flash_buffer[spi_flash_loc++] = t;
		if(spi_flash_loc >= spi_flash_len){
			spi_flash_stop();
		}else{
			SPI2BUF = 0x00;
		}
	/* WRITE */
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_INIT0){
		SPI2BUF = 0x00;
		spi_flash_state = SPI_FLASH_STATE_WRITE_STATUS0;
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_STATUS0){
		if((t & 0x01) == 0){
			spi_flash_stop();
			spi_flash_state = SPI_FLASH_STATE_WRITE_UNLOCK;
			spi_flash_start(0x06);
		}else{
			if(spi_flash_try++ > 250){
				spi_flash_stop();
				spi_flash_try = 0;
				spi_flash_state = SPI_FLASH_STATE_WRITE_INIT0;
				spi_flash_start(0x05);
			}else{
				SPI2BUF = 0x00;
			}
		}
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_UNLOCK){
		spi_flash_stop();
		spi_flash_state = SPI_FLASH_STATE_WRITE_INIT1;
		spi_flash_start(0x05);
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_INIT1){
		SPI2BUF = 0x00;
		spi_flash_state = SPI_FLASH_STATE_WRITE_STATUS1;
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_STATUS1){
		if((t & 0x03) == 2){
			spi_flash_stop();
			spi_flash_state = SPI_FLASH_STATE_WRITE_ADDRESS1;
			spi_flash_start(0x02);
		}else{
			if(spi_flash_try++ > 250){
				spi_flash_stop();
				spi_flash_try = 0;
				spi_flash_state = SPI_FLASH_STATE_WRITE_INIT0;
				spi_flash_start(0x05);
			}else{
				SPI2BUF = 0x00;
			}
		}
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_ADDRESS1){
		SPI2BUF = ((spi_flash_address >> 16) & 0xFF);
		spi_flash_state = SPI_FLASH_STATE_WRITE_ADDRESS2;
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_ADDRESS2){
		SPI2BUF = ((spi_flash_address >> 8) & 0xFF);
		spi_flash_state = SPI_FLASH_STATE_WRITE_ADDRESS3;
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_ADDRESS3){
		SPI2BUF = ((spi_flash_address >> 0) & 0xFF);
		spi_flash_state = SPI_FLASH_STATE_WRITE_DATA;
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_DATA){
		SPI2BUF = spi_flash_buffer[spi_flash_loc++];
		if(spi_flash_loc >= spi_flash_len)spi_flash_state = SPI_FLASH_STATE_WRITE_STOP;
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_STOP){
		spi_flash_stop();
		spi_flash_state = SPI_FLASH_STATE_WRITE_INIT2;
		spi_flash_start(0x05);
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_INIT2){
		SPI2BUF = 0x00;
		spi_flash_state = SPI_FLASH_STATE_WRITE_STATUS2;
	}else if(spi_flash_state == SPI_FLASH_STATE_WRITE_STATUS2){
		if((t & 0x03) == 0){
			spi_flash_stop();
		}else{
			SPI2BUF = 0x00;
		}
	/* ERASE */
	}else if(spi_flash_state == SPI_FLASH_STATE_ERASE_INIT0){
		SPI2BUF = 0x00;
		spi_flash_state = SPI_FLASH_STATE_ERASE_STATUS0;
	}else if(spi_flash_state == SPI_FLASH_STATE_ERASE_STATUS0){
		if((t & 0x01) == 0){
			spi_flash_stop();
			spi_flash_state = SPI_FLASH_STATE_ERASE_UNLOCK;
			spi_flash_start(0x06);
		}else{
			if(spi_flash_try++ > 250){
				spi_flash_stop();
				spi_flash_try = 0;
				spi_flash_state = SPI_FLASH_STATE_ERASE_INIT0;
				spi_flash_start(0x05);
			}else{
				SPI2BUF = 0x00;
			}
		}
	}else if(spi_flash_state == SPI_FLASH_STATE_ERASE_UNLOCK){
		spi_flash_stop();
		spi_flash_state = SPI_FLASH_STATE_ERASE_INIT1;
		spi_flash_start(0x05);
	}else if(spi_flash_state == SPI_FLASH_STATE_ERASE_INIT1){
		SPI2BUF = 0x00;
		spi_flash_state = SPI_FLASH_STATE_ERASE_STATUS1;
	}else if(spi_flash_state == SPI_FLASH_STATE_ERASE_STATUS1){
		if((t & 0x03) == 2){
			spi_flash_stop();
			spi_flash_state = SPI_FLASH_STATE_ERASE_ADDRESS1;
			spi_flash_start(0x20);
		}else{
			if(spi_flash_try++ > 250){
				spi_flash_stop();
				spi_flash_try = 0;
				spi_flash_state = SPI_FLASH_STATE_ERASE_INIT0;
				spi_flash_start(0x05);
			}else{
				SPI2BUF = 0x00;
			}
		}
	}else if(spi_flash_state == SPI_FLASH_STATE_ERASE_ADDRESS1){
		SPI2BUF = ((spi_flash_address >> 16) & 0xFF);
		spi_flash_state = SPI_FLASH_STATE_ERASE_ADDRESS2;
	}else if(spi_flash_state == SPI_FLASH_STATE_ERASE_ADDRESS2){
		SPI2BUF = ((spi_flash_address >> 8) & 0xFF);
		spi_flash_state = SPI_FLASH_STATE_ERASE_ADDRESS3;
	}else if(spi_flash_state == SPI_FLASH_STATE_ERASE_ADDRESS3){
		SPI2BUF = ((spi_flash_address >> 0) & 0xFF);
		spi_flash_state = SPI_FLASH_STATE_ERASE_STOP;
	}else if(spi_flash_state == SPI_FLASH_STATE_ERASE_STOP){
		spi_flash_stop();
		spi_flash_state = SPI_FLASH_STATE_ERASE_INIT2;
		spi_flash_start(0x05);
	}else if(spi_flash_state == SPI_FLASH_STATE_ERASE_INIT2){
		SPI2BUF = 0x00;
		spi_flash_state = SPI_FLASH_STATE_ERASE_STATUS2;
	}else if(spi_flash_state == SPI_FLASH_STATE_ERASE_STATUS2){
		if((t & 0x03) == 0){
			spi_flash_stop();
		}else{
			SPI2BUF = 0x00;
		}
	/* MASS ERASE */
	}else if(spi_flash_state == SPI_FLASH_STATE_MASS_ERASE_INIT0){
		SPI2BUF = 0x00;
		spi_flash_state = SPI_FLASH_STATE_MASS_ERASE_STATUS0;
	}else if(spi_flash_state == SPI_FLASH_STATE_MASS_ERASE_STATUS0){
		if((t & 0x01) == 0){
			spi_flash_stop();
			spi_flash_state = SPI_FLASH_STATE_MASS_ERASE_UNLOCK;
			spi_flash_start(0x06);
		}else{
			if(spi_flash_try++ > 250){
				spi_flash_stop();
				spi_flash_try = 0;
				spi_flash_state = SPI_FLASH_STATE_MASS_ERASE_INIT0;
				spi_flash_start(0x05);
			}else{
				SPI2BUF = 0x00;
			}
		}
	}else if(spi_flash_state == SPI_FLASH_STATE_MASS_ERASE_UNLOCK){
		spi_flash_stop();
		spi_flash_state = SPI_FLASH_STATE_MASS_ERASE_INIT1;
		spi_flash_start(0x05);
	}else if(spi_flash_state == SPI_FLASH_STATE_MASS_ERASE_INIT1){
		SPI2BUF = 0x00;
		spi_flash_state = SPI_FLASH_STATE_MASS_ERASE_STATUS1;
	}else if(spi_flash_state == SPI_FLASH_STATE_MASS_ERASE_STATUS1){
		if((t & 0x03) == 2){
			spi_flash_stop();
			spi_flash_state = SPI_FLASH_STATE_MASS_ERASE_STOP;
			spi_flash_start(0x60);
		}else{
			if(spi_flash_try++ > 250){
				spi_flash_stop();
				spi_flash_try = 0;
				spi_flash_state = SPI_FLASH_STATE_MASS_ERASE_INIT0;
				spi_flash_start(0x05);
			}else{
				SPI2BUF = 0x00;
			}
		}
	}else if(spi_flash_state == SPI_FLASH_STATE_MASS_ERASE_STOP){
		spi_flash_stop();
		spi_flash_state = SPI_FLASH_STATE_MASS_ERASE_INIT2;
		spi_flash_start(0x05);
	}else if(spi_flash_state == SPI_FLASH_STATE_MASS_ERASE_INIT2){
		SPI2BUF = 0x00;
		spi_flash_state = SPI_FLASH_STATE_MASS_ERASE_STATUS2;
	}else if(spi_flash_state == SPI_FLASH_STATE_MASS_ERASE_STATUS2){
		if((t & 0x03) == 0){
			spi_flash_stop();
		}else{
			SPI2BUF = 0x00;
		}
	}else{
		spi_flash_stop();
	}
}

void _SPI2ErrHandler(){
	SPI2STATbits.SPIROV = 0;
	IFS2bits.SPI2EIF = 0;
}

