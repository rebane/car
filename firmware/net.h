#ifndef _NET_H_
#define _NET_H_

#include <stdint.h>

void net_init();
void net_poll();

void net_connected();

void socket_reset();
int16_t socket_read(void *buf, uint16_t count);
int16_t socket_write(const void *buf, uint16_t count);
int16_t socket_write_space(void);

#endif

