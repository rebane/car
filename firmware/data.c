#include "data.h"
#include <stdio.h>
#include <xc.h>
#include <libpic30.h>
#include "mtimer.h"
#include "pmem.h"
#include "header_parser.h"
#include "firmware.h"
#include "bjson.h"
#include "flash.h"
#include "net.h"
#include "protocol.h"
#include "clock.h"
#include "nmea.h"
#include "voltage.h"
#include "modem.h"

#define DATA_STATE_READ   1
#define DATA_STATE_SEND   2
#define DATA_STATE_RECV   3
#define DATA_STATE_DELETE 4

#define DATA_SEND_LEN     FLASH_DATA_LEN
#define DATA_RECV_LEN     PMEM_PAGE_SIZE // seda kasutatakse alguses identi saatmiseks
#define DATA_BUF_LEN      4096

static struct{
	struct protocol_header header;
	uint8_t data[DATA_SEND_LEN];
}__attribute__((packed)) data_send_buffer;

static struct{
	struct protocol_header header;
	uint8_t data[DATA_RECV_LEN];
}__attribute__((packed)) data_recv_buffer;

static struct protocol_header data_firmware_ack_buffer;

static mtimer_t data_timer;
static uint8_t data_state, data_buffer[DATA_BUF_LEN];
static uint16_t data_loc, data_send_loc;
static uint32_t data_recv_loc, data_firmware_loc;
static uint32_t data_firmware_crc;
static uint16_t data_firmware_version;
static uint8_t data_hardware_platform;

static void data_firmware_save(void *data, uint32_t offset, uint16_t len);
static uint32_t data_firmware_crc_get(uint32_t start, void *buffer);
static uint16_t data_firmware_version_get(uint32_t start, void *buffer);
static uint8_t data_hardware_platform_get(uint32_t start, void *buffer);

void data_init(){
	data_firmware_crc = data_firmware_crc_get(LOAD_ADDRESS, data_recv_buffer.data);
	data_firmware_version = data_firmware_version_get(LOAD_ADDRESS, data_recv_buffer.data);
	data_hardware_platform = data_hardware_platform_get(LOAD_ADDRESS, data_recv_buffer.data);
	mtimer_timeout_set(&data_timer, 0);
	data_recv_loc = data_send_loc = 0;
	protocol_set_len(&data_send_buffer.header, 0);
	data_state = DATA_STATE_READ;
	protocol_set_command(&data_firmware_ack_buffer, PROTOCOL_COMMAND_FIRMWARE);
}

void data_poll(){
	int16_t i, l;
	uint16_t j;
	uint8_t f, *data;
	if(mtimer_timeout(&data_timer)){
		if(flash_write_ready()){
			data = data_buffer;
			j = 3;
			l = clock_read(&data[j]);
			if(l){
				j += l;
				l = j;
				j += data_read(&data[j]);
				if(j > l){
					if(modem_imsi() != NULL){
						j += bjson_string(&data[j], "imsi");
						j += bjson_string(&data[j], modem_imsi());
					}
					if(modem_custom() != NULL){
						j += bjson_string(&data[j], "custom");
						j += bjson_string(&data[j], modem_custom());
					}
					j += bjson_string(&data[j], "firmwareCRC");
					j += bjson_positive(&data[j], data_firmware_crc);
					j += bjson_string(&data[j], "firmwareVersion");
					j += bjson_positive(&data[j], data_firmware_version);
					j += bjson_string(&data[j], "uptime");
					j += bjson_positive(&data[j], mtimer_get(NULL));
					bjson_value16(data, j - 3, BJSON_BASE_MAP);
					flash_write(&data[0], j);
					mtimer_timeout_set(&data_timer, 5000);
				}
			}
		}
	}
	l = socket_read(data_buffer, DATA_BUF_LEN);
	if(l > 0){
		for(i = 0; i < l; i++){
			if(data_recv_loc < (DATA_RECV_LEN + PROTOCOL_HEADER_LEN))((char *)&data_recv_buffer)[data_recv_loc] = data_buffer[i];
			data_recv_loc++;
			if((data_recv_loc == 5) && (protocol_get_command(&data_recv_buffer.header) == PROTOCOL_COMMAND_FIRMWARE))data_firmware_loc = 0;
			if(data_recv_loc > 15){
				if((data_recv_loc == (PROTOCOL_HEADER_LEN + PMEM_PAGE_SIZE)) && (protocol_get_command(&data_recv_buffer.header) == PROTOCOL_COMMAND_FIRMWARE)){
					data_firmware_save(data_recv_buffer.data, data_firmware_loc, PMEM_PAGE_SIZE);
					data_firmware_loc += PMEM_PAGE_SIZE;
					data_recv_loc -= PMEM_PAGE_SIZE;
					protocol_set_len(&data_recv_buffer.header, protocol_get_len(&data_recv_buffer.header) - PMEM_PAGE_SIZE);
				}
				if((data_recv_loc >= (PROTOCOL_HEADER_LEN + protocol_get_len(&data_recv_buffer.header)))){
					if(protocol_get_command(&data_recv_buffer.header) == PROTOCOL_COMMAND_DATE){
						clock_set(protocol_get_uid(&data_recv_buffer.header), CLOCK_SOURCE_NET);
						printf("GOT CLOCK: %llu\n", (unsigned long long int)protocol_get_uid(&data_recv_buffer.header));
					}else if(protocol_get_command(&data_recv_buffer.header) == PROTOCOL_COMMAND_DATA_ACK){
						printf("GOT DATA ACK, UID: %llu\n", (unsigned long long int)protocol_get_uid(&data_recv_buffer.header));
						if((data_state == DATA_STATE_RECV) && (protocol_get_uid(&data_recv_buffer.header) == protocol_get_uid(&data_send_buffer.header)))data_state = DATA_STATE_DELETE;
					}else if(protocol_get_command(&data_recv_buffer.header) == PROTOCOL_COMMAND_FIRMWARE){
						data_firmware_save(data_recv_buffer.data, data_firmware_loc, protocol_get_len(&data_recv_buffer.header));
						data_firmware_loc += protocol_get_len(&data_recv_buffer.header);
						printf("FIRMWARE DONE\n");
						protocol_set_len(&data_firmware_ack_buffer, 0);
						protocol_set_command(&data_firmware_ack_buffer, PROTOCOL_COMMAND_FIRMWARE_ACK);
						protocol_set_status(&data_firmware_ack_buffer, firmware_validate(LOAD_NEW_ADDRESS, &data_firmware_loc, data_recv_buffer.data));
						protocol_set_len2(&data_firmware_ack_buffer, 0);
						protocol_set_uid(&data_firmware_ack_buffer, protocol_get_uid(&data_recv_buffer.header));
					}else if(protocol_get_command(&data_recv_buffer.header) == PROTOCOL_COMMAND_REBOOT){
						__asm__("RESET");
					}
					data_recv_loc = 0;
				}
			}
		}
	}else if(l < 0){
		data_recv_loc = data_send_loc = 0;
		data_state = DATA_STATE_READ;
		protocol_set_command(&data_firmware_ack_buffer, PROTOCOL_COMMAND_FIRMWARE);
	}
	if(data_state == DATA_STATE_DELETE){
		if(flash_write_ready()){
			// printf("DATA DELETE: %llu, %u\n", (unsigned long long int)protocol_get_uid(&data_recv_buffer.header), (unsigned int)data_loc);
			flash_delete(protocol_get_uid(&data_recv_buffer.header), data_loc);
			protocol_set_len(&data_send_buffer.header, 0);
			data_state = DATA_STATE_READ;
		}
	}
	if(data_state == DATA_STATE_READ){
		if(protocol_get_command(&data_firmware_ack_buffer) == PROTOCOL_COMMAND_FIRMWARE_ACK){
			if(socket_write_space() >= PROTOCOL_HEADER_LEN){
				protocol_set_len(&data_firmware_ack_buffer, 0);
				socket_write(&data_firmware_ack_buffer, PROTOCOL_HEADER_LEN);
				protocol_set_command(&data_firmware_ack_buffer, PROTOCOL_COMMAND_FIRMWARE);
			}
		}else{
			if(flash_read_ready()){
				protocol_set_len(&data_send_buffer.header, flash_read(data_send_buffer.data, &data_send_buffer.header.uid, &data_loc, &j, &f)); /**/
				if(protocol_get_len(&data_send_buffer.header)){
					protocol_set_command(&data_send_buffer.header, PROTOCOL_COMMAND_DATA);
					if(j)j--;
					protocol_set_len2(&data_send_buffer.header, j);
					protocol_set_status(&data_send_buffer.header, f);
					protocol_set_uid(&data_send_buffer.header, data_send_buffer.header.uid); /* correct endianness */
				}
				data_send_loc = 0;
				// printf("DATA READ: %llu, %u\n", (unsigned long long int)protocol_get_uid(&data_send_buffer.header), (unsigned int)data_loc);
			}
			if(protocol_get_len(&data_send_buffer.header) > 0)data_state = DATA_STATE_SEND;
		}
	}
	if(data_state == DATA_STATE_SEND){
		l = socket_write(&((uint8_t *)&data_send_buffer)[data_send_loc], (protocol_get_len(&data_send_buffer.header) + PROTOCOL_HEADER_LEN) - data_send_loc);
		if(l > 0){
			data_send_loc += l;
			if(data_send_loc >= (protocol_get_len(&data_send_buffer.header) + PROTOCOL_HEADER_LEN)){
				protocol_set_len(&data_send_buffer.header, 0);
				data_send_loc = 0;
				data_state = DATA_STATE_RECV;
				// printf("DATA SENT: %llu, %u\n", (unsigned long long int)protocol_get_uid(&data_send_buffer.header), (unsigned int)data_loc);
			}
		}else if(l < 0){
			data_recv_loc = data_send_loc = 0;
			data_state = DATA_STATE_READ;
		}
	}
}

void net_connected(){
	uint16_t j;
	uint8_t *data;
	data = data_recv_buffer.data;
	j = 3;
	j += clock_read(&data[j]);

	if(modem_imei() != NULL){
		j += bjson_string(&data[j], "imei");
		j += bjson_string(&data[j], modem_imei());
	}
	if(modem_imsi() != NULL){
		j += bjson_string(&data[j], "imsi");
		j += bjson_string(&data[j], modem_imsi());
	}
	if(modem_custom() != NULL){
		j += bjson_string(&data[j], "custom");
		j += bjson_string(&data[j], modem_custom());
	}
	j += bjson_string(&data[j], "firmwareCRC");
	j += bjson_positive(&data[j], data_firmware_crc);

	j += bjson_string(&data[j], "firmwareVersion");
	j += bjson_positive(&data[j], data_firmware_version);

	j += bjson_string(&data[j], "hardwarePlatform");
	j += bjson_positive(&data[j], data_hardware_platform);

	j += bjson_string(&data[j], "uptime");
	j += bjson_positive(&data[j], mtimer_get((void *)0));

	protocol_set_len(&data_recv_buffer.header, j);
	j -= 3;
	bjson_value16(data, j, BJSON_BASE_MAP);
	protocol_set_command(&data_recv_buffer.header, PROTOCOL_COMMAND_IDENT);
	protocol_set_status(&data_recv_buffer.header, modem_serial((uint8_t *)&data_recv_buffer.header.uid));
	protocol_set_len2(&data_recv_buffer.header, DATA_RECV_LEN);
	socket_write(&data_recv_buffer, protocol_get_len(&data_recv_buffer.header) + PROTOCOL_HEADER_LEN);
}

static void data_firmware_save(void *data, uint32_t offset, uint16_t len){
	if(!len)return;
	pmem_erase_page_containing(offset + LOAD_NEW_ADDRESS);
	pmem_cpy_d2p(offset + LOAD_NEW_ADDRESS, data, len);
	printf("FIRMWARE SAVE, OFFSET: %lu, LEN: %u\n", (unsigned long int)offset, (unsigned int)len);
}

static uint32_t data_firmware_crc_get(uint32_t start, void *buffer){
	void *header;
	pmem_cpy_p2d(buffer, start, PMEM_PAGE_SIZE);
	header = header_validate(buffer, &((uint8_t *)buffer)[PMEM_PAGE_SIZE - 1]);
	if(header == ((void *)0))return(0);
	return(header_value(header, HEADER_CRC));
}

static uint16_t data_firmware_version_get(uint32_t start, void *buffer){
	void *header;
	pmem_cpy_p2d(buffer, start, PMEM_PAGE_SIZE);
	header = header_validate(buffer, &((uint8_t *)buffer)[PMEM_PAGE_SIZE - 1]);
	if(header == ((void *)0))return(0);
	return(((uint16_t)header_value(header, HEADER_VERSION_MAJOR) << 8) | header_value(header, HEADER_VERSION_MINOR));
}

static uint8_t data_hardware_platform_get(uint32_t start, void *buffer){
	void *header;
	pmem_cpy_p2d(buffer, start, PMEM_PAGE_SIZE);
	header = header_validate(buffer, &((uint8_t *)buffer)[PMEM_PAGE_SIZE - 1]);
	if(header == ((void *)0))return(0);
	return(header_value(header, HEADER_TARGET_PLATFORM));
}

