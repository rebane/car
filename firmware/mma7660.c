#include "mma7660.h"
#include <stdio.h>
#include "mtimer.h"
#include "i2c.h"
#include "bjson.h"

#define MMA7660_I2C_ADDRESS       0x4C
#define MMA7660_INTERVAL_TIMEOUT  2000

#define MMA7660_REGISTER_XOUT     0x00
#define MMA7660_REGISTER_YOUT     0x01
#define MMA7660_REGISTER_ZOUT     0x02
#define MMA7660_REGISTER_TILT     0x03
#define MMA7660_REGISTER_SRST     0x04
#define MMA7660_REGISTER_SPCNT    0x05
#define MMA7660_REGISTER_INTSU    0x06
#define MMA7660_REGISTER_MODE     0x07
#define MMA7660_REGISTER_SR       0x08
#define MMA7660_REGISTER_PDET     0x09
#define MMA7660_REGISTER_PD       0x0A

#define MMA7660_STATE_READ        0
#define MMA7660_STATE_READ_WAIT   1
#define MMA7660_STATE_WRITE       2
#define MMA7660_STATE_ENABLE      3
#define MMA7660_STATE_ENABLE_WAIT 4

static uint8_t mma7660_state, mma7660_reg[11];
static uint8_t mma7660_ok[3];
static int32_t mma7660_val[3];
static int32_t mma7660_count[3];
static int32_t mma7660_calc[3];
static mtimer_t mma7660_timer, mma7660_interval_timer;

void mma7660_init(){
	mma7660_state = MMA7660_STATE_READ;
	mma7660_ok[0] = mma7660_ok[1] = mma7660_ok[2] = 0;
	mma7660_count[0] = mma7660_count[1] = mma7660_count[2] = 0;
	mma7660_calc[0] = mma7660_calc[1] = mma7660_calc[2] = 0;
	mtimer_timeout_set(&mma7660_timer, 0);
	mtimer_timeout_set(&mma7660_interval_timer, MMA7660_INTERVAL_TIMEOUT);
}

void mma7660_poll(){
	int8_t i, status;
	if(mtimer_timeout(&mma7660_interval_timer)){
//		printf("AVG:");
		for(i = 0; i < 3; i++){
			// printf(" %lld %llu", (long long int)mma7660_calc[i], (unsigned long long int)mma7660_count[i]);
			if(mma7660_count[i]){
				mma7660_val[i] = (mma7660_calc[i] * 10) / mma7660_count[i];
				mma7660_ok[i] = 1;
				mma7660_calc[i] = 0;
				mma7660_count[i] = 0;
			}else{
				mma7660_ok[i] = 0;
			}
//			printf(" %lld", (long long int)mma7660_val[i]);
		}
//		printf("\n");
		mtimer_timeout_set(&mma7660_interval_timer, MMA7660_INTERVAL_TIMEOUT);
	}
	if(mma7660_state == MMA7660_STATE_READ){
		if(mtimer_timeout(&mma7660_timer)){
			i2c_read(MMA7660_I2C_ADDRESS, 0x00, mma7660_reg, 11);
			mma7660_state = MMA7660_STATE_READ_WAIT;
			mtimer_timeout_set(&mma7660_timer, 500);
		}
	}else if(mma7660_state == MMA7660_STATE_READ_WAIT){
		if(!mtimer_timeout(&mma7660_timer)){
			status = i2c_ready();
			if(status < 0){
				mma7660_state = MMA7660_STATE_READ;
				mtimer_timeout_set(&mma7660_timer, 500);
			}else if(status > 0){
			//	printf("MMA:");
				for(i = 0; i < 3; i++){
					if(!(mma7660_reg[i] & 0x40)){
						if(mma7660_reg[i] & 0x20){
							status = ((int)(mma7660_reg[i] & 0x1F)) - 32;
						}else{
							status = (mma7660_reg[i] & 0x1F);
						}
						mma7660_calc[i] += status;
						mma7660_count[i]++;
					}
			//		printf("  %03d", (int)status);
				}
			//	printf("\n");
				if((mma7660_reg[MMA7660_REGISTER_MODE] != 0x01) || (mma7660_reg[MMA7660_REGISTER_SR] != 0xE0)){
					i2c_write(MMA7660_I2C_ADDRESS, MMA7660_REGISTER_MODE, "\x00", 1);
					mma7660_state = MMA7660_STATE_WRITE;
					mtimer_timeout_set(&mma7660_timer, 500);
				}else{
					mma7660_state = MMA7660_STATE_READ;
					mtimer_timeout_set(&mma7660_timer, 0);
				}
			}
		}else{
			mma7660_state = MMA7660_STATE_READ;
		}
	}else if(mma7660_state == MMA7660_STATE_WRITE){
		if(!mtimer_timeout(&mma7660_timer)){
			status = i2c_ready();
			if(status < 0){
				mma7660_state = MMA7660_STATE_READ;
				mtimer_timeout_set(&mma7660_timer, 500);
			}else if(status > 0){
				i2c_write(MMA7660_I2C_ADDRESS, MMA7660_REGISTER_SR, "\xE0", 1);
				mma7660_state = MMA7660_STATE_ENABLE;
				mtimer_timeout_set(&mma7660_timer, 500);
			}
		}else{
			mma7660_state = MMA7660_STATE_READ;
		}
	}else if(mma7660_state == MMA7660_STATE_ENABLE){
		if(!mtimer_timeout(&mma7660_timer)){
			status = i2c_ready();
			if(status < 0){
				mma7660_state = MMA7660_STATE_READ;
				mtimer_timeout_set(&mma7660_timer, 500);
			}else if(status > 0){
				i2c_write(MMA7660_I2C_ADDRESS, MMA7660_REGISTER_MODE, "\x01", 1);
				mma7660_state = MMA7660_STATE_ENABLE_WAIT;
				mtimer_timeout_set(&mma7660_timer, 500);
			}
		}else{
			mma7660_state = MMA7660_STATE_READ;
		}
	}else if(mma7660_state == MMA7660_STATE_ENABLE_WAIT){
		if(!mtimer_timeout(&mma7660_timer)){
			status = i2c_ready();
			if(status < 0){
				mma7660_state = MMA7660_STATE_READ;
				mtimer_timeout_set(&mma7660_timer, 500);
			}else if(status > 0){
				mma7660_state = MMA7660_STATE_READ;
				mtimer_timeout_set(&mma7660_timer, 500);
			}
		}else{
			mma7660_state = MMA7660_STATE_READ;
		}
	}
}

uint16_t mma7660_read(uint8_t *buffer){
	uint16_t l;
	l = 0;
	if(mma7660_ok[0]){
		l += bjson_string(&buffer[l], "accelX");
		if(mma7660_val[0] < 0){
			l += bjson_negative(&buffer[l], -mma7660_val[0]);
		}else{
			l += bjson_positive(&buffer[l], mma7660_val[0]);
		}
	}
	if(mma7660_ok[1]){
		l += bjson_string(&buffer[l], "accelY");
		if(mma7660_val[1] < 0){
			l += bjson_negative(&buffer[l], -mma7660_val[1]);
		}else{
			l += bjson_positive(&buffer[l], mma7660_val[1]);
		}
	}
	if(mma7660_ok[2]){
		l += bjson_string(&buffer[l], "accelZ");
		if(mma7660_val[2] < 0){
			l += bjson_negative(&buffer[l], -mma7660_val[2]);
		}else{
			l += bjson_positive(&buffer[l], mma7660_val[2]);
		}
	}
	return(l);
}

