#include "net.h"
#include <stdio.h>
#include <xc.h>
#include "platform.h"
#include "mtimer.h"
#include "ppp.h"
#include "uip.h"
#include "serial_gsm.h"
#include "modem.h"
#include "led.h"

#define NET_UIP_ID                1
#define SOCKET_SEND_FIFO_LEN      512
#define SOCKET_RECV_FIFO_LEN      1600

#define NET_STATE_MODEM_PWR       1
#define NET_STATE_MODEM_PWR_WAIT  2
#define NET_STATE_MODEM_AT_SEND1  3
#define NET_STATE_MODEM_AT_SEND2  4
#define NET_STATE_MODEM_AT_SEND3  5
#define NET_STATE_MODEM_AT_WAIT   6
#define NET_STATE_MODEM_GETCONFIG 7
#define NET_STATE_MODEM_INIT      8
#define NET_STATE_MODEM_INIT2     9
#define NET_STATE_MODEM_INIT3     10
#define NET_STATE_MODEM_INIT4     11
#define NET_STATE_PPP_INIT        12
#define NET_STATE_PPP_POLL        13

#define SOCKET_STATE_CONNECT      1
#define SOCKET_STATE_CONNECT_WAIT 2
#define SOCKET_STATE_CONNECTED    3

static uint8_t net_state, net_try, net_buffer[16], net_buffer_loc;
static mtimer_t net_timer, net_periodic_timer;
static struct uip_conn *net_conn;

static volatile uint8_t socket_send_fifo[SOCKET_SEND_FIFO_LEN];
static volatile uint8_t socket_recv_fifo[SOCKET_RECV_FIFO_LEN];
static volatile uint16_t socket_recv_fifo_in, socket_recv_fifo_out, socket_recv_fifo_len;
static volatile uint16_t socket_send_fifo_in, socket_send_fifo_out, socket_send_fifo_len, socket_last_sent;
static uint8_t socket_state;
static mtimer_t socket_timer;

void net_init(){
	net_conn = uip_conn_id(NET_UIP_ID);
	net_state = NET_STATE_MODEM_PWR;
	mtimer_timeout_set(&net_timer, 0);
	led_set(1, LED_STATE_ON, 0, 0);
}

void net_poll(){
	uint8_t c;
	int8_t s;
	uip_ipaddr_t ipaddr;
	if(net_state == NET_STATE_MODEM_PWR){
		if(mtimer_timeout(&net_timer)){
			PORTAbits.RA7 = 0;
			printf("MODEM: PWRKEY LOW\n");
			mtimer_timeout_set(&net_timer, 2000);
			net_state = NET_STATE_MODEM_PWR_WAIT;
			net_try = 0;
		}
	}
	if(net_state == NET_STATE_MODEM_PWR_WAIT){
		if(mtimer_timeout(&net_timer)){
			PORTAbits.RA7 = 1;
			printf("MODEM: PWRKEY HI\n");
			mtimer_timeout_set(&net_timer, 4000);
			net_state = NET_STATE_MODEM_AT_SEND1;
		}
	}
	if(net_state == NET_STATE_MODEM_AT_SEND1){
		if(mtimer_timeout(&net_timer)){
			printf("MODEM: WRITE A\n");
			serial_gsm_write("A", 1);
			mtimer_timeout_set(&net_timer, 500);
			net_state = NET_STATE_MODEM_AT_SEND2;
		}
	}
	if(net_state == NET_STATE_MODEM_AT_SEND2){
		if(mtimer_timeout(&net_timer)){
			printf("MODEM: WRITE T\n");
			serial_gsm_write("T", 1);
			net_buffer_loc = 0;
			mtimer_timeout_set(&net_timer, 500);
			net_state = NET_STATE_MODEM_AT_SEND3;
		}
	}
	if(net_state == NET_STATE_MODEM_AT_SEND3){
		if(mtimer_timeout(&net_timer)){
			printf("MODEM: WAITING AT ANSWER\n");
			serial_gsm_read_flush();
			serial_gsm_write("\r", 1);
			net_buffer_loc = 0;
			mtimer_timeout_set(&net_timer, 2000);
			net_state = NET_STATE_MODEM_AT_WAIT;
		}
	}
	if(net_state == NET_STATE_MODEM_AT_WAIT){
		if(serial_gsm_read(&c, 1) > 0){
			if((c == '\r') || (c == '\n')){
				net_buffer[net_buffer_loc] = 0;
				if(!memcmp((char *)net_buffer, "OK", 2)){
					printf("MODEM: GET CONFIG\n");
					modem_getconfig_reset();
					mtimer_timeout_set(&net_timer, 20000);
					net_state = NET_STATE_MODEM_GETCONFIG;
				}
				net_buffer_loc = 0;
			}else{
				if(net_buffer_loc < 15)net_buffer[net_buffer_loc++] = c;
			}
		}
		if(mtimer_timeout(&net_timer)){
			printf("MODEM: NO AT ANSWER\n");
			net_state = NET_STATE_MODEM_PWR;
		}
	}
	if(net_state == NET_STATE_MODEM_GETCONFIG){
		s = modem_getconfig();
		if(s > 0){
			printf("MODEM: IMEI: %s\n", modem_imei());
			if(modem_imsi() != NULL)printf("IMSI: %s\n", modem_imsi());
			printf("MODEM: APN: %s\n", modem_apn());
			printf("MODEM: IP: %u.%u.%u.%u\n", (unsigned int)((uint8_t *)modem_address())[0], (unsigned int)((uint8_t *)modem_address())[1], (unsigned int)((uint8_t *)modem_address())[2], (unsigned int)((uint8_t *)modem_address())[3]);
			printf("MODEM: PORT: %u\n", (unsigned int)modem_port());
			if(modem_custom() != NULL)printf("CUSTOM: %s\n", modem_custom());
			net_state = NET_STATE_MODEM_INIT3;
			led_set(1, LED_STATE_BLINK, 500, 500);
		}else if(s < 0){
			printf("MODEM: GET CONFIG TIMEOUT/ERROR\n");
			net_state = NET_STATE_MODEM_PWR;
			led_set(1, LED_STATE_OFF, 0, 0);
		}
	}
	if(net_state == NET_STATE_MODEM_INIT){
		if(mtimer_timeout(&net_timer)){
			serial_gsm_write("+++", 3);
			printf("MODEM: SENDING +++\n");
			mtimer_timeout_set(&net_timer, 3000);
			net_state = NET_STATE_MODEM_INIT2;
		}
	}
	if(net_state == NET_STATE_MODEM_INIT2){
		if(mtimer_timeout(&net_timer)){
			serial_gsm_write("\rATH\r", 5);
			printf("MODEM: SENDING ATH\n");
			mtimer_timeout_set(&net_timer, 1000);
			net_state = NET_STATE_MODEM_INIT3;
		}
	}
	if(net_state == NET_STATE_MODEM_INIT3){
		if(mtimer_timeout(&net_timer)){
			serial_gsm_write("AT+CGDCONT=1,\"IP\",\"", 19);
			serial_gsm_write(modem_apn(), strlen(modem_apn()));
			serial_gsm_write("\"\r", 2);
			printf("MODEM: SETTING APN\n");
			mtimer_timeout_set(&net_timer, 500);
			net_state = NET_STATE_MODEM_INIT4;
			// net_state = 99;
		}
	}
	if(net_state == NET_STATE_MODEM_INIT4){
		if(mtimer_timeout(&net_timer)){
			serial_gsm_read_flush();
			serial_gsm_write("ATD*99***1#\r", 12);
			ppp_init();
			net_buffer_loc = 0;
			printf("MODEM: DIALING\n");
			printf("NET: WAITING PPP\n");
			mtimer_timeout_set(&net_timer, 20000);
			net_state = NET_STATE_PPP_INIT;
		}
	}
	if(net_state == NET_STATE_PPP_INIT){
		if(serial_gsm_read(&c, 1) > 0){
			if((c == '\r') || (c == '\n')){
				net_buffer[net_buffer_loc] = 0;
				if(!memcmp((char *)net_buffer, "NO CARRIER", 10)){
					printf("MODEM: NO CARRIER\n");
					if(net_try++ > 3){
						printf("MODEM: GIVING UP\n");
						net_state = NET_STATE_MODEM_PWR;
					}else{
						mtimer_timeout_set(&net_timer, 5000);
						net_state = NET_STATE_MODEM_INIT;
					}
				}
				net_buffer_loc = 0;
			}else{
				if(net_buffer_loc < 15)net_buffer[net_buffer_loc++] = c;
			}
			ppp_read_byte(c);
		}
		if(mtimer_timeout(&net_timer)){
			printf("NET: PPP TIMEOUT\n");
			net_state = NET_STATE_MODEM_PWR;
		}
		ppp_poll();
		if(ppp_carrier()){
			if((ppp_ip_local[0] == 0) && (ppp_ip_local[1] == 0) && (ppp_ip_local[2] == 0) && (ppp_ip_local[3] == 0)){
				printf("NET: BAD IP!!!\n");
				if(net_try++ > 3){
					printf("NET: GIVING UP\n");
					net_state = NET_STATE_MODEM_PWR;
				}else{
					net_state = NET_STATE_MODEM_INIT;
				}
			}else{
				uip_ipaddr(ipaddr, ppp_ip_local[0], ppp_ip_local[1], ppp_ip_local[2], ppp_ip_local[3]);
				uip_sethostaddr(ipaddr);
				uip_ipaddr(ipaddr, 255, 255, 255, 255);
				uip_setnetmask(ipaddr);
				if((ppp_ip_remote[0] == 0) && (ppp_ip_remote[1] == 0) && (ppp_ip_remote[2] == 0) && (ppp_ip_remote[3] == 0)){
					ppp_ip_remote[0] = 10;
					ppp_ip_remote[1] = 10;
					ppp_ip_remote[2] = 10;
					ppp_ip_remote[3] = 64;
				}
				uip_ipaddr(ipaddr, ppp_ip_remote[0], ppp_ip_remote[1], ppp_ip_remote[2], ppp_ip_remote[3]);
				uip_setdraddr(ipaddr);
				mtimer_timeout_set(&net_periodic_timer, 0);
				mtimer_timeout_set(&net_timer, 60000);
				socket_reset();
				printf("NET: PPP IS UP!!!\n");
				net_state = NET_STATE_PPP_POLL;
				net_try = 0;
			}
		}
	}
	if(net_state == NET_STATE_PPP_POLL){
		if(serial_gsm_read(&c, 1)){
			ppp_len = 0;
			ppp_read_byte(c);
			if(ppp_len){
				memcpy(&uip_buf[UIP_LLH_LEN], ppp_buf, ppp_len);
				uip_len = ppp_len + UIP_LLH_LEN;
				uip_input();
				if(uip_len)ppp_send(&uip_buf[UIP_LLH_LEN], uip_len);
			}
		}
		if(mtimer_timeout(&net_periodic_timer)){
			for(c = 0; c < UIP_CONNS; c++){
				uip_periodic(c);
				if(uip_len > 0)ppp_send(&uip_buf[UIP_LLH_LEN], uip_len);
			}
			mtimer_timeout_set(&net_periodic_timer, 500);
		}
		ppp_poll();
		if(socket_state == SOCKET_STATE_CONNECT){
			uip_connect_conn(net_conn, modem_address(), HTONS(modem_port()));
			socket_state = SOCKET_STATE_CONNECT_WAIT;
		}
		if((socket_state == SOCKET_STATE_CONNECT_WAIT) || (socket_state == SOCKET_STATE_CONNECTED)){
			if(mtimer_timeout(&socket_timer))socket_reset();
		}
		if(mtimer_timeout(&net_timer)){
			net_state = NET_STATE_MODEM_INIT;
		}
	}
}

void socket_reset(){
	socket_state = SOCKET_STATE_CONNECT;
	mtimer_timeout_set(&socket_timer, 30000);
	socket_recv_fifo_in = socket_recv_fifo_out = socket_recv_fifo_len = 0;
	socket_send_fifo_in = socket_send_fifo_out = socket_send_fifo_len = 0;
	socket_last_sent = 0;
	led_set(1, LED_STATE_BLINK, 500, 500);
}

int16_t socket_read(void *buf, uint16_t count){
	int16_t len;
	for(len = 0; socket_recv_fifo_len && (len < count); len++){
		((uint8_t *)buf)[len] = socket_recv_fifo[socket_recv_fifo_out];
		socket_recv_fifo_len--;
		socket_recv_fifo_out = (socket_recv_fifo_out + 1) % SOCKET_RECV_FIFO_LEN;
	}
	if((len == 0) && (socket_state != SOCKET_STATE_CONNECTED))len = -1;
	return(len);
}

int16_t socket_write(const void *buf, uint16_t count){
	int16_t len;
	if(socket_state == SOCKET_STATE_CONNECTED){
		for(len = 0; (socket_send_fifo_len < SOCKET_SEND_FIFO_LEN) && (len < count); len++){
			socket_send_fifo[socket_send_fifo_in] = ((uint8_t *)buf)[len];
			socket_send_fifo_len++;
			socket_send_fifo_in = (socket_send_fifo_in + 1) % SOCKET_SEND_FIFO_LEN;
		}
	}else{
		len = -1;
	}
	return(len);
}

int16_t socket_write_space(void){
	int16_t len;
	if(socket_state == SOCKET_STATE_CONNECTED){
		len = (SOCKET_SEND_FIFO_LEN - socket_send_fifo_len);
	}else{
		len = -1;
	}
	return(len);
}

void net_appcall(){
	uint16_t i, j;
	if(net_conn != uip_conn)return;
//	printf("SOCKET: %u, %u, %u, %u, %u, %u, %u\n", (unsigned int)uip_newdata(), (unsigned int)uip_acked(), (unsigned int)uip_rexmit(), (unsigned int)uip_aborted(), (unsigned int)uip_closed(), (unsigned int)uip_timedout(), (unsigned int)uip_connected());
	if(uip_aborted() || uip_closed() || uip_timedout()){
		socket_reset();
		printf("NET: SOCKET CLOSED\n");
	}else if((uip_newdata() && !uip_connected()) || uip_acked() || uip_rexmit()){
		mtimer_timeout_set(&socket_timer, 30000);
		mtimer_timeout_set(&net_timer, 60000);
	}else if(uip_connected()){
		socket_reset();
		socket_state = SOCKET_STATE_CONNECTED;
		printf("NET: SOCKET CONNECTED\n");
		net_connected();
	}
	if(uip_newdata() && !uip_connected())led_set(1, LED_STATE_ON, 0, 0);
	if(socket_state == SOCKET_STATE_CONNECTED){
		if(uip_newdata()){
			led_set(1, LED_STATE_OFF_TEMP, 100, 0);
			for(i = 0; (i < uip_datalen()) && (socket_recv_fifo_len < SOCKET_RECV_FIFO_LEN); i++){
				socket_recv_fifo[socket_recv_fifo_in] = ((uint8_t *)uip_appdata)[i];
				socket_recv_fifo_in = (socket_recv_fifo_in + 1) % SOCKET_RECV_FIFO_LEN;
				socket_recv_fifo_len++;
			}
			if(socket_recv_fifo_len > ((SOCKET_RECV_FIFO_LEN * 2) / 3)){
				uip_stop();
			}
		}else{
			if(uip_stopped(uip_conn)){
				if(socket_recv_fifo_len < ((SOCKET_RECV_FIFO_LEN * 1) / 3))uip_restart();
			}
		}
		if(uip_acked()){
			for( ; socket_last_sent && socket_send_fifo_len; socket_last_sent--){
				socket_send_fifo_out = (socket_send_fifo_out + 1) % SOCKET_SEND_FIFO_LEN;
				socket_send_fifo_len--;
			}
		}
		if(uip_rexmit()){
			i = socket_send_fifo_out;
			for(j = 0; (j < socket_last_sent) && (j < uip_mss()) && (j < socket_send_fifo_len); j++){
				((uint8_t *)uip_appdata)[j] = socket_send_fifo[i];
				i = (i + 1) % SOCKET_SEND_FIFO_LEN;
			}
			uip_send(uip_appdata, j);
		}else if(!socket_last_sent && (uip_newdata() || uip_acked() || uip_poll())){
			i = socket_send_fifo_out;
			for(socket_last_sent = 0; (socket_last_sent < uip_mss()) && (socket_last_sent < socket_send_fifo_len); socket_last_sent++){
				((uint8_t *)uip_appdata)[socket_last_sent] = socket_send_fifo[i];
				i = (i + 1) % SOCKET_SEND_FIFO_LEN;
			}
			if(socket_last_sent){
				uip_send(uip_appdata, socket_last_sent);
			}
		}
	}
}

void net_udp_appcall(){
}

void ppp_arch_putchar(uint8_t c){
	while(!serial_gsm_write(&c, 1)); // mingi timeout panna
}

