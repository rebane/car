#ifndef _CAN_H_
#define _CAN_H_

#include <stdint.h>

#define CAN_EFF_FLAG 0x80000000UL
#define CAN_RTR_FLAG 0x40000000UL
#define CAN_ERR_FLAG 0x20000000UL

#define CAN_SFF_MASK 0x000007FFUL
#define CAN_EFF_MASK 0x1FFFFFFFUL
#define CAN_ERR_MASK 0x1FFFFFFFUL

typedef uint32_t canid_t;

struct can_frame{
	canid_t can_id;
	uint8_t can_dlc;
	uint8_t data[8];
};

void can_init(void);
int8_t can_read(struct can_frame *frame);

#endif

