#include "iso9141.h"
#include <stdio.h>
#include "kline.h"
#include "mtimer.h"
#include "bjson.h"
#include "led.h"

// https://code.google.com/p/opengauge/source/browse/trunk/obduino/obduino.pde?r=18
// http://blog.perquin.com/prj/obdii/
// http://www.outilsobdfacile.com/vehicle-list-compatible-obd2.php?brand=peugeot

#define ISO9141_VIN_PID          0xFE
#define ISO9141_UNUSED_PID       0xFF
#define ISO9141_DATA_LEN         4

#define ISO9141_BUFFER_LEN       32

#define ISO9141_STATE_INIT       0
#define ISO9141_STATE_INIT_WAIT1 1
#define ISO9141_STATE_INIT_WAIT2 2
#define ISO9141_STATE_INIT_WAIT3 3
#define ISO9141_STATE_INIT_WAIT4 4
#define ISO9141_STATE_QUERY      5
#define ISO9141_STATE_WAIT       6

static struct{
	uint8_t pid;
	uint8_t datalen;
	uint8_t data[ISO9141_DATA_LEN];
	mtimer_t timer;
}iso9141_status[] = {
//	{ ISO9141_VIN_PID, 17 },
//	{ 0x00, 4 }, // PIDs supported [01 - 20]
//	{ 0x01, 4 }, // Monitor status since DTCs cleared
//	{ 0x03, 2 }, // Fuel system status
	{ 0x04, 1 }, // Calculated engine load value
	{ 0x05, 1 }, // Engine coolant temperature
//	{ 0x06, 1 }, // Short term fuel % trim - Bank 1
//	{ 0x07, 1 }, // Long term fuel % trim - Bank 1

//	{ 0x0B, 1 }, // Intake manifold absolute pressure
	{ 0x0C, 2 }, // Engine RPM
	{ 0x0D, 1 }, // Vehicle speed
//	{ 0x0E, 1 }, // Timing advance
	{ 0x0F, 1 }, // Intake air temperature

	{ 0x11, 1 }, // Throttle position
//	{ 0x13, 1 }, // Oxygen sensors present
//	{ 0x14, 2 }, // Bank 1, Sensor 1: Oxygen sensor voltage, Short term fuel trim
//	{ 0x15, 2 }, // Bank 1, Sensor 2: Oxygen sensor voltage, Short term fuel trim

//	{ 0x1C, 1 }, // OBD standards this vehicle conforms to

//	{ 0x20, 4 }, // PIDs supported [0x21 - 0x40]
//	{ 0x21, 1 }, // Distance traveled with malfunction indicator lamp (MIL) on
	{ ISO9141_UNUSED_PID, 0 }
};

static uint8_t iso9141_state, iso9141_key1, iso9141_key2, iso9141_pid_loc, iso9141_try, iso9141_buffer_loc;
static uint8_t iso9141_buffer[ISO9141_BUFFER_LEN], iso9141_vin[21], iso9141_vin_ok;
static mtimer_t iso9141_timer;

void iso9141_init(){
	uint8_t i;
	iso9141_state = ISO9141_STATE_INIT;
	mtimer_timeout_set(&iso9141_timer, 0);
	for(i = 0; iso9141_status[i].datalen; i++){
		mtimer_timeout_set(&iso9141_status[i].timer, 0);
	}
	iso9141_vin_ok = 0;
}

void iso9141_poll(){
	uint8_t c, sum;
	if(iso9141_state == ISO9141_STATE_INIT){
		led_set(0, LED_STATE_OFF, 0, 0);
		if(mtimer_timeout(&iso9141_timer)){
			iso9141_vin_ok = 0;
			kline_send_slow_init(0x33);
			iso9141_state = ISO9141_STATE_INIT_WAIT1;
			mtimer_timeout_set(&iso9141_timer, 7000);
		}
	}
	if(iso9141_state == ISO9141_STATE_INIT_WAIT1){
		if((kline_read(&c, 1) == 1)){
			if((c == 0x55)){
				iso9141_state = ISO9141_STATE_INIT_WAIT2;
				mtimer_timeout_set(&iso9141_timer, 100);
			}
		}else if(mtimer_timeout(&iso9141_timer)){
			iso9141_state = ISO9141_STATE_INIT;
		}
	}
	if(iso9141_state == ISO9141_STATE_INIT_WAIT2){
		if((kline_read(&c, 1) == 1)){
			if(((c == 0x08) || (c == 0x94) || (c == 0x8F))){
				iso9141_key1 = c;
				iso9141_state = ISO9141_STATE_INIT_WAIT3;
				mtimer_timeout_set(&iso9141_timer, 100);
			}
		}else if(mtimer_timeout(&iso9141_timer)){
			iso9141_state = ISO9141_STATE_INIT;
		}
	}
	if(iso9141_state == ISO9141_STATE_INIT_WAIT3){
		if((kline_read(&c, 1) == 1)){
			if(((c == 0x08) || (c == 0x94))){
				iso9141_key2 = c;
				c ^= 0xFF;
				kline_send(25, 0, &c, 1);
				mtimer_timeout_set(&iso9141_timer, 150);
				iso9141_state = ISO9141_STATE_INIT_WAIT4;
			}
		}else if(mtimer_timeout(&iso9141_timer)){
			iso9141_state = ISO9141_STATE_INIT;
		}
	}
	if(iso9141_state == ISO9141_STATE_INIT_WAIT4){
		if((kline_read(&c, 1) == 1)){
			if((c == 0xCC)){
				iso9141_pid_loc = 0;
				iso9141_try = 0;
				iso9141_state = ISO9141_STATE_QUERY;
			}
		}else if(mtimer_timeout(&iso9141_timer)){
			iso9141_state = ISO9141_STATE_INIT;
		}
	}
	if(iso9141_state == ISO9141_STATE_QUERY){
		if((iso9141_status[iso9141_pid_loc].pid == ISO9141_VIN_PID) && iso9141_vin_ok)iso9141_pid_loc++;
		if(iso9141_status[iso9141_pid_loc].datalen == 0)iso9141_pid_loc = 0;
		if(((iso9141_key1 == 0x08) && (iso9141_key2 == 0x08)) || ((iso9141_key1 == 0x94) && (iso9141_key2 == 0x94))){
			iso9141_buffer[0] = 0x68;
			iso9141_buffer[1] = 0x6A;
		}else{
			iso9141_buffer[0] = 0xC2;
			iso9141_buffer[1] = 0x33;
		}
		iso9141_buffer[2] = 0xF1;
		if(iso9141_status[iso9141_pid_loc].pid == ISO9141_VIN_PID){
			iso9141_buffer[3] = 0x09;
			iso9141_buffer[4] = 0x02;
		}else{
			iso9141_buffer[3] = 0x01;
			iso9141_buffer[4] = iso9141_status[iso9141_pid_loc].pid;
		}
		iso9141_buffer[5] = 0;
		for(c = 0; c < 5; c++)iso9141_buffer[5] += iso9141_buffer[c];
		kline_send(55, 5, iso9141_buffer, 6);
		iso9141_buffer_loc = 0;
		iso9141_state = ISO9141_STATE_WAIT;
		mtimer_timeout_set(&iso9141_timer, 1000);
	}
	if(iso9141_state == ISO9141_STATE_WAIT){
		if((kline_read(&c, 1) == 1)){
			if(iso9141_buffer_loc < ISO9141_BUFFER_LEN){
				iso9141_buffer[iso9141_buffer_loc++] = c;
				if(iso9141_buffer_loc == (iso9141_status[iso9141_pid_loc].datalen + 6)){
					sum = iso9141_buffer[0];
					for(c = 1; c < (iso9141_buffer_loc - 1); c++){
						sum += iso9141_buffer[c];
					}
					if(sum == iso9141_buffer[iso9141_buffer_loc - 1]){
						if(iso9141_status[iso9141_pid_loc].pid == ISO9141_VIN_PID){
							if((iso9141_buffer[0] == 0x48) && (iso9141_buffer[1] == 0x6B) && (iso9141_buffer[3] == 0x49) && (iso9141_buffer[4] == 0x02)){
								for(c = 0; c < (iso9141_status[iso9141_pid_loc].datalen) && (c < ISO9141_DATA_LEN); c++){
									iso9141_vin[c] = iso9141_buffer[5 + c];
								}
								iso9141_vin[17] = 0;
								iso9141_vin_ok = 1;
								mtimer_timeout_set(&iso9141_status[iso9141_pid_loc].timer, 15000);
								led_set(0, LED_STATE_ON, 0, 0);
								// printf("ISO9141 VIN OK: %s\n", (char *)iso9141_vin);
								iso9141_try = 0;
							}
						}else{
							if((iso9141_buffer[0] == 0x48) && (iso9141_buffer[1] == 0x6B) && (iso9141_buffer[3] == 0x41) && (iso9141_buffer[4] == iso9141_status[iso9141_pid_loc].pid)){
								for(c = 0; c < (iso9141_status[iso9141_pid_loc].datalen) && (c < ISO9141_DATA_LEN); c++){
									iso9141_status[iso9141_pid_loc].data[c] = iso9141_buffer[5 + c];
								}
								mtimer_timeout_set(&iso9141_status[iso9141_pid_loc].timer, 15000);
								led_set(0, LED_STATE_ON, 0, 0);
								// printf("ISO9141 PID OK: %02X\n", (unsigned int)iso9141_status[iso9141_pid_loc].pid);
								iso9141_try = 0;
							}
						}
					}
					iso9141_pid_loc++;
					iso9141_state = ISO9141_STATE_QUERY;
				}
			}
		}
		if(mtimer_timeout(&iso9141_timer)){
			printf("ISO9141 PID ERROR: %02X,", (unsigned int)iso9141_status[iso9141_pid_loc].pid);
			for(c = 0; c < iso9141_buffer_loc; c++){
				printf(" %02X", (unsigned int)c);
			}
			printf("\n");
			iso9141_try++;
			if(iso9141_try > 7){
				mtimer_timeout_set(&iso9141_timer, 5000);
				iso9141_state = ISO9141_STATE_INIT;
			}else{
				iso9141_pid_loc++;
				iso9141_state = ISO9141_STATE_QUERY;
			}
		}
	}
}

uint16_t iso9141_read(uint8_t *data){
	char name[16];
	uint8_t i;
	uint16_t j;
	j = 0;
	for(i = 0; iso9141_status[i].datalen; i++){
		if(iso9141_status[i].pid == ISO9141_VIN_PID){
			if(iso9141_vin_ok){
				j += bjson_string(&data[j], "iso9141_vin");
				j += bjson_binary(&data[j], iso9141_vin, 17);
			}
		}else if(!mtimer_timeout(&iso9141_status[i].timer)){
			snprintf(name, 15, "iso9141_pid%u", (unsigned int)iso9141_status[i].pid);
			name[15] = 0;
			j += bjson_string(&data[j], name);
			j += bjson_binary(&data[j], iso9141_status[i].data, iso9141_status[i].datalen);
		}
	}
	return(j);
}

