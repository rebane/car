#include "seats.h"
#include <stdio.h>
#include "serial_485.h"
#include "mtimer.h"
#include "bjson.h"

#define SEATS_MAX            128
#define SEATS_TIMEOUT_ONLINE 60000
#define SEATS_TIMEOUT_DOUBLE 60000

static uint8_t seats_buffer[8], seats_packet[8];
static uint8_t seats_loc;
static mtimer_t seats_timer;
static struct{
	mtimer_t timer_online;
	mtimer_t timer_double;
	uint8_t random;
	uint8_t uptime;
	uint8_t status;
	uint16_t value;
}seats[SEATS_MAX];
static uint8_t seats_recorded_last;

static uint8_t seats_crc(uint8_t crc, uint8_t data);

void seats_init(){
	uint8_t i;
	seats_loc = 0;
	mtimer_timeout_set(&seats_timer, 0);
	for(i = 0; i < SEATS_MAX; i++){
		mtimer_timeout_set(&seats[i].timer_online, 0);
		mtimer_timeout_set(&seats[i].timer_double, 0);
		seats[i].random = 0x80;
		seats[i].uptime = 0;
		seats[i].status = 0;
		seats[i].value = 0;
	}
	seats_recorded_last = 0;
}

void seats_poll(void){
	uint16_t i, l, j;
	l = serial_485_read(seats_buffer, 8);
	for(i = 0; i < l; i++){
		mtimer_timeout_set(&seats_timer, 500);
		seats_packet[seats_loc] = seats_buffer[i];
		if(seats_packet[seats_loc] & 0x80){
			seats_packet[0] = seats_packet[seats_loc];
			seats_loc = 0;
		}
		if(seats_loc == 0){
			if((seats_packet[0] & 0x80) && (seats_packet[0] & 0x7F))seats_loc = 1;
		}else{
			if(seats_loc == 6){
				seats_packet[7] = 0;
				for(j = 0; j < 6; j++)seats_packet[7] = seats_crc(seats_packet[7], seats_packet[j]);
				if(seats_packet[6] == seats_packet[7]){
					seats_packet[0] = (seats_packet[0] & 0x7F) - 1;
					if(seats_packet[0] > seats_recorded_last)seats_recorded_last = seats_packet[0];
					if((seats[seats_packet[0]].random != 0x80) && (seats[seats_packet[0]].random != seats_packet[1]))mtimer_timeout_set(&seats[seats_packet[0]].timer_double, SEATS_TIMEOUT_DOUBLE);
					seats[seats_packet[0]].random = seats_packet[1];
					seats[seats_packet[0]].uptime = seats_packet[2];
					seats[seats_packet[0]].status = seats_packet[3];
					seats[seats_packet[0]].value = (((uint16_t)seats_packet[4] << 7) | seats_packet[5]);
					if(!seats[seats_packet[0]].value)seats[seats_packet[0]].value = 1;
					mtimer_timeout_set(&seats[seats_packet[0]].timer_online, SEATS_TIMEOUT_ONLINE);
					printf("SEAT: %u, RANDOM: %u, UPTIME: %u, STATUS: 0x%02X, VALUE: %u, CRC: 0x%02X\n", (unsigned int)(seats_packet[0] & 0x7F), (unsigned int)seats_packet[1], (unsigned int)seats_packet[2], (unsigned int)seats_packet[3], (unsigned int)(((uint16_t)seats_packet[4] << 7) | seats_packet[5]), (unsigned int)seats_packet[6]);
				}else{
					printf("CRC ERROR\n");
				}
				seats_loc = 0;
			}else{
				if(seats_loc < 7)seats_loc++;
			}
		}
	}
	if(mtimer_timeout(&seats_timer))seats_loc = 0;
}

uint16_t seats_read(uint8_t *buffer){
	uint16_t i, l;
	uint8_t seats_last;
	seats_last = 0;
	for(i = 0; i < SEATS_MAX; i++){
		if(mtimer_timeout(&seats[i].timer_online)){
			seats[i].random = 0x80;
			seats[i].uptime = 0;
			seats[i].status = 0;
			seats[i].value = 0;
		}else{
			seats_last = i + 1;
		}
	}
	if(!seats_last)return(0);
	l = 0;
	l += bjson_string(&buffer[l], "seats");
	for(i = 0; i < seats_last; i++){
		if(seats[i].value){
			buffer[l + 3 + (i * 2) + 0] = (seats[i].value >> 8) & 0x3F;
			if(!mtimer_timeout(&seats[i].timer_double))buffer[l + 3 + (i * 2) + 0] |= 0x80;
			if(seats[i].uptime < 127)buffer[l + 3 + (i * 2) + 0] |= 0x40;
			buffer[l + 3 + (i * 2) + 1] = (seats[i].value >> 0) & 0xFF;
		}else{
			buffer[l + 3 + (i * 2) + 0] = buffer[l + 3 + (i * 2) + 1] = 0;
		}
	}
	bjson_value16(&buffer[l], (i * 2), BJSON_BASE_BINARY);
	return(l + 3 + (i * 2));
}

static uint8_t seats_crc(uint8_t crc, uint8_t data){
	uint8_t i;
	crc ^= (data & 0x7F);
	for(i = 0; i < 7; i++){
		if(crc & 0x01){
			crc = (crc >> 1) ^ 0x4C;
		}else{
			crc >>= 1;
		}
	}
	return(crc);
}

