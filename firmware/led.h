#ifndef _LED_H_
#define _LED_H_

#include <stdint.h>

#define LED_COUNT           2

#define LED_STATE_OFF       0
#define LED_STATE_ON        1
#define LED_STATE_OFF_TEMP  2
#define LED_STATE_ON_TEMP   3
#define LED_STATE_BLINK     4
#define LED_STATE_BLINK_ON  5
#define LED_STATE_BLINK_OFF 6

void led_init();
void led_poll();
void led_set(uint8_t led, uint8_t state, uint16_t timeout1, uint16_t timeout2);

#endif

