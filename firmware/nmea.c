#include "nmea.h"
#include <stdio.h>
#include "platform.h"
#include "mtimer.h"
#include "serial_gps.h"
#include "bjson.h"
#include "clock.h"
#include "led.h"

#define tobin(c) ((((c) >= '0') && ((c) <= '9')) ? ((c) - '0') : ((((c) >= 'A') && ((c) <= 'F')) ? ((c) - 'A' + 10) : ((((c) >= 'a') && ((c) <= 'f')) ? ((c) - 'a' + 10) : (0))))

static uint8_t nmea_buffer[256];
static uint16_t nmea_loc;
static mtimer_t nmea_timer, nmea_invert_timer, nmea_led_timer;
static uint8_t nmea_lat_sign, nmea_lon_sign;
static uint64_t nmea_lat, nmea_lon, nmea_speed, nmea_date;

static void nmea_parse();
static uint8_t nmea_parse_number(char *str, uint8_t len, uint64_t *value);
static uint64_t nmea_power(uint64_t value, uint8_t from, uint8_t to);

void nmea_init(){
	mtimer_timeout_set(&nmea_timer, 0);
	mtimer_timeout_set(&nmea_invert_timer, 5000);
	mtimer_timeout_set(&nmea_led_timer, 0);
	nmea_loc = 0;
}

void nmea_poll(){
	uint16_t i, l;
	uint8_t buffer[32];
	l = serial_gps_read(buffer, 32);
	for(i = 0; i < l; i++){
		if((buffer[i] == '\r') || (buffer[i] == '\n')){
			if(nmea_loc){
//				write_stdout(nmea_buffer, nmea_loc);
//				write_stdout("\n", 1);
				nmea_buffer[nmea_loc] = 0;
				nmea_parse();
				nmea_loc = 0;
			}
		}else{
			if(nmea_loc < 255)nmea_buffer[nmea_loc++] = buffer[i];
		}
	}
	if(mtimer_timeout(&nmea_invert_timer)){
		serial_gps_rx_invert();
		mtimer_timeout_set(&nmea_invert_timer, 5000);
	}
	if(mtimer_timeout(&nmea_led_timer)){
		//led_set(0, LED_STATE_OFF, 0, 0);
	}else{
		if(mtimer_timeout(&nmea_timer)){
			//led_set(0, LED_STATE_BLINK, 500, 500);
		}else{
			//led_set(0, LED_STATE_ON, 0, 0);
		}
	}
}

static void nmea_parse(){
	uint8_t i, chk_calc, chk_orig;
	uint8_t field_start[32], field_size[32], field_count;
	struct tm t;
	if(nmea_loc < 7)return;
	if(nmea_buffer[0] != '$')return;
	// if(memcmp(&nmea_buffer[1], "GP", 2))return; // only for GPS
	if(nmea_buffer[6] != ',')return;
	if(nmea_buffer[nmea_loc - 3] == '*'){
		if(nmea_loc < 10)return;
		chk_orig = (tobin(nmea_buffer[nmea_loc - 2]) << 4) | tobin(nmea_buffer[nmea_loc - 1]);
		chk_calc = 0;
		nmea_loc -= 3;
		for(i = 1; i < nmea_loc; i++)chk_calc ^= nmea_buffer[i];
		if(chk_calc != chk_orig)return;
	}
	mtimer_timeout_set(&nmea_invert_timer, 5000);
	mtimer_timeout_set(&nmea_led_timer, 5000);
	for(field_count = 0, i = 6; i < nmea_loc; i++){
		if(nmea_buffer[i] == ','){
			if(field_count > 30)break;
			field_start[field_count] = i + 1;
			field_size[field_count] = 0;
			field_count++;
		}else{
			if(field_count)field_size[field_count - 1]++;
		}
	}
	/**/
	if(!memcmp(&nmea_buffer[3], "RMC", 3)){ // recommended minimum
		// 1 Knot = 1.852 Kilometers per Hour
		if(field_count < 9)return;
		if(!field_size[1] || ((nmea_buffer[field_start[1]] != 'A') && (nmea_buffer[field_start[1]] != 'a')))return;

		i = nmea_parse_number((char *)&nmea_buffer[field_start[2]], field_size[2], &nmea_lat);
		nmea_lat = nmea_power(nmea_lat, i, 4);
		if((nmea_buffer[field_start[3]] == 'S') || (nmea_buffer[field_start[3]] == 's'))nmea_lat_sign = 1; else nmea_lat_sign = 0;
		i = nmea_parse_number((char *)&nmea_buffer[field_start[4]], field_size[4], &nmea_lon);
		nmea_lon = nmea_power(nmea_lon, i, 4);
		if((nmea_buffer[field_start[5]] == 'W') || (nmea_buffer[field_start[5]] == 'w'))nmea_lon_sign = 1; else nmea_lon_sign = 0;
		i = nmea_parse_number((char *)&nmea_buffer[field_start[6]], field_size[6], &nmea_speed);
		nmea_speed = nmea_power(nmea_speed, i, 2);
		if((field_size[0] > 5) && (field_size[8] > 5)){
			t.tm_hour = (((uint16_t)nmea_buffer[field_start[0] + 0] - '0') * 10) + (nmea_buffer[field_start[0] + 1] - '0');
			t.tm_min  = (((uint16_t)nmea_buffer[field_start[0] + 2] - '0') * 10) + (nmea_buffer[field_start[0] + 3] - '0');
			t.tm_sec  = (((uint16_t)nmea_buffer[field_start[0] + 4] - '0') * 10) + (nmea_buffer[field_start[0] + 5] - '0');
			t.tm_mday = (((uint16_t)nmea_buffer[field_start[8] + 0] - '0') * 10) + (nmea_buffer[field_start[8] + 1] - '0');
			t.tm_mon  = (((uint16_t)nmea_buffer[field_start[8] + 2] - '0') * 10) + (nmea_buffer[field_start[8] + 3] - '0');
			t.tm_year = (((uint16_t)nmea_buffer[field_start[8] + 4] - '0') * 10) + (nmea_buffer[field_start[8] + 5] - '0');
			if(t.tm_mon)t.tm_mon--;
			t.tm_year += 100;
			nmea_date = mktime(&t);
		}
		mtimer_timeout_set(&nmea_timer, 15000);
		clock_set(nmea_date, CLOCK_SOURCE_GPS);
//		printf("NMEA LAT: %s%llu, LON: %s%llu, SPEED: %llu, DATE: %llu\n", nmea_lat_sign ? "-" : "", (unsigned long long int)nmea_lat, nmea_lon_sign ? "-" : "", (unsigned long long int)nmea_lon, (unsigned long long int)nmea_speed, (unsigned long long int)nmea_date);
	}
}

uint16_t nmea_read(uint8_t *buffer){
	uint16_t l;
	l = 0;
	if(!mtimer_timeout(&nmea_timer)){
		l += bjson_string(&buffer[l], "gpsLatitude");
		if(nmea_lat_sign){
			l += bjson_negative(&buffer[l], nmea_lat);
		}else{
			l += bjson_positive(&buffer[l], nmea_lat);
		}
		l += bjson_string(&buffer[l], "gpsLongitude");
		if(nmea_lon_sign){
			l += bjson_negative(&buffer[l], nmea_lon);
		}else{
			l += bjson_positive(&buffer[l], nmea_lon);
		}
		l += bjson_string(&buffer[l], "gpsSpeed");
		l += bjson_positive(&buffer[l], nmea_speed);
	}
	return(l);
}

static uint8_t nmea_parse_number(char *str, uint8_t len, uint64_t *value){
	uint8_t pow, s, i;
	*value = 0;
	pow = s = 0;
	for(i = 0; i < len; i++){
		if(isdigit(str[i])){
			*value = (*value * 10) + (str[i] - '0');
			if(s)pow++;
		}else if(str[i] == '.'){
			s = 1;
		}
	}
	return(pow);
}

static uint64_t nmea_power(uint64_t value, uint8_t from, uint8_t to){
	uint8_t i;
	if(from < to){
		for(i = 0; i < (to - from); i++){
			value *= 10;
		}
	}else{
		for(i = 0; i < (from - to); i++){
			value /= 10;
		}
	}
	return(value);
}

