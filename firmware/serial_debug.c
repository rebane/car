#include "serial_debug.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"

// TX = RB11, RP43
// RX = RA8,  RPI24

#define SERIAL_DEBUG_SEND_FIFO_LEN    8
#define SERIAL_DEBUG_RECV_FIFO_LEN    16

#define serial_debug_critical_enter() do{ INTCON2bits.GIE = 0; }while(0)
#define serial_debug_critical_exit()  do{ INTCON2bits.GIE = 1; }while(0)

#if SERIAL_DEBUG_SEND_FIFO_LEN < 256
static volatile uint8_t serial_debug_send_fifo_in, serial_debug_send_fifo_out, serial_debug_send_fifo_len;
#elif SERIAL_DEBUG_SEND_FIFO_LEN < 65536
static volatile uint16_t serial_debug_send_fifo_in, serial_debug_send_fifo_out, serial_debug_send_fifo_len;
#else
#error SERIAL_DEBUG_SEND_FIFO_LEN too big
#endif

#if SERIAL_DEBUG_RECV_FIFO_LEN < 256
static volatile uint8_t serial_debug_recv_fifo_in, serial_debug_recv_fifo_out, serial_debug_recv_fifo_len;
#elif SERIAL_DEBUG_RECV_FIFO_LEN < 65536
static volatile uint16_t serial_debug_recv_fifo_in, serial_debug_recv_fifo_out, serial_debug_recv_fifo_len;
#else
#error SERIAL_DEBUG_RECV_FIFO_LEN too big
#endif

static volatile uint8_t serial_debug_send_fifo[SERIAL_DEBUG_SEND_FIFO_LEN], serial_debug_recv_fifo[SERIAL_DEBUG_RECV_FIFO_LEN];

void serial_debug_init(){
	IEC5bits.U4TXIE = 0;
	IEC5bits.U4RXIE = 0;
	IEC5bits.U4EIE = 0;

	serial_debug_send_fifo_in = serial_debug_send_fifo_out = serial_debug_send_fifo_len = 0;
	serial_debug_recv_fifo_in = serial_debug_recv_fifo_out = serial_debug_recv_fifo_len = 0;

	U4MODE = 0;

	// TX
	RPOR4bits.RP43R = 29;

	// RX
//	RPINR28bits.U4RXR = 24;

	U4STA = 0;
	U4BRG = (PLATFORM_FCPU / (4LLU * 9600LLU)) - 1LLU;
	U4MODEbits.BRGH = 1;
	U4STAbits.UTXISEL1 = 0;
	U4STAbits.UTXISEL0 = 0;
	U4MODEbits.UARTEN = 1;
	U4STAbits.UTXEN = 1; // Note: The UTXEN bit is set after the UARTEN bit has been set; otherwise, UART transmissions will not be enabled.

	IPC22bits.U4TXIP = 1;
	IPC22bits.U4RXIP = 1;

	IFS5bits.U4TXIF = 0;
	IFS5bits.U4RXIF = 0;
	IFS5bits.U4EIF = 0;

	IEC5bits.U4RXIE = 1;
	IEC5bits.U4EIE = 1;
}

int16_t serial_debug_read(void *buf, uint16_t count){
	uint16_t len;
	if(!serial_debug_recv_fifo_len)return(0);
	serial_debug_critical_enter();
	IEC5bits.U4RXIE = 0;
	for(len = 0; serial_debug_recv_fifo_len && (len < count) && (len < 32767); len++){
		((uint8_t *)buf)[len] = serial_debug_recv_fifo[serial_debug_recv_fifo_out];
		serial_debug_recv_fifo_len--;
		serial_debug_recv_fifo_out = (serial_debug_recv_fifo_out + 1) % SERIAL_DEBUG_RECV_FIFO_LEN;
	}
	IEC5bits.U4RXIE = 1;
	serial_debug_critical_exit();
	return(len);
}

int16_t serial_debug_write(const void *buf, uint16_t count){
	uint16_t len;
	serial_debug_critical_enter();
	IEC5bits.U4TXIE = 0;
	for(len = 0; (serial_debug_send_fifo_len < SERIAL_DEBUG_SEND_FIFO_LEN) && (len < count) && (len < 32767); len++){
		serial_debug_send_fifo[serial_debug_send_fifo_in] = ((uint8_t *)buf)[len];
		serial_debug_send_fifo_len++;
		serial_debug_send_fifo_in = (serial_debug_send_fifo_in + 1) % SERIAL_DEBUG_SEND_FIFO_LEN;
	}
	if(serial_debug_send_fifo_len){
		IFS5bits.U4TXIF = 1;
		IEC5bits.U4TXIE = 1;
	}
	serial_debug_critical_exit();
	return(len);
}

int16_t serial_debug_write_space(void){
	int16_t len;
	serial_debug_critical_enter();
	IEC5bits.U4TXIE = 0;
	len = (SERIAL_DEBUG_SEND_FIFO_LEN - serial_debug_send_fifo_len);
	if(serial_debug_send_fifo_len)IEC5bits.U4TXIE = 1;
	serial_debug_critical_exit();
	return(len);
}

int16_t serial_debug_read_flush(void){
	serial_debug_critical_enter();
	IEC5bits.U4RXIE = 0;
	serial_debug_recv_fifo_len = 0;
	serial_debug_recv_fifo_out = serial_debug_recv_fifo_in;
	IEC5bits.U4RXIE = 1;
	serial_debug_critical_exit();
	return(0);
}

void _U4ErrHandler(){
	U4STAbits.OERR = 0;
	U4STAbits.PERR = 0;
	U4STAbits.FERR = 0;
	IFS5bits.U4EIF = 0;
}

void _U4RXHandler(){
	uint16_t c;
	while(U4STAbits.URXDA){
		c = U4RXREG;
		if(serial_debug_recv_fifo_len < SERIAL_DEBUG_RECV_FIFO_LEN){
			serial_debug_recv_fifo[serial_debug_recv_fifo_in] = (c & 0xFF);
			serial_debug_recv_fifo_in = (serial_debug_recv_fifo_in + 1) % SERIAL_DEBUG_RECV_FIFO_LEN;
			serial_debug_recv_fifo_len++;
		}
	}
	IFS5bits.U4RXIF = 0;
}

void _U4TXHandler(){
	while(!U4STAbits.UTXBF && serial_debug_send_fifo_len){
		U4TXREG = serial_debug_send_fifo[serial_debug_send_fifo_out];
		serial_debug_send_fifo_out = (serial_debug_send_fifo_out + 1) % SERIAL_DEBUG_SEND_FIFO_LEN;
		serial_debug_send_fifo_len--;
	}
	if(!serial_debug_send_fifo_len)IEC5bits.U4TXIE = 0;
	IFS5bits.U4TXIF = 0;
}

