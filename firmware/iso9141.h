#ifndef _ISO9141_H_
#define _ISO9141_H_

#include <stdint.h>

void iso9141_init();
void iso9141_poll();
uint16_t iso9141_read(uint8_t *data);

#endif

