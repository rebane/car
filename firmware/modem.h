#ifndef _MODEM_H_
#define _MODEM_H_

#include <stdint.h>
#include "uip.h"

void modem_init();
void modem_getconfig_reset();
int8_t modem_getconfig();
char *modem_imei();
char *modem_imsi();
char *modem_apn();
uip_ipaddr_t *modem_address();
uint16_t modem_port();
char *modem_custom();
uint8_t modem_serial(uint8_t *buffer);

#endif

