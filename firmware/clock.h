#ifndef _CLOCK_H_
#define _CLOCK_H_

#include <stdint.h>

#define CLOCK_SOURCE_GPS 1
#define CLOCK_SOURCE_GSM 2
#define CLOCK_SOURCE_NET 3
#define CLOCK_SOURCE_CAN 4

void clock_init();
void clock_set(uint64_t date, uint8_t source);
uint16_t clock_read(uint8_t *buffer);

#endif

