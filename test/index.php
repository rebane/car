<html>
	<head>
		<title>PO ASUKOHT</title>
		<script> 
			!function(w, d, x){
				x in w||eval("w."+x+"=function(){function a(n){try{var x=new ActiveXObject(n);w."+x+"=function(){return new ActiveXObject(n)};return x}catch(e){return false}}return a('Msxml2.XMLHTTP.6.0')||a('Msxml2.XMLHTTP.3.0')||a('Msxml2.XMLHTTP')}")
				w.include = function(url,cb){
					var req = new XMLHttpRequest(), tag = d.getElementsByTagName("script")
					tag = tag[tag.length-1];
					req.open("GET", url, true);
					req.onreadystatechange = function(){
						req.readyState === 4 && tag.parentNode.insertBefore(d.createTextNode(req.responseText || "?"), tag) && cb && cb(req.responseText)
					}
					req.send();
				}
			}(window, document, "XMLHttpRequest")
		</script>
	</head>
	<body>
		<div id="map" style="float:left;width:70%;height:100%">
			<div id="mapdiv" style="height:85%"></div>
			<center>
				<br/>
				Keep Center:
				<input type="checkbox" onClick="javascript:redraw(0)" id="keepCenter" checked="true"/>
				<input type="submit" onClick="javascript:redraw(1)" value="Center"/>
				<input type="submit" onClick="javascript:set_type(1)" value="Open Street Map"/>
				<input type="submit" onClick="javascript:set_type(0)" value="Google Satellite"/>
				<input type="text" id="date" readonly size="30"/>
			</center>
			<script src="http://www.openlayers.org/api/OpenLayers.js"></script>
			<script src="http://maps.google.com/maps/api/js?v=3.6&amp;sensor=false"></script>
		</div>
		<div id="data" style="margin-left:70%;height:100%">
			<center>
				<select id="serial" onchange="refresh()">
					<option value="862118020911019">PO</option>
					<option value="862118020911666">TEST</option>
				</select><br/><br/>
				<b>LAST KNOWN DATA:</b><br/><br/>
				<table border="0" cellpadding="7" cellspacing="0">
					<tr>
						<td nowrap>&nbsp;&nbsp;Vehicle Reg. No</td>
						<td nowrap id="custom"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;Controller version</td>
						<td nowrap id="firmwareVersion"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;Controller uptime</td>
						<td nowrap id="uptime"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;Controller IP</td>
						<td nowrap id="ip"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;IMEI</td>
						<td nowrap id="imei"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;SIM</td>
						<td nowrap id="imsi"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;Voltage</td>
						<td nowrap id="voltage"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;Vehicle speed GPS</td>
						<td nowrap id="gpsSpeed"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;Vehicle speed OBD</td>
						<td nowrap id="obdSpeed"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;Engine load</td>
						<td nowrap id="obdLoad"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;Coolant temperature</td>
						<td nowrap id="obdCoolantTemperature"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;Engine RPM</td>
						<td nowrap id="obdRPM"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;Intake air temperature</td>
						<td nowrap id="obdIntakeAirTemperature"></td>
					</tr>
					<tr>
						<td nowrap>&nbsp;&nbsp;Throttle position</td>
						<td nowrap id="obdThrottlePosition"></td>
					</tr>
				</table>
				<br/>
			</center>
			<pre style="display:none">
				<script><? $state = json_decode(shell_exec("php status.php 862118020911019"), true); ?>

					var map = new OpenLayers.Map("mapdiv");
					var layer_osm = new OpenLayers.Layer.OSM();
					var layer_google = new OpenLayers.Layer.Google("Google Satellite", {type: google.maps.MapTypeId.SATELLITE, numZoomLevels: 22});
					var markers = new OpenLayers.Layer.Markers("Markers");
					map.addLayer(layer_osm);
					map.addLayer(markers);
					map.addControl(new OpenLayers.Control.ScaleLine());
					var projection = new OpenLayers.Projection("EPSG:4326");
					var state = eval('(' + '<? print json_encode($state); ?>' + ')');
					var lonlat = new OpenLayers.LonLat(state.gpsLongitude, state.gpsLatitude);
					var transform = lonlat.transform(projection, map.getProjectionObject());
					var marker = new OpenLayers.Marker(transform);
					markers.addMarker(marker);
					map.setCenter(transform, 15);
					function convert_number(state, unit, fixed){
						if(state == null)return("---");
						return("" + state.toFixed(fixed) + " " + unit);
					}
					function convert_string(state, unit){
						if(state == null)return("---");
						return("" + state + " " + unit);
					}
					function refresh(){
						var s = document.getElementById("serial");
						include("status.php?serial=" + s.options[s.selectedIndex].value, function(s){
							state = eval('('+s+')');
							redraw(0);
							setTimeout("refresh()", 2000);
						});
					}
					function redraw(center){
						if(state.date == null){
							document.getElementById("date").value = "---";
						}else{
							var date = new Date(state.date * 1000);
							document.getElementById("date").value = date.toLocaleDateString() + " " + date.toLocaleTimeString(window.navigator.language, {hour12: false});
						}
						lonlat.lon = state.gpsLongitude;
						lonlat.lat = state.gpsLatitude;
						transform = lonlat.transform(projection, map.getProjectionObject());
						marker.lonlat = transform;
						marker.draw(map.getLayerPxFromLonLat(marker.lonlat));
						if(center || (document.getElementById("keepCenter").checked == true))map.setCenter(transform);
						document.getElementById("custom").textContent = convert_string(state.custom, "");
						document.getElementById("firmwareVersion").textContent = convert_string(state.firmwareVersion, "");
						document.getElementById("uptime").textContent = convert_number(state.uptime, "s", 0);
						document.getElementById("ip").textContent = convert_string(state.ip, "");
						document.getElementById("voltage").textContent = convert_number(state.voltage, "V", 1);
						document.getElementById("imei").textContent = convert_string(state.imei, "");
						document.getElementById("imsi").textContent = convert_string(state.imsi, "");
						document.getElementById("gpsSpeed").textContent = convert_number(state.gpsSpeed, "km/h", 1);
						document.getElementById("obdSpeed").textContent = convert_number(state.obdSpeed, "km/h", 0);
						document.getElementById("obdLoad").textContent = convert_number(state.obdLoad, "%", 1);
						document.getElementById("obdCoolantTemperature").textContent = convert_number(state.obdCoolantTemperature, " \xB0" + "C", 0);
						document.getElementById("obdRPM").textContent = convert_number(state.obdRPM, "rpm", 0);
						document.getElementById("obdIntakeAirTemperature").textContent = convert_number(state.obdIntakeAirTemperature, " \xB0" + "C", 0);
						document.getElementById("obdThrottlePosition").textContent = convert_number(state.obdThrottlePosition, "%", 1);
					}
					function set_type(type){
						if(type){
							map.addLayer(layer_osm);
							map.removeLayer(layer_google);
						}else{
							map.addLayer(layer_google);
							map.removeLayer(layer_osm);
						}
					}
					redraw();
					refresh();
				</script>
			</pre>
		</div>
	</body>
</html>
