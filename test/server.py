#!/usr/bin/python

import sys
import time
import math
import simplejson

data = {}
file = "/data/www/po.alkohol.ee/data/" + sys.argv[1] + ".json"
data["serialNumber"] = sys.argv[1]
data["ip"] = sys.argv[2]
data["port"] = sys.argv[3]

while 1:
	j = sys.stdin.readline()
	sys.stdout.write(j)
	sys.stdout.flush()
	p = simplejson.loads(j)
	if type(p) is dict and "command" in p and "data" in p:
		c = p["command"]
		d = p["data"]
		if type(d) is dict:
			if c == "ident":
				for key, value in d.iteritems():
					data[key] = value
			elif c == "data" and "fresh" in p and p["fresh"] == True:
				for key, value in d.iteritems():
					data[key] = value
				f = open(file, "w")
				f.write(simplejson.dumps(data))
				f.write("\n")
				f.close()

