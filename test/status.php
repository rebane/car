<?
	Header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	Header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	Header("Cache-Control: no-store, no-cache, must-revalidate");
	Header("Cache-Control: post-check=0, pre-check=0", false);
	Header("Pragma: no-cache");
	function hexnum($str, $n){
		$len = strlen($str);
		if((($n + 1) * 2) > $len)return(0);
		$byte = pack("H*", substr($str, $n * 2, 2));
		return(ord($byte[0]));
	}
	$serial = null;
	if($_GET != null)$serial = $_GET['serial'];
	if($serial == null)$serial = $argv[1];
	$data = array();
	$indata = json_decode(file_get_contents("data/" . $serial . ".json"), true);
	if($indata["gpsDate"]){
		$data["date"] = $indata["gpsDate"];
	}else if($indata["gsmDate"]){
		$data["date"] = $indata["gsmDate"];
	}else if($indata["netDate"]){
		$data["date"] = $indata["netDate"];
	}
	foreach($indata as $key => $value){
		if($key == "gpsLatitude"){
			$data["gpsLatitude"] = floor($value / 1000000) + (round($value - (floor($value / 1000000) * 1000000)) / 600000);
		}else if($key == "gpsLongitude"){
			$data["gpsLongitude"] = floor($value / 1000000) + (round($value - (floor($value / 1000000) * 1000000)) / 600000);
		}else if($key == "gpsSpeed"){
			$data["gpsSpeed"] = $value * 0.01852; // 1 Knot = 1.852 Kilometers per Hour
		}else if($key == "voltage"){
			$data["voltage"] = $value / 10;
		}else if($key == "firmwareVersion"){
			$data["firmwareVersion"] = "" . floor($value / 256) . "." . sprintf("%02u", $value % 256);
		}else if($key == "iso9141_pid4"){
			$data["obdLoad"] = (hexnum($value, 0) * 100) / 255;
		}else if($key == "iso9141_pid5"){
			$data["obdCoolantTemperature"] = hexnum($value, 0) - 40;
		}else if($key == "iso9141_pid12"){
			$data["obdRPM"] = ((hexnum($value, 0) * 256) + hexnum($value, 1)) / 4;
		}else if($key == "iso9141_pid13"){
			$data["obdSpeed"] = hexnum($value, 0);
		}else if($key == "iso9141_pid15"){
			$data["obdIntakeAirTemperature"] = hexnum($value, 0) - 40;
		}else if($key == "iso9141_pid17"){
			$data["obdThrottlePosition"] = (hexnum($value, 0) * 100) / 255;
		}else{
			$data[$key] = $value;
		}
	}
	print json_encode($data);
?>

