#include "i2c.h"
#include <xc.h>
#include <libpic30.h>
#include "platform.h"
#include "mtimer.h"
#include <stdio.h>

// SDA = RA8, RPI24
// SCL = RB9, RP41  (RB4, RP36)

#define i2c_delay()
// mtimer_sleep(1)

static uint8_t i2c_started;

static void i2c_start();
static void i2c_stop();
static void i2c_write(uint8_t bit);
static uint8_t i2c_read();
static uint8_t i2c_write_byte(uint8_t send_start, uint8_t send_stop, uint8_t byte);
static uint8_t i2c_sda_get();
static void i2c_sda_low();
static uint8_t i2c_scl_get();
static void i2c_scl_low();

void i2c_init(){
	ANSELBbits.ANSB9 = 0;
	i2c_sda_get();
	i2c_scl_get();
	i2c_started = 0;

	TRISBbits.TRISB4 = 0;
	PORTBbits.RB4 = 0;
}

void i2c_test(){
	printf("I2C: %u\n", (unsigned int)i2c_write_byte(1, 1, 0x3B)); // 00111011
}

static void i2c_start(){
	if(i2c_started){
		i2c_sda_get();
		i2c_delay();
		while(i2c_scl_get() == 0);
		i2c_delay();
	}
	if(i2c_sda_get() == 0){
		printf("arb lost 1\n");
	}
	i2c_sda_low();
	i2c_delay();
	i2c_scl_low();
	i2c_started = 1;
}

static void i2c_stop(){
	i2c_sda_low();
	i2c_delay();
	while(i2c_scl_get() == 0);
	i2c_delay();
	if(i2c_sda_get() == 0){
		printf("arb lost 2\n");
	}
	i2c_delay();
	i2c_started = 0;
}

static void i2c_write(uint8_t bit){
	if(bit){
		i2c_sda_get();
	}else{
		i2c_sda_low();
	}
	i2c_delay();
	while(i2c_scl_get() == 0);
	if(bit && (i2c_sda_get() == 0)){
		printf("arb lost 3\n");
	}
	i2c_delay();
	i2c_scl_low();
}

static uint8_t i2c_read(){
	uint8_t bit;
	i2c_sda_get();
	i2c_delay();
	while(i2c_scl_get() == 0);
	bit = i2c_sda_get();
	i2c_delay();
	i2c_scl_low();
	return(bit);
}

static uint8_t i2c_write_byte(uint8_t send_start, uint8_t send_stop, uint8_t byte){
	uint8_t bit, nack;
	if(send_start)i2c_start();
	for(bit = 0; bit < 8; bit++){
		i2c_write(byte & 0x80);
		byte <<= 1;
	}
	nack = i2c_read();
	if(send_stop)i2c_stop();
	return(nack);
}

static uint8_t i2c_sda_get(){
	TRISAbits.TRISA8 = 1;
	mtimer_sleep(5);
	return(PORTAbits.RA8 != 0);
}

static void i2c_sda_low(){
	TRISAbits.TRISA8 = 0;
	PORTAbits.RA8 = 0;
}

static uint8_t i2c_scl_get(){
	TRISBbits.TRISB9 = 1;
	mtimer_sleep(5);
	return(PORTBbits.RB9 != 0);
}

static void i2c_scl_low(){
	TRISBbits.TRISB9 = 0;
	PORTBbits.RB9 = 0;
}

