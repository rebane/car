#ifndef _HEADER_PARSER_H_
#define _HEADER_PARSER_H_

#include <stdint.h>

#define HEADER_MAX_VERSION             3

#define HEADER_TYPE_BOOTLOADER         0x01
#define HEADER_TYPE_FIRMWARE           0x49

#define HEADER_ENDIAN_LITTLE           0
#define HEADER_ENDIAN_BIG              1

#define HEADER_TYPE                    0
#define HEADER_VERSION                 1
#define HEADER_ENDIAN                  2
#define HEADER_OFFSET                  3
#define HEADER_TARGET_PLATFORM         4
#define HEADER_LOAD_ADDRESS            5
#define HEADER_SIZE                    6
#define HEADER_CRC                     7
#define HEADER_NAME                    8
#define HEADER_VERSION_MAJOR           9
#define HEADER_VERSION_MINOR           10
#define HEADER_IEEE_ADDRESS            11
#define HEADER_DEFAULT_IP              12
#define HEADER_BOOTLOADER_TYPE         13

#define HEADER_BOOTLOADER_TYPE_DUMMY   0
#define HEADER_BOOTLOADER_TYPE_TFTP    1

void *header_validate(void *start, void *end);
uint8_t header_crc_ok(void *header);
uint32_t header_crc(void *header);

uint32_t header_get(void *header, uint32_t offset, uint8_t size);
uint8_t header_has(void *header, uint8_t type);
uint32_t header_location(void *header, uint8_t type, uint8_t *size);
uint32_t header_value(void *header, uint8_t type);

void header_set(void *header, uint32_t offset, uint8_t size, uint32_t value);
void header_value_set(void *header, uint8_t type, uint32_t value);

#endif

