#ifndef _BJSON_H_
#define _BJSON_H_

// http://bjson.org/

#include <stdint.h>

#ifndef BJSON_MAX
#define BJSON_MAX 64
#endif

#ifndef bjson_umax_t
#if BJSON_MAX == 64
#define bjson_umax_t uint64_t
#else
#define bjson_umax_t uint32_t
#endif
#endif

#ifndef bjson_max_t
#if BJSON_MAX == 64
#define bjson_max_t int64_t
#else
#define bjson_max_t int32_t
#endif
#endif

#define BJSON_BASE_POSITIVE 4
#define BJSON_BASE_NEGATIVE 8
#define BJSON_BASE_STRING   16
#define BJSON_BASE_BINARY   20
#define BJSON_BASE_ARRAY    32
#define BJSON_BASE_MAP      36

uint8_t bjson_null(void *bjson);
uint8_t bjson_true(void *bjson);
uint8_t bjson_false(void *bjson);
uint8_t bjson_positive(void *bjson, bjson_umax_t value);
uint8_t bjson_negative(void *bjson, bjson_umax_t value);
uint8_t bjson_number(void *bjson, bjson_max_t value);
uint8_t bjson_unsigned(void *bjson, bjson_umax_t value);
uint8_t bjson_signed(void *bjson, bjson_max_t value);
uint8_t bjson_float(void *bjson, float value);
uint8_t bjson_double(void *bjson, double value);
bjson_umax_t bjson_string(void *bjson, const char *string);
bjson_umax_t bjson_binary(void *bjson, const void *data, bjson_umax_t len);
bjson_umax_t bjson_array(void *bjson, bjson_umax_t len);
bjson_umax_t bjson_map(void *bjson, bjson_umax_t len);

uint8_t bjson_value(void *bjson, bjson_umax_t value, uint8_t base);
uint8_t bjson_value8(void *bjson, bjson_umax_t value, uint8_t base);
uint8_t bjson_value16(void *bjson, bjson_umax_t value, uint8_t base);
uint8_t bjson_value32(void *bjson, bjson_umax_t value, uint8_t base);

#if BJSON_MAX == 64
uint8_t bjson_value64(void *bjson, bjson_umax_t value, uint8_t base);
#endif

#endif

