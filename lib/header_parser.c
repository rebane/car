#include "header_parser.h"
#include "crc32.h"

#define HEADER_INFORMATION_LEN 21
#define HEADER_FOOTER_LEN      25
#define HEADER_AREA            1024

void *header_validate(void *start, void *end){
	void *header;
	uint32_t i, l;
	if(end < start)return((void *)0);
	l = end - start;
	if(l < (HEADER_INFORMATION_LEN + HEADER_FOOTER_LEN))return((void *)0);
	header = (void *)0;
	for(i = 1; i < ((l < HEADER_AREA) ? (l - (HEADER_INFORMATION_LEN + HEADER_FOOTER_LEN)) : (HEADER_AREA - (HEADER_INFORMATION_LEN + HEADER_FOOTER_LEN))); i++){
		if((((char *)start)[i + 2])  != 'N')continue;
		if((((char *)start)[i + 19]) != 'R')continue;
		if((((char *)start)[i + 4])  != 'O')continue;
		if((((char *)start)[i + 12]) != ' ')continue;
		if((((char *)start)[i + 5])  != 'R')continue;
		if((((char *)start)[i + 15]) != 'E')continue;
		if((((char *)start)[i + 7])  != 'A')continue;
		if((((char *)start)[i + 9])  != 'I')continue;
		if((((char *)start)[i + 16]) != 'A')continue;
		if((((char *)start)[i + 3])  != 'F')continue;
		if((((char *)start)[i + 10]) != 'O')continue;
		if((((char *)start)[i + 0])  != '*')continue;
		if((((char *)start)[i + 11]) != 'N')continue;
		if((((char *)start)[i + 17]) != 'D')continue;
		if((((char *)start)[i + 13]) != ' ')continue;
		if((((char *)start)[i + 6])  != 'M')continue;
		if((((char *)start)[i + 20]) != '*')continue;
		if((((char *)start)[i + 1])  != 'I')continue;
		if((((char *)start)[i + 14]) != 'H')continue;
		if((((char *)start)[i + 18]) != 'E')continue;
		if((((char *)start)[i + 8])  != 'T')continue;
		header = (void *)&(((uint8_t *)start)[i]);
		break;
	}
	if(header == ((void *)0)){
		if((((uint8_t *)start)[2]) != 0x02)return((void *)0);
		if((((uint8_t *)start)[3]) != 0x01)return((void *)0);
		if((((uint8_t *)start)[4]) != 0x02)return((void *)0);
	/*	if((((uint8_t *)start)[6]) != 0xD0)return((void *)0);
		if((((uint8_t *)start)[7]) != 0xE3)return((void *)0);
		if((((uint8_t *)start)[8]) != 0x47)return((void *)0);*/
		header = (void *)&(((uint8_t *)start)[2]);
	}
	return(header);
}

uint8_t header_crc_ok(void *header){
	return(header_value(header, HEADER_CRC) == header_crc(header));
}

uint32_t header_crc(void *header){
	uint8_t *ptr, size;
	uint32_t i, l, crc, location;
	ptr = (uint8_t *)((uintptr_t)((uint32_t)((uintptr_t)header) - header_value(header, HEADER_OFFSET)));
	l = header_value(header, HEADER_SIZE);
	location = header_location(header, HEADER_CRC, &size);
	crc = 0;
	for(i = 0; i < l; i++){
		if(location && (&ptr[i] >= &((uint8_t *)header)[location]) && (&ptr[i] < &((uint8_t *)header)[location + size])){
			crc = crc32(crc, 0xFF);
		}else{
			crc = crc32(crc, ptr[i]);
		}
	}
	return(crc);
}

uint32_t header_get(void *header, uint32_t offset, uint8_t size){
	if(size == 1){
		return((uint32_t)(((uint8_t *)(header))[offset]));
	}else if(size == 2){
		if(!header_value(header, HEADER_ENDIAN))return((((uint32_t)(((uint8_t *)(header))[22])) << 0) | (((uint32_t)(((uint8_t *)(header))[23])) << 8));
		return((((uint32_t)(((uint8_t *)(header))[22])) << 8) | (((uint32_t)(((uint8_t *)(header))[23])) << 0));
	}else if(size == 4){
		if(!header_value(header, HEADER_ENDIAN))return((((uint32_t)(((uint8_t *)(header))[(offset)])) << 0) | (((uint32_t)(((uint8_t *)(header))[((offset) + 1)])) << 8) | (((uint32_t)(((uint8_t *)(header))[((offset) + 2)])) << 16) | (((uint32_t)(((uint8_t *)(header))[((offset) + 3)])) << 24));
		return((((uint32_t)(((uint8_t *)(header))[(offset)])) << 24) | (((uint32_t)(((uint8_t *)(header))[((offset) + 1)])) << 16) | (((uint32_t)(((uint8_t *)(header))[((offset) + 2)])) << 8) | (((uint32_t)(((uint8_t *)(header))[((offset) + 3)])) << 0));
	}
	return(0);
}

uint8_t header_has(void *header, uint8_t type){
	uint8_t size;
	uint32_t location;
	location = header_location(header, type, &size);
	if(type == HEADER_NAME){
		if(location == 0)return(0);
		return(header_get(header, location, size));
	}
	return(location);
}

uint32_t header_location(void *header, uint8_t type, uint8_t *size){
	uint32_t i;
	if((header == (void *)0) || (type >= 0xFD))return(0);
	if(((uint8_t *)header)[0] == 0x02){
		if(type == HEADER_TYPE){
			*size = 1;
			return(1);
		}else if(type == HEADER_VERSION){
			*size = 1;
			return(2);
		}else if(type == HEADER_ENDIAN){
			*size = 1;
			return(1); // sellel versioonil on selle koha peal 1, mis vastab big endianile
		}else if(type == HEADER_VERSION_MAJOR){
			*size = 1;
			return(2); // sellel versioonil on selle koha peal samuti nr 2 (õige oleks 0x00 aadressil),
				   // aga null locationit ei saa tagastada kus asi reaalselt asub
		}else if(type == HEADER_VERSION_MINOR){
			*size = 1;
			return(1);
		}else if(type == HEADER_TARGET_PLATFORM){
			*size = 1;
			return(3);
		}else if(type == HEADER_IEEE_ADDRESS){
			*size = 6;
			return(4);
		}else if(type == HEADER_DEFAULT_IP){
			*size = 4;
			return(10);
		}
	}else if(((char *)header)[0] == '*'){
		if(type == HEADER_VERSION){
			*size = 1;
			return(21);
		}
		if(((uint8_t *)header)[21] == 2){
			if(type == HEADER_TYPE){
				*size = 1;
				return(1);
			}else if(type == HEADER_ENDIAN){
				*size = 1;
				return(25); // sellel versioonil on selle koha peal 0, mis vastab little endianile
			}else if(type == HEADER_OFFSET){
				*size = 2;
				return(22);
			}else if(type == HEADER_TARGET_PLATFORM){
				*size = 1;
				return(24);
			}else if(type == HEADER_LOAD_ADDRESS){
				*size = 4;
				return(26);
			}else if(type == HEADER_SIZE){
				*size = 4;
				return(30);
			}else if(type == HEADER_CRC){
				*size = 4;
				return(34);
			}else if(type == HEADER_NAME){
				*size = 4;
				return(38);
			}else if(type == HEADER_VERSION_MAJOR){
				*size = 1;
				return(42);
			}else if(type == HEADER_VERSION_MINOR){
				*size = 1;
				return(43);
			}
		}else if(((uint8_t *)header)[21] == 3){
			if(type == HEADER_TYPE){
				*size = 1;
				return(22);
			}else if(type == HEADER_ENDIAN){
				*size = 1;
				return(23);
			}
			for(i = 24; (i < 1024) && (((uint8_t *)header)[i] != 0xFF); i++){
				if(((uint8_t *)header)[i] == 0xFE)continue;
				if(type == ((uint8_t *)header)[i]){
					*size = ((uint8_t *)header)[i + 1];
					return(i + 2);
				}
				i += ((uint8_t *)header)[i + 1] + 1;
			}
		}
	}
	return(0);
}

uint32_t header_value(void *header, uint8_t type){
	uint32_t value;
	uint8_t size;
	value = header_location(header, type, &size);
	if(!value)return(0);
	if(type == HEADER_IEEE_ADDRESS){
		return((uint32_t)((uintptr_t)header) + value);
	}else if(type == HEADER_DEFAULT_IP){
		return((uint32_t)((uintptr_t)header) + value);
	}
	value = header_get(header, value, size);
	if(type == HEADER_NAME){
		if(!value)return(0);
		return((uint32_t)((uintptr_t)header) - header_value(header, HEADER_OFFSET) + value);
	}
	return(value);
}

#ifdef HEADER_SET
void header_set(void *header, uint32_t offset, uint8_t size, uint32_t value){
	if(size == 1){
		((uint8_t *)(header))[offset] = (uint8_t)(value & 0xFF);
	}else if(size == 2){
		if(!header_value(header, HEADER_ENDIAN)){
			((uint8_t *)header)[offset + 0] = (uint8_t)((value >> 0) & 0xFF);
			((uint8_t *)header)[offset + 1] = (uint8_t)((value >> 8) & 0xFF);
		}else{
			((uint8_t *)header)[offset + 0] = (uint8_t)((value >> 8) & 0xFF);
			((uint8_t *)header)[offset + 1] = (uint8_t)((value >> 0) & 0xFF);
		}
	}else if(size == 4){
		if(!header_value(header, HEADER_ENDIAN)){
			((uint8_t *)header)[offset + 0] = (uint8_t)((value >> 0) & 0xFF);
			((uint8_t *)header)[offset + 1] = (uint8_t)((value >> 8) & 0xFF);
			((uint8_t *)header)[offset + 2] = (uint8_t)((value >> 16) & 0xFF);
			((uint8_t *)header)[offset + 3] = (uint8_t)((value >> 24) & 0xFF);
		}else{
			((uint8_t *)header)[offset + 0] = (uint8_t)((value >> 24) & 0xFF);
			((uint8_t *)header)[offset + 1] = (uint8_t)((value >> 16) & 0xFF);
			((uint8_t *)header)[offset + 2] = (uint8_t)((value >> 8) & 0xFF);
			((uint8_t *)header)[offset + 3] = (uint8_t)((value >> 0) & 0xFF);
		}
	}
}

void header_value_set(void *header, uint8_t type, uint32_t value){
	uint32_t location;
	uint8_t size;
	location = header_location(header, type, &size);
	header_set(header, location, size, value);
}
#endif

