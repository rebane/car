#include "bjson.h"

uint8_t bjson_null(void *bjson){
	((uint8_t *)bjson)[0] = 0;
	return(1);
}

uint8_t bjson_true(void *bjson){
	((uint8_t *)bjson)[0] = 3;
	return(1);
}

uint8_t bjson_false(void *bjson){
	((uint8_t *)bjson)[0] = 1;
	return(1);
}

uint8_t bjson_positive(void *bjson, bjson_umax_t value){
	return(bjson_unsigned(bjson, value));
}

uint8_t bjson_negative(void *bjson, bjson_umax_t value){
	return(bjson_value(bjson, value, BJSON_BASE_NEGATIVE));
}

uint8_t bjson_number(void *bjson, bjson_max_t value){
	return(bjson_signed(bjson, value));
}

uint8_t bjson_unsigned(void *bjson, bjson_umax_t value){
	return(bjson_value(bjson, value, BJSON_BASE_POSITIVE));
}

uint8_t bjson_signed(void *bjson, bjson_max_t value){
	if(value < 0)return(bjson_value(bjson, (value * (-1)), BJSON_BASE_NEGATIVE));
	return(bjson_value(bjson, value, BJSON_BASE_POSITIVE));
}

uint8_t bjson_float(void *bjson, float value){
	if(sizeof(float) == 8){
		((uint8_t *)bjson)[0] = 13;
		((uint8_t *)bjson)[1] = ((uint8_t *)&value)[0];
		((uint8_t *)bjson)[2] = ((uint8_t *)&value)[1];
		((uint8_t *)bjson)[3] = ((uint8_t *)&value)[2];
		((uint8_t *)bjson)[4] = ((uint8_t *)&value)[3];
		((uint8_t *)bjson)[5] = ((uint8_t *)&value)[4];
		((uint8_t *)bjson)[6] = ((uint8_t *)&value)[5];
		((uint8_t *)bjson)[7] = ((uint8_t *)&value)[6];
		((uint8_t *)bjson)[8] = ((uint8_t *)&value)[7];
		return(9);
	}else if(sizeof(float) == 4){
		((uint8_t *)bjson)[0] = 12;
		((uint8_t *)bjson)[1] = ((uint8_t *)&value)[0];
		((uint8_t *)bjson)[2] = ((uint8_t *)&value)[1];
		((uint8_t *)bjson)[3] = ((uint8_t *)&value)[2];
		((uint8_t *)bjson)[4] = ((uint8_t *)&value)[3];
		return(5);
	}
	return(0);
}

uint8_t bjson_double(void *bjson, double value){
	if(sizeof(double) == 8){
		((uint8_t *)bjson)[0] = 13;
		((uint8_t *)bjson)[1] = ((uint8_t *)&value)[0];
		((uint8_t *)bjson)[2] = ((uint8_t *)&value)[1];
		((uint8_t *)bjson)[3] = ((uint8_t *)&value)[2];
		((uint8_t *)bjson)[4] = ((uint8_t *)&value)[3];
		((uint8_t *)bjson)[5] = ((uint8_t *)&value)[4];
		((uint8_t *)bjson)[6] = ((uint8_t *)&value)[5];
		((uint8_t *)bjson)[7] = ((uint8_t *)&value)[6];
		((uint8_t *)bjson)[8] = ((uint8_t *)&value)[7];
		return(9);
	}else if(sizeof(double) == 4){
		((uint8_t *)bjson)[0] = 12;
		((uint8_t *)bjson)[1] = ((uint8_t *)&value)[0];
		((uint8_t *)bjson)[2] = ((uint8_t *)&value)[1];
		((uint8_t *)bjson)[3] = ((uint8_t *)&value)[2];
		((uint8_t *)bjson)[4] = ((uint8_t *)&value)[3];
		return(5);
	}
	return(0);
}

bjson_umax_t bjson_string(void *bjson, const char *string){
	bjson_umax_t len, i, j;
	for(len = 0; string[len]; len++);
	if(len == 0){
		((uint8_t *)bjson)[0] = 2;
		return(1);
	}
	j = bjson_value(bjson, len, BJSON_BASE_STRING);
	for(i = 0; i < len; i++){
		((uint8_t *)bjson)[j + i] = ((uint8_t *)string)[i];
	}
	return(len + j);
}

bjson_umax_t bjson_binary(void *bjson, const void *data, bjson_umax_t len){
	bjson_umax_t i, j;
	j = bjson_value(bjson, len, BJSON_BASE_BINARY);
	for(i = 0; i < len; i++){
		((uint8_t *)bjson)[j + i] = ((uint8_t *)data)[i];
	}
	return(len + j);
}

bjson_umax_t bjson_array(void *bjson, bjson_umax_t len){
	return(bjson_value(bjson, len, BJSON_BASE_ARRAY));
}

bjson_umax_t bjson_map(void *bjson, bjson_umax_t len){
	return(bjson_value(bjson, len, BJSON_BASE_MAP));
}

uint8_t bjson_value(void *bjson, bjson_umax_t value, uint8_t base){
#if BJSON_MAX == 64
	if(value > ((65536LLU * 65536LLU) - 1LLU))return(bjson_value64(bjson, value, base));
#endif
	if(value > (65536LLU - 1LLU))return(bjson_value32(bjson, value, base));
	if(value > (256LLU - 1LLU))return(bjson_value16(bjson, value, base));
	return(bjson_value8(bjson, value, base));
}

uint8_t bjson_value8(void *bjson, bjson_umax_t value, uint8_t base){
	((uint8_t *)bjson)[0] = base;
	((uint8_t *)bjson)[1] = ((value >>  0) & 0xFF);
	return(2);
}

uint8_t bjson_value16(void *bjson, bjson_umax_t value, uint8_t base){
	((uint8_t *)bjson)[0] = base + 1;
	((uint8_t *)bjson)[1] = ((value >>  0) & 0xFF);
	((uint8_t *)bjson)[2] = ((value >>  8) & 0xFF);
	return(3);
}

uint8_t bjson_value32(void *bjson, bjson_umax_t value, uint8_t base){
	((uint8_t *)bjson)[0] = base + 2;
	((uint8_t *)bjson)[1] = ((value >>  0) & 0xFF);
	((uint8_t *)bjson)[2] = ((value >>  8) & 0xFF);
	((uint8_t *)bjson)[3] = ((value >> 16) & 0xFF);
	((uint8_t *)bjson)[4] = ((value >> 24) & 0xFF);
	return(5);
}

#if BJSON_MAX == 64
uint8_t bjson_value64(void *bjson, bjson_umax_t value, uint8_t base){
	((uint8_t *)bjson)[0] = base + 3;
	((uint8_t *)bjson)[1] = ((value >>  0) & 0xFF);
	((uint8_t *)bjson)[2] = ((value >>  8) & 0xFF);
	((uint8_t *)bjson)[3] = ((value >> 16) & 0xFF);
	((uint8_t *)bjson)[4] = ((value >> 24) & 0xFF);
	((uint8_t *)bjson)[5] = ((value >> 32) & 0xFF);
	((uint8_t *)bjson)[6] = ((value >> 40) & 0xFF);
	((uint8_t *)bjson)[7] = ((value >> 48) & 0xFF);
	((uint8_t *)bjson)[8] = ((value >> 56) & 0xFF);
	return(9);
}
#endif

