#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

#include <stdint.h>

#define PROTOCOL_COMMAND_IDENT        0
#define PROTOCOL_COMMAND_DATE         1
#define PROTOCOL_COMMAND_DATA         2
#define PROTOCOL_COMMAND_DATA_ACK     3
#define PROTOCOL_COMMAND_FIRMWARE     5
#define PROTOCOL_COMMAND_FIRMWARE_ACK 6
#define PROTOCOL_COMMAND_PING         7
#define PROTOCOL_COMMAND_REBOOT       8

struct protocol_header{
	uint32_t len;
	uint8_t  command;
	uint8_t  status;  // IDENT - serial_len, DATA - fresh
	uint16_t len2;    // IDENT - buffer_len, DATA - remain
	uint64_t uid;     // IDENT - serial,     DATA - uid,   DATA_ACK - uid, DATE - date
}__attribute__((packed));

#define PROTOCOL_HEADER_LEN       (sizeof(struct protocol_header))

#if !defined (__BYTE_ORDER__) || (!defined (__ORDER_LITTLE_ENDIAN__) && !defined (__ORDER_BIG_ENDIAN__))
#error unknown endian
#endif

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define protocol_get_len(header_ptr)                  ((header_ptr)->len)
#define protocol_get_command(header_ptr)              ((header_ptr)->command)
#define protocol_get_status(header_ptr)               ((header_ptr)->status)
#define protocol_get_len2(header_ptr)                 ((header_ptr)->len2)
#define protocol_get_uid(header_ptr)                  ((header_ptr)->uid)
#define protocol_set_len(header_ptr, new_len)         do{ ((header_ptr)->len) = new_len; }while(0)
#define protocol_set_command(header_ptr, new_command) do{ ((header_ptr)->command) = new_command; }while(0)
#define protocol_set_status(header_ptr, new_status)   do{ ((header_ptr)->status) = new_status; }while(0)
#define protocol_set_len2(header_ptr, new_len2)       do{ ((header_ptr)->len2) = new_len2; }while(0)
#define protocol_set_uid(header_ptr, new_uid)         do{ ((header_ptr)->uid) = new_uid; }while(0)
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#error big endian not supported yet
#else
#error unknown endian
#endif

#endif

