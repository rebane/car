<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.1">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="ATT_MISO" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="centerline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="punktiir" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="obd">
<packages>
<package name="OBD">
<pad name="1" x="-14" y="4.3" drill="1.7"/>
<pad name="2" x="-10" y="4.3" drill="1.7"/>
<pad name="3" x="-6" y="4.3" drill="1.7"/>
<pad name="4" x="-2" y="4.3" drill="1.7"/>
<pad name="5" x="2" y="4.3" drill="1.7"/>
<pad name="6" x="6" y="4.3" drill="1.7"/>
<pad name="7" x="10" y="4.3" drill="1.7"/>
<pad name="8" x="14" y="4.3" drill="1.7"/>
<pad name="9" x="-14" y="-4.3" drill="1.7"/>
<pad name="10" x="-10" y="-4.3" drill="1.7"/>
<pad name="11" x="-6" y="-4.3" drill="1.7"/>
<pad name="12" x="-2" y="-4.3" drill="1.7"/>
<pad name="13" x="2" y="-4.3" drill="1.7"/>
<pad name="14" x="6" y="-4.3" drill="1.7"/>
<pad name="15" x="10" y="-4.3" drill="1.7"/>
<pad name="16" x="14" y="-4.3" drill="1.7"/>
</package>
</packages>
<symbols>
<symbol name="OBD">
<pin name="1" x="-10.16" y="12.7" visible="pad" length="short" rot="R270"/>
<pin name="J1850+" x="-7.62" y="12.7" length="short" rot="R270"/>
<pin name="3" x="-5.08" y="12.7" visible="pad" length="short" rot="R270"/>
<pin name="CH_GND" x="-2.54" y="12.7" length="short" rot="R270"/>
<pin name="SIG_GND" x="0" y="12.7" length="short" rot="R270"/>
<pin name="CAN_HI" x="2.54" y="12.7" length="short" rot="R270"/>
<pin name="K" x="5.08" y="12.7" length="short" rot="R270"/>
<pin name="8" x="7.62" y="12.7" visible="pad" length="short" rot="R270"/>
<pin name="9" x="-10.16" y="-15.24" visible="pad" length="short" rot="R90"/>
<pin name="J1850-" x="-7.62" y="-15.24" length="short" rot="R90"/>
<pin name="11" x="-5.08" y="-15.24" visible="pad" length="short" rot="R90"/>
<pin name="12" x="-2.54" y="-15.24" visible="pad" length="short" rot="R90"/>
<pin name="13" x="0" y="-15.24" visible="pad" length="short" rot="R90"/>
<pin name="CAN_LO" x="2.54" y="-15.24" length="short" rot="R90"/>
<pin name="L" x="5.08" y="-15.24" length="short" rot="R90"/>
<pin name="BATTERY" x="7.62" y="-15.24" length="short" rot="R90"/>
<wire x1="-12.7" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="-12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-12.7" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="OBD">
<gates>
<gate name="G$1" symbol="OBD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="OBD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="BATTERY" pad="16"/>
<connect gate="G$1" pin="CAN_HI" pad="6"/>
<connect gate="G$1" pin="CAN_LO" pad="14"/>
<connect gate="G$1" pin="CH_GND" pad="4"/>
<connect gate="G$1" pin="J1850+" pad="2"/>
<connect gate="G$1" pin="J1850-" pad="10"/>
<connect gate="G$1" pin="K" pad="7"/>
<connect gate="G$1" pin="L" pad="15"/>
<connect gate="G$1" pin="SIG_GND" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE_PINHEAD">
<packages>
<package name="PINHEADER-2.54-8X2_N">
<pad name="P$1" x="-8.89" y="1.27" drill="1.016"/>
<pad name="P$3" x="-6.35" y="1.27" drill="1.016"/>
<pad name="P$5" x="-3.81" y="1.27" drill="1.016"/>
<pad name="P$7" x="-1.27" y="1.27" drill="1.016"/>
<wire x1="-9.398" y1="2.54" x2="9.398" y2="2.54" width="0.127" layer="37"/>
<wire x1="-9.398" y1="-2.54" x2="9.398" y2="-2.54" width="0.127" layer="37"/>
<wire x1="-10.16" y1="-1.778" x2="-10.16" y2="1.778" width="0.127" layer="37"/>
<wire x1="10.16" y1="-1.778" x2="10.16" y2="1.778" width="0.127" layer="37"/>
<text x="0" y="3.048" size="1.27" layer="25">&gt;Name</text>
<pad name="P$9" x="1.27" y="1.27" drill="1.016"/>
<pad name="P$11" x="3.81" y="1.27" drill="1.016"/>
<pad name="P$13" x="6.35" y="1.27" drill="1.016"/>
<wire x1="-9.398" y1="2.54" x2="9.398" y2="2.54" width="0.127" layer="38"/>
<wire x1="-9.398" y1="-2.54" x2="9.398" y2="-2.54" width="0.127" layer="38"/>
<wire x1="-10.16" y1="-1.778" x2="-10.16" y2="1.778" width="0.127" layer="38"/>
<wire x1="10.16" y1="-1.778" x2="10.16" y2="1.778" width="0.127" layer="38"/>
<pad name="P$15" x="8.89" y="1.27" drill="1.016"/>
<pad name="P$2" x="-8.89" y="-1.27" drill="1.016"/>
<pad name="P$4" x="-6.35" y="-1.27" drill="1.016"/>
<pad name="P$6" x="-3.81" y="-1.27" drill="1.016"/>
<pad name="P$8" x="-1.27" y="-1.27" drill="1.016"/>
<pad name="P$10" x="1.27" y="-1.27" drill="1.016"/>
<pad name="P$12" x="3.81" y="-1.27" drill="1.016"/>
<pad name="P$14" x="6.35" y="-1.27" drill="1.016"/>
<pad name="P$16" x="8.89" y="-1.27" drill="1.016"/>
</package>
</packages>
<symbols>
<symbol name="PINHEADER-2.54-8X2">
<pin name="P$1" x="5.08" y="10.16" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$3" x="5.08" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-5.08" y1="12.7" x2="2.54" y2="12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="12.7" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="-5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="-5.08" y2="12.7" width="0.254" layer="94"/>
<text x="0" y="13.97" size="1.778" layer="95">&gt;Name</text>
<text x="0.762" y="9.144" size="1.778" layer="95">1</text>
<text x="-4.318" y="9.144" size="1.778" layer="95">2</text>
<pin name="P$5" x="5.08" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$7" x="5.08" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="0.762" y="6.604" size="1.778" layer="95">3</text>
<text x="-4.318" y="6.604" size="1.778" layer="95">4</text>
<pin name="P$9" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$11" x="5.08" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="0.762" y="4.064" size="1.778" layer="95">5</text>
<text x="-4.318" y="4.064" size="1.778" layer="95">6</text>
<pin name="P$13" x="5.08" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="0.762" y="1.524" size="1.778" layer="95">7</text>
<pin name="P$15" x="5.08" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-4.318" y="1.524" size="1.778" layer="95">8</text>
<text x="0.762" y="-1.016" size="1.778" layer="95">9</text>
<text x="-4.572" y="-1.016" size="1.778" layer="95">10</text>
<pin name="P$2" x="-7.62" y="10.16" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$4" x="-7.62" y="7.62" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$6" x="-7.62" y="5.08" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$8" x="-7.62" y="2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$10" x="-7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$12" x="-7.62" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$14" x="-7.62" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$16" x="-7.62" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-0.254" y="-3.556" size="1.778" layer="95">11</text>
<text x="-4.572" y="-3.556" size="1.778" layer="95">12</text>
<text x="-0.254" y="-6.096" size="1.778" layer="95">13</text>
<text x="-4.572" y="-6.096" size="1.778" layer="95">14</text>
<text x="-0.254" y="-8.636" size="1.778" layer="95">15</text>
<text x="-4.572" y="-8.636" size="1.778" layer="95">16</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHEADER-2.54-8X2">
<gates>
<gate name="G$1" symbol="PINHEADER-2.54-8X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PINHEADER-2.54-8X2_N">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$13" pad="P$13"/>
<connect gate="G$1" pin="P$14" pad="P$14"/>
<connect gate="G$1" pin="P$15" pad="P$15"/>
<connect gate="G$1" pin="P$16" pad="P$16"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="obd" deviceset="OBD" device=""/>
<part name="U$2" library="REBANE_PINHEAD" deviceset="PINHEADER-2.54-8X2" device=""/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="96.52" y="96.52"/>
<instance part="U$2" gate="G$1" x="152.4" y="81.28"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="P$1"/>
<wire x1="157.48" y1="91.44" x2="160.02" y2="91.44" width="0.1524" layer="91"/>
<wire x1="160.02" y1="91.44" x2="160.02" y2="99.06" width="0.1524" layer="91"/>
<wire x1="160.02" y1="99.06" x2="142.24" y2="99.06" width="0.1524" layer="91"/>
<wire x1="142.24" y1="99.06" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="P$16"/>
<wire x1="142.24" y1="73.66" x2="144.78" y2="73.66" width="0.1524" layer="91"/>
<wire x1="142.24" y1="73.66" x2="111.76" y2="73.66" width="0.1524" layer="91"/>
<junction x="142.24" y="73.66"/>
<label x="111.76" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="CH_GND"/>
<wire x1="93.98" y1="109.22" x2="93.98" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="SIG_GND"/>
<wire x1="93.98" y1="111.76" x2="93.98" y2="119.38" width="0.1524" layer="91"/>
<wire x1="96.52" y1="109.22" x2="96.52" y2="111.76" width="0.1524" layer="91"/>
<wire x1="96.52" y1="111.76" x2="93.98" y2="111.76" width="0.1524" layer="91"/>
<junction x="93.98" y="111.76"/>
<label x="93.98" y="119.38" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="L" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="P$3"/>
<wire x1="157.48" y1="88.9" x2="162.56" y2="88.9" width="0.1524" layer="91"/>
<wire x1="162.56" y1="88.9" x2="162.56" y2="101.6" width="0.1524" layer="91"/>
<wire x1="162.56" y1="101.6" x2="139.7" y2="101.6" width="0.1524" layer="91"/>
<wire x1="139.7" y1="101.6" x2="139.7" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="P$14"/>
<wire x1="139.7" y1="76.2" x2="144.78" y2="76.2" width="0.1524" layer="91"/>
<wire x1="139.7" y1="76.2" x2="111.76" y2="76.2" width="0.1524" layer="91"/>
<junction x="139.7" y="76.2"/>
<label x="111.76" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="L"/>
<wire x1="101.6" y1="81.28" x2="101.6" y2="68.58" width="0.1524" layer="91"/>
<label x="101.6" y="68.58" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="K" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="P$5"/>
<wire x1="157.48" y1="86.36" x2="165.1" y2="86.36" width="0.1524" layer="91"/>
<wire x1="165.1" y1="86.36" x2="165.1" y2="104.14" width="0.1524" layer="91"/>
<wire x1="165.1" y1="104.14" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<wire x1="137.16" y1="104.14" x2="137.16" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="P$12"/>
<wire x1="137.16" y1="78.74" x2="144.78" y2="78.74" width="0.1524" layer="91"/>
<wire x1="137.16" y1="78.74" x2="111.76" y2="78.74" width="0.1524" layer="91"/>
<junction x="137.16" y="78.74"/>
<label x="111.76" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="K"/>
<wire x1="101.6" y1="109.22" x2="101.6" y2="119.38" width="0.1524" layer="91"/>
<label x="101.6" y="119.38" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="CAN_LO" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="P$7"/>
<wire x1="157.48" y1="83.82" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
<wire x1="167.64" y1="83.82" x2="167.64" y2="106.68" width="0.1524" layer="91"/>
<wire x1="167.64" y1="106.68" x2="134.62" y2="106.68" width="0.1524" layer="91"/>
<wire x1="134.62" y1="106.68" x2="134.62" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="P$10"/>
<wire x1="134.62" y1="81.28" x2="144.78" y2="81.28" width="0.1524" layer="91"/>
<wire x1="134.62" y1="81.28" x2="111.76" y2="81.28" width="0.1524" layer="91"/>
<junction x="134.62" y="81.28"/>
<label x="111.76" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="CAN_LO"/>
<wire x1="99.06" y1="81.28" x2="99.06" y2="68.58" width="0.1524" layer="91"/>
<label x="99.06" y="68.58" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="CAN_HI" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="P$9"/>
<wire x1="157.48" y1="81.28" x2="170.18" y2="81.28" width="0.1524" layer="91"/>
<wire x1="170.18" y1="81.28" x2="170.18" y2="109.22" width="0.1524" layer="91"/>
<wire x1="170.18" y1="109.22" x2="132.08" y2="109.22" width="0.1524" layer="91"/>
<wire x1="132.08" y1="109.22" x2="132.08" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="P$8"/>
<wire x1="132.08" y1="83.82" x2="144.78" y2="83.82" width="0.1524" layer="91"/>
<wire x1="132.08" y1="83.82" x2="111.76" y2="83.82" width="0.1524" layer="91"/>
<junction x="132.08" y="83.82"/>
<label x="111.76" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="CAN_HI"/>
<wire x1="99.06" y1="109.22" x2="99.06" y2="119.38" width="0.1524" layer="91"/>
<label x="99.06" y="119.38" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="J1850-" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="P$11"/>
<wire x1="157.48" y1="78.74" x2="172.72" y2="78.74" width="0.1524" layer="91"/>
<wire x1="172.72" y1="78.74" x2="172.72" y2="111.76" width="0.1524" layer="91"/>
<wire x1="172.72" y1="111.76" x2="129.54" y2="111.76" width="0.1524" layer="91"/>
<wire x1="129.54" y1="111.76" x2="129.54" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="P$6"/>
<wire x1="129.54" y1="86.36" x2="144.78" y2="86.36" width="0.1524" layer="91"/>
<wire x1="129.54" y1="86.36" x2="111.76" y2="86.36" width="0.1524" layer="91"/>
<junction x="129.54" y="86.36"/>
<label x="111.76" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="J1850-"/>
<wire x1="88.9" y1="81.28" x2="88.9" y2="68.58" width="0.1524" layer="91"/>
<label x="88.9" y="68.58" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="J1850+" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="P$13"/>
<wire x1="157.48" y1="76.2" x2="175.26" y2="76.2" width="0.1524" layer="91"/>
<wire x1="175.26" y1="76.2" x2="175.26" y2="114.3" width="0.1524" layer="91"/>
<wire x1="175.26" y1="114.3" x2="127" y2="114.3" width="0.1524" layer="91"/>
<wire x1="127" y1="114.3" x2="127" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="P$4"/>
<wire x1="127" y1="88.9" x2="144.78" y2="88.9" width="0.1524" layer="91"/>
<wire x1="127" y1="88.9" x2="111.76" y2="88.9" width="0.1524" layer="91"/>
<junction x="127" y="88.9"/>
<label x="111.76" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="J1850+"/>
<wire x1="88.9" y1="109.22" x2="88.9" y2="119.38" width="0.1524" layer="91"/>
<label x="88.9" y="119.38" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="BATTERY" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="P$15"/>
<wire x1="157.48" y1="73.66" x2="177.8" y2="73.66" width="0.1524" layer="91"/>
<wire x1="177.8" y1="73.66" x2="177.8" y2="116.84" width="0.1524" layer="91"/>
<wire x1="177.8" y1="116.84" x2="124.46" y2="116.84" width="0.1524" layer="91"/>
<wire x1="124.46" y1="116.84" x2="124.46" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="P$2"/>
<wire x1="124.46" y1="91.44" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
<wire x1="124.46" y1="91.44" x2="111.76" y2="91.44" width="0.1524" layer="91"/>
<junction x="124.46" y="91.44"/>
<label x="111.76" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="BATTERY"/>
<wire x1="104.14" y1="81.28" x2="104.14" y2="68.58" width="0.1524" layer="91"/>
<label x="104.14" y="68.58" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
