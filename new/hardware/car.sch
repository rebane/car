<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.1">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="ATT_MISO" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="centerline" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="punktiir" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="REBANE">
<packages>
<package name="0603[1608-METRIC]-DIODE">
<smd name="C" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="A" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<wire x1="-0.8" y1="0.6" x2="0.8" y2="0.6" width="0.127" layer="37"/>
<wire x1="-0.8" y1="-0.6" x2="0.8" y2="-0.6" width="0.127" layer="37"/>
<text x="-1.5" y="0.9" size="1.27" layer="25">&gt;Name</text>
<wire x1="-1.2" y1="0.3" x2="-1.2" y2="-0.3" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0" x2="0.2" y2="0.4" width="0.127" layer="37"/>
<wire x1="0.2" y1="0.4" x2="0.2" y2="-0.4" width="0.127" layer="37"/>
<wire x1="0.2" y1="-0.4" x2="-0.3" y2="0" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0" x2="0.1" y2="0.2" width="0.127" layer="37"/>
<wire x1="0.1" y1="0.2" x2="0.1" y2="-0.2" width="0.127" layer="37"/>
<wire x1="0.1" y1="-0.2" x2="-0.3" y2="0" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0" x2="0" y2="0.1" width="0.127" layer="37"/>
<wire x1="0" y1="0.1" x2="0" y2="0" width="0.127" layer="37"/>
<wire x1="0" y1="0" x2="0" y2="-0.1" width="0.127" layer="37"/>
<wire x1="0" y1="-0.1" x2="-0.3" y2="0" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0" x2="0" y2="0" width="0.127" layer="37"/>
</package>
<package name="LED3MM">
<pad name="A" x="1.27" y="0" drill="0.8128"/>
<pad name="C" x="-1.27" y="0" drill="0.8128"/>
<text x="-1.6" y="0.9" size="1.27" layer="25" ratio="10">&gt;Name</text>
<circle x="0" y="0" radius="2.102378125" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="37"/>
</package>
<package name="LED5MM">
<pad name="A" x="1.27" y="0" drill="0.8128"/>
<pad name="C" x="-1.27" y="0" drill="0.8128"/>
<text x="-1.6" y="0.9" size="1.27" layer="25" ratio="10">&gt;Name</text>
<circle x="0" y="0" radius="3.257296875" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="37"/>
</package>
<package name="LED2MM-SMD">
<smd name="C" x="-1.7" y="0" dx="1.2" dy="1.5" layer="1"/>
<smd name="A" x="1.7" y="0" dx="1.2" dy="1.5" layer="1"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="-1.6" y1="0.9" x2="1.6" y2="0.9" width="0.127" layer="37"/>
<wire x1="-1.6" y1="-0.9" x2="1.6" y2="-0.9" width="0.127" layer="37"/>
<wire x1="-0.7" y1="0.7" x2="-0.7" y2="-0.7" width="0.127" layer="37"/>
<text x="-1.2" y="1.3" size="1.27" layer="37">&gt;Name</text>
</package>
<package name="TACTILE-TH">
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="37"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="37"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="37"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="37"/>
<circle x="0" y="0" radius="2" width="0.2032" layer="37"/>
<pad name="A@1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="A@2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="B@1" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="B@2" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="-1.44" y="3.61" size="1.27" layer="25" ratio="10">&gt;Name</text>
</package>
<package name="TACTILE-FSM4JSMA">
<smd name="1" x="-2.25" y="4.55" dx="1.4" dy="2.1" layer="1"/>
<smd name="2" x="-2.25" y="-4.55" dx="1.4" dy="2.1" layer="1"/>
<smd name="3" x="2.25" y="4.55" dx="1.4" dy="2.1" layer="1"/>
<smd name="4" x="2.25" y="-4.55" dx="1.4" dy="2.1" layer="1"/>
<wire x1="-3.5" y1="3" x2="-3.5" y2="-3" width="0.2032" layer="37"/>
<wire x1="3.5" y1="3" x2="3.5" y2="-3" width="0.2032" layer="37"/>
<circle x="0" y="0" radius="2" width="0.2032" layer="37"/>
<text x="-4.21" y="-2.64" size="1.27" layer="25" ratio="10" rot="R90">&gt;Name</text>
</package>
<package name="TACTILE-FSMSM">
<smd name="1" x="-4.55" y="0" dx="2.11" dy="1.6" layer="1"/>
<smd name="2" x="4.55" y="0" dx="2.11" dy="1.6" layer="1"/>
<wire x1="-3.5" y1="1.8" x2="3.5" y2="1.8" width="0.3048" layer="37"/>
<wire x1="-3.5" y1="-1.8" x2="3.5" y2="-1.8" width="0.3048" layer="37"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<pin name="C" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="-1.016" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.016" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.016" y1="0.381" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.762" x2="2.54" y2="0.381" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.524" x2="1.651" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.651" y1="1.27" x2="2.032" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.032" y1="0.762" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.381" x2="1.651" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.651" y1="0.127" x2="2.032" y2="-0.381" width="0.254" layer="94"/>
<wire x1="2.032" y1="-0.381" x2="2.54" y2="0.381" width="0.254" layer="94"/>
<wire x1="2.032" y1="0.762" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.032" y1="-0.381" x2="1.905" y2="0.127" width="0.254" layer="94"/>
<text x="-1.778" y="-2.54" size="1.778" layer="95" rot="R90">&gt;Name</text>
<text x="2.286" y="2.032" size="1.778" layer="96" rot="R90">&gt;Value</text>
<wire x1="0" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="SWITCH">
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="B" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="95">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>LED</description>
<gates>
<gate name="LED" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]-DIODE">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM" package="LED3MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM" package="LED5MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-SMD" package="LED2MM-SMD">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TACTILE-SWITCH" prefix="SW" uservalue="yes">
<description>TACTILE SWITCH,
FARNELL: 1703878</description>
<gates>
<gate name="SW" symbol="SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="-TH" package="TACTILE-TH">
<connects>
<connect gate="SW" pin="A" pad="A@1 A@2"/>
<connect gate="SW" pin="B" pad="B@1 B@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FSM4JSMA" package="TACTILE-FSM4JSMA">
<connects>
<connect gate="SW" pin="A" pad="1 2"/>
<connect gate="SW" pin="B" pad="3 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FSMSM" package="TACTILE-FSMSM">
<connects>
<connect gate="SW" pin="A" pad="1"/>
<connect gate="SW" pin="B" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="globaltop">
<packages>
<package name="GTOP10PIN">
<smd name="P$1" x="-8" y="6.75" dx="2" dy="1" layer="1"/>
<wire x1="-8" y1="7.5" x2="-8" y2="8" width="0.127" layer="21"/>
<wire x1="-8" y1="8" x2="8" y2="8" width="0.127" layer="21"/>
<wire x1="8" y1="8" x2="8" y2="7.5" width="0.127" layer="21"/>
<smd name="P$2" x="-8" y="5.25" dx="2" dy="1" layer="1"/>
<smd name="P$3" x="-8" y="3.75" dx="2" dy="1" layer="1"/>
<smd name="P$4" x="-8" y="2.25" dx="2" dy="1" layer="1"/>
<smd name="P$5" x="-8" y="0.75" dx="2" dy="1" layer="1"/>
<smd name="P$6" x="-8" y="-0.75" dx="2" dy="1" layer="1"/>
<smd name="P$7" x="-8" y="-2.25" dx="2" dy="1" layer="1"/>
<smd name="P$8" x="-8" y="-3.75" dx="2" dy="1" layer="1"/>
<smd name="P$9" x="-8" y="-5.25" dx="2" dy="1" layer="1"/>
<smd name="P$10" x="-8" y="-6.75" dx="2" dy="1" layer="1"/>
<wire x1="-8" y1="-7.5" x2="-8" y2="-8" width="0.127" layer="21"/>
<wire x1="-8" y1="-8" x2="8" y2="-8" width="0.127" layer="21"/>
<wire x1="8" y1="-8" x2="8" y2="-7.5" width="0.127" layer="21"/>
<smd name="P$11" x="8" y="-6.75" dx="2" dy="1" layer="1"/>
<smd name="P$12" x="8" y="-5.25" dx="2" dy="1" layer="1"/>
<smd name="P$13" x="8" y="-3.75" dx="2" dy="1" layer="1"/>
<smd name="P$14" x="8" y="-2.25" dx="2" dy="1" layer="1"/>
<smd name="P$15" x="8" y="-0.75" dx="2" dy="1" layer="1"/>
<smd name="P$16" x="8" y="0.75" dx="2" dy="1" layer="1"/>
<smd name="P$17" x="8" y="2.25" dx="2" dy="1" layer="1"/>
<smd name="P$18" x="8" y="3.75" dx="2" dy="1" layer="1"/>
<smd name="P$19" x="8" y="5.25" dx="2" dy="1" layer="1"/>
<smd name="P$20" x="8" y="6.75" dx="2" dy="1" layer="1"/>
<circle x="-4.5" y="5.5" radius="0.79056875" width="0.127" layer="21"/>
<hole x="0" y="0.75" drill="3.75"/>
</package>
</packages>
<symbols>
<symbol name="COMMON">
<pin name="VCC" x="-20.32" y="12.7" visible="pin" length="middle" direction="pwr"/>
<pin name="EN/NRESET" x="-20.32" y="10.16" visible="pin" length="middle" direction="in"/>
<pin name="GND@0" x="-20.32" y="7.62" visible="pin" length="middle" direction="pwr"/>
<pin name="VBACKUP" x="-20.32" y="5.08" visible="pin" length="middle" direction="pwr"/>
<pin name="3D-FIX" x="-20.32" y="2.54" visible="pin" length="middle" direction="out"/>
<pin name="1PPS-1" x="-20.32" y="0" visible="pin" length="middle" direction="out"/>
<pin name="GND@1" x="-20.32" y="-5.08" visible="pin" length="middle" direction="pwr"/>
<pin name="TX0" x="-20.32" y="-7.62" visible="pin" length="middle" direction="out"/>
<pin name="RX0" x="-20.32" y="-10.16" visible="pin" length="middle" direction="in"/>
<pin name="ANT" x="20.32" y="-10.16" visible="pin" length="middle" rot="R180"/>
<pin name="GND@2" x="20.32" y="-7.62" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="1PPS-2" x="20.32" y="-5.08" visible="pin" length="middle" direction="out" rot="R180"/>
<pin name="RX1" x="20.32" y="-2.54" visible="pin" length="middle" direction="in" rot="R180"/>
<pin name="TX1" x="20.32" y="0" visible="pin" length="middle" direction="out" rot="R180"/>
<pin name="GND@3" x="20.32" y="10.16" visible="pin" length="middle" direction="pwr" rot="R180"/>
<wire x1="-15.24" y1="15.24" x2="-15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-12.7" x2="15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="-15.24" y2="15.24" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="COMMON">
<gates>
<gate name="G$1" symbol="COMMON" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GTOP10PIN">
<connects>
<connect gate="G$1" pin="1PPS-1" pad="P$6"/>
<connect gate="G$1" pin="1PPS-2" pad="P$13"/>
<connect gate="G$1" pin="3D-FIX" pad="P$5"/>
<connect gate="G$1" pin="ANT" pad="P$11"/>
<connect gate="G$1" pin="EN/NRESET" pad="P$2"/>
<connect gate="G$1" pin="GND@0" pad="P$3"/>
<connect gate="G$1" pin="GND@1" pad="P$8"/>
<connect gate="G$1" pin="GND@2" pad="P$12"/>
<connect gate="G$1" pin="GND@3" pad="P$19"/>
<connect gate="G$1" pin="RX0" pad="P$10"/>
<connect gate="G$1" pin="RX1" pad="P$14"/>
<connect gate="G$1" pin="TX0" pad="P$9"/>
<connect gate="G$1" pin="TX1" pad="P$15"/>
<connect gate="G$1" pin="VBACKUP" pad="P$4"/>
<connect gate="G$1" pin="VCC" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="sim800c">
<packages>
<package name="SIM800C">
<smd name="1" x="-7.95" y="6.05" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="-7.95" y="4.95" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="3" x="-7.95" y="3.85" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="4" x="-7.95" y="2.75" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="5" x="-7.95" y="1.65" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="6" x="-7.95" y="0.55" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="7" x="-7.95" y="-0.55" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="8" x="-7.95" y="-1.65" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="9" x="-7.95" y="-2.75" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="10" x="-7.95" y="-3.85" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="11" x="-7.95" y="-4.95" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="12" x="-7.95" y="-6.05" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="13" x="-4.4" y="-8.9" dx="0.6" dy="1.8" layer="1" rot="R180"/>
<smd name="14" x="-3.3" y="-8.9" dx="0.6" dy="1.8" layer="1" rot="R180"/>
<smd name="15" x="-2.2" y="-8.9" dx="0.6" dy="1.8" layer="1" rot="R180"/>
<smd name="16" x="-1.1" y="-8.9" dx="0.6" dy="1.8" layer="1" rot="R180"/>
<smd name="17" x="0" y="-8.9" dx="0.6" dy="1.8" layer="1" rot="R180"/>
<smd name="18" x="1.1" y="-8.9" dx="0.6" dy="1.8" layer="1" rot="R180"/>
<smd name="19" x="2.2" y="-8.9" dx="0.6" dy="1.8" layer="1" rot="R180"/>
<smd name="20" x="3.3" y="-8.9" dx="0.6" dy="1.8" layer="1" rot="R180"/>
<smd name="21" x="4.4" y="-8.9" dx="0.6" dy="1.8" layer="1" rot="R180"/>
<smd name="22" x="7.95" y="-6.05" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="23" x="7.95" y="-4.95" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="24" x="7.95" y="-3.85" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="25" x="7.95" y="-2.75" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="26" x="7.95" y="-1.65" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="27" x="7.95" y="-0.55" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="28" x="7.95" y="0.55" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="29" x="7.95" y="1.65" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="30" x="7.95" y="2.75" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="31" x="7.95" y="3.85" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="32" x="7.95" y="4.95" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="33" x="7.95" y="6.05" dx="0.6" dy="1.8" layer="1" rot="R270"/>
<smd name="34" x="4.4" y="8.9" dx="0.6" dy="1.8" layer="1"/>
<smd name="35" x="3.3" y="8.9" dx="0.6" dy="1.8" layer="1"/>
<smd name="36" x="2.2" y="8.9" dx="0.6" dy="1.8" layer="1"/>
<smd name="37" x="1.1" y="8.9" dx="0.6" dy="1.8" layer="1"/>
<smd name="38" x="0" y="8.9" dx="0.6" dy="1.8" layer="1"/>
<smd name="39" x="-1.1" y="8.9" dx="0.6" dy="1.8" layer="1"/>
<smd name="40" x="-2.2" y="8.9" dx="0.6" dy="1.8" layer="1"/>
<smd name="41" x="-3.3" y="8.9" dx="0.6" dy="1.8" layer="1"/>
<smd name="42" x="-4.4" y="8.9" dx="0.6" dy="1.8" layer="1"/>
<wire x1="-7.85" y1="6.8" x2="-7.85" y2="8.8" width="0.2" layer="37"/>
<wire x1="-7.85" y1="8.8" x2="-5.1" y2="8.8" width="0.2" layer="37"/>
<circle x="-8.4" y="9.4" radius="0.360553125" width="0.2" layer="37"/>
<wire x1="7.85" y1="-6.8" x2="7.85" y2="-8.8" width="0.2" layer="37"/>
<wire x1="7.85" y1="-8.8" x2="5.1" y2="-8.8" width="0.2" layer="37"/>
<wire x1="-7.85" y1="-6.8" x2="-7.85" y2="-8.8" width="0.2" layer="37"/>
<wire x1="-7.85" y1="-8.8" x2="-5.1" y2="-8.8" width="0.2" layer="37"/>
<wire x1="7.85" y1="6.8" x2="7.85" y2="8.8" width="0.2" layer="37"/>
<wire x1="7.85" y1="8.8" x2="5.1" y2="8.8" width="0.2" layer="37"/>
</package>
</packages>
<symbols>
<symbol name="SIM800C">
<pin name="TXD" x="-22.86" y="15.24" length="short"/>
<pin name="RXD" x="-22.86" y="12.7" length="short"/>
<pin name="RTS" x="-22.86" y="10.16" length="short"/>
<pin name="CTS" x="-22.86" y="7.62" length="short"/>
<pin name="DCD" x="-22.86" y="5.08" length="short"/>
<pin name="DTR" x="-22.86" y="2.54" length="short"/>
<pin name="RI" x="-22.86" y="0" length="short"/>
<pin name="GND@1" x="-22.86" y="-2.54" length="short"/>
<pin name="MICP" x="-22.86" y="-5.08" length="short"/>
<pin name="MICN" x="-22.86" y="-7.62" length="short"/>
<pin name="SPKP" x="-22.86" y="-10.16" length="short"/>
<pin name="SPKN" x="-22.86" y="-12.7" length="short"/>
<pin name="GND@2" x="-10.16" y="-25.4" length="short" rot="R90"/>
<pin name="SIM_DET" x="-7.62" y="-25.4" length="short" rot="R90"/>
<pin name="SIM_DATA" x="-5.08" y="-25.4" length="short" rot="R90"/>
<pin name="SIM_CLK" x="-2.54" y="-25.4" length="short" rot="R90"/>
<pin name="SIM_RST" x="0" y="-25.4" length="short" rot="R90"/>
<pin name="SIM_VDD" x="2.54" y="-25.4" length="short" rot="R90"/>
<pin name="GND@3" x="5.08" y="-25.4" length="short" rot="R90"/>
<pin name="BT_ANT" x="7.62" y="-25.4" length="short" rot="R90"/>
<pin name="GND@4" x="10.16" y="-25.4" length="short" rot="R90"/>
<pin name="TXD2" x="22.86" y="-12.7" length="short" rot="R180"/>
<pin name="RXD2" x="22.86" y="-10.16" length="short" rot="R180"/>
<pin name="USB_VBUS" x="22.86" y="-7.62" length="short" rot="R180"/>
<pin name="USB_DP" x="22.86" y="-5.08" length="short" rot="R180"/>
<pin name="USB_DM" x="22.86" y="-2.54" length="short" rot="R180"/>
<pin name="GND@5" x="22.86" y="0" length="short" rot="R180"/>
<pin name="VRTC" x="22.86" y="2.54" length="short" rot="R180"/>
<pin name="RF_SYNC" x="22.86" y="5.08" length="short" rot="R180"/>
<pin name="GND@6" x="22.86" y="7.62" length="short" rot="R180"/>
<pin name="GND@7" x="22.86" y="10.16" length="short" rot="R180"/>
<pin name="GSM_ANT" x="22.86" y="12.7" length="short" rot="R180"/>
<pin name="GND@8" x="22.86" y="15.24" length="short" rot="R180"/>
<pin name="VBAT@1" x="10.16" y="27.94" length="short" rot="R270"/>
<pin name="VBAT@2" x="7.62" y="27.94" length="short" rot="R270"/>
<pin name="GND@9" x="5.08" y="27.94" length="short" rot="R270"/>
<pin name="GND@10" x="2.54" y="27.94" length="short" rot="R270"/>
<pin name="ADC" x="0" y="27.94" length="short" rot="R270"/>
<pin name="PWRKEY" x="-2.54" y="27.94" length="short" rot="R270"/>
<pin name="VDD_EXT" x="-5.08" y="27.94" length="short" rot="R270"/>
<pin name="NETLIGHT" x="-7.62" y="27.94" length="short" rot="R270"/>
<pin name="STATUS" x="-10.16" y="27.94" length="short" rot="R270"/>
<wire x1="-20.32" y1="25.4" x2="-20.32" y2="20.32" width="0.254" layer="94"/>
<wire x1="-20.32" y1="20.32" x2="-20.32" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-22.86" x2="20.32" y2="-22.86" width="0.254" layer="94"/>
<wire x1="20.32" y1="-22.86" x2="20.32" y2="25.4" width="0.254" layer="94"/>
<wire x1="20.32" y1="25.4" x2="-15.24" y2="25.4" width="0.254" layer="94"/>
<wire x1="-15.24" y1="25.4" x2="-20.32" y2="25.4" width="0.254" layer="94"/>
<wire x1="-20.32" y1="20.32" x2="-15.24" y2="25.4" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SIM800C">
<gates>
<gate name="G$1" symbol="SIM800C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SIM800C">
<connects>
<connect gate="G$1" pin="ADC" pad="38"/>
<connect gate="G$1" pin="BT_ANT" pad="20"/>
<connect gate="G$1" pin="CTS" pad="4"/>
<connect gate="G$1" pin="DCD" pad="5"/>
<connect gate="G$1" pin="DTR" pad="6"/>
<connect gate="G$1" pin="GND@1" pad="8"/>
<connect gate="G$1" pin="GND@10" pad="37"/>
<connect gate="G$1" pin="GND@2" pad="13"/>
<connect gate="G$1" pin="GND@3" pad="19"/>
<connect gate="G$1" pin="GND@4" pad="21"/>
<connect gate="G$1" pin="GND@5" pad="27"/>
<connect gate="G$1" pin="GND@6" pad="30"/>
<connect gate="G$1" pin="GND@7" pad="31"/>
<connect gate="G$1" pin="GND@8" pad="33"/>
<connect gate="G$1" pin="GND@9" pad="36"/>
<connect gate="G$1" pin="GSM_ANT" pad="32"/>
<connect gate="G$1" pin="MICN" pad="10"/>
<connect gate="G$1" pin="MICP" pad="9"/>
<connect gate="G$1" pin="NETLIGHT" pad="41"/>
<connect gate="G$1" pin="PWRKEY" pad="39"/>
<connect gate="G$1" pin="RF_SYNC" pad="29"/>
<connect gate="G$1" pin="RI" pad="7"/>
<connect gate="G$1" pin="RTS" pad="3"/>
<connect gate="G$1" pin="RXD" pad="2"/>
<connect gate="G$1" pin="RXD2" pad="23"/>
<connect gate="G$1" pin="SIM_CLK" pad="16"/>
<connect gate="G$1" pin="SIM_DATA" pad="15"/>
<connect gate="G$1" pin="SIM_DET" pad="14"/>
<connect gate="G$1" pin="SIM_RST" pad="17"/>
<connect gate="G$1" pin="SIM_VDD" pad="18"/>
<connect gate="G$1" pin="SPKN" pad="12"/>
<connect gate="G$1" pin="SPKP" pad="11"/>
<connect gate="G$1" pin="STATUS" pad="42"/>
<connect gate="G$1" pin="TXD" pad="1"/>
<connect gate="G$1" pin="TXD2" pad="22"/>
<connect gate="G$1" pin="USB_DM" pad="26"/>
<connect gate="G$1" pin="USB_DP" pad="25"/>
<connect gate="G$1" pin="USB_VBUS" pad="24"/>
<connect gate="G$1" pin="VBAT@1" pad="34"/>
<connect gate="G$1" pin="VBAT@2" pad="35"/>
<connect gate="G$1" pin="VDD_EXT" pad="40"/>
<connect gate="G$1" pin="VRTC" pad="28"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="esp32">
<packages>
<package name="ESP-WROOM-32">
<smd name="1" x="7.7" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="2" x="6.43" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="3" x="5.16" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="4" x="3.89" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="5" x="2.62" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="6" x="1.35" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="7" x="0.08" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="8" x="-1.19" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="9" x="-2.46" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="10" x="-3.73" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="11" x="-5" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="12" x="-6.27" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="13" x="-7.54" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="14" x="-8.81" y="9" dx="0.9" dy="1.5" layer="1"/>
<smd name="15" x="-10.3" y="5.715" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="16" x="-10.3" y="4.445" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="17" x="-10.3" y="3.175" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="18" x="-10.3" y="1.905" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="19" x="-10.3" y="0.635" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="20" x="-10.3" y="-0.635" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="21" x="-10.3" y="-1.905" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="22" x="-10.3" y="-3.175" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="23" x="-10.3" y="-4.445" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="24" x="-10.3" y="-5.715" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="25" x="-8.81" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="26" x="-7.54" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="27" x="-6.27" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="28" x="-5" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="29" x="-3.73" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="30" x="-2.46" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="31" x="-1.19" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="32" x="0.08" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="33" x="1.35" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="34" x="2.62" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="35" x="3.89" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="36" x="5.16" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="37" x="6.43" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="38" x="7.7" y="-9" dx="0.9" dy="1.5" layer="1"/>
<smd name="GND" x="0" y="0" dx="6" dy="6" layer="1"/>
<wire x1="-10.427" y1="6.35" x2="-10.427" y2="9.127" width="0.2032" layer="21"/>
<wire x1="-10.427" y1="9.127" x2="-9.4" y2="9.127" width="0.2032" layer="21"/>
<wire x1="-10.427" y1="-6.35" x2="-10.427" y2="-9.127" width="0.2032" layer="21"/>
<wire x1="-10.427" y1="-9.127" x2="-9.4" y2="-9.127" width="0.2032" layer="21"/>
<text x="-7.76" y="0" size="0.8128" layer="25" rot="R90" align="center">&gt;Name</text>
<text x="-6.49" y="0" size="0.8128" layer="27" rot="R90" align="center">&gt;Value</text>
<wire x1="8.311" y1="9.127" x2="15.327" y2="9.127" width="0.2032" layer="21"/>
<wire x1="8.311" y1="-9.127" x2="15.327" y2="-9.127" width="0.2032" layer="21"/>
<wire x1="15.327" y1="9.127" x2="15.327" y2="-9.127" width="0.2032" layer="21"/>
<polygon width="0.127" layer="39">
<vertex x="8.89" y="11.43"/>
<vertex x="17.78" y="11.43"/>
<vertex x="17.78" y="-11.43"/>
<vertex x="8.89" y="-11.43"/>
</polygon>
<polygon width="0.127" layer="40">
<vertex x="8.89" y="11.43"/>
<vertex x="17.78" y="11.43"/>
<vertex x="17.78" y="-11.43"/>
<vertex x="8.89" y="-11.43"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="8.89" y="11.43"/>
<vertex x="17.78" y="11.43"/>
<vertex x="17.78" y="-11.43"/>
<vertex x="8.89" y="-11.43"/>
</polygon>
<polygon width="0.127" layer="42">
<vertex x="8.89" y="11.43"/>
<vertex x="17.78" y="11.43"/>
<vertex x="17.78" y="-11.43"/>
<vertex x="8.89" y="-11.43"/>
</polygon>
<polygon width="0.127" layer="43">
<vertex x="8.89" y="11.43"/>
<vertex x="17.78" y="11.43"/>
<vertex x="17.78" y="-11.43"/>
<vertex x="8.89" y="-11.43"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="ESP-WROOM-32">
<pin name="GND@1" x="-17.78" y="2.54" length="short"/>
<pin name="3V3" x="-17.78" y="0" length="short"/>
<pin name="EN" x="-17.78" y="-2.54" length="short"/>
<pin name="SENSOR_VP" x="-17.78" y="-5.08" length="short"/>
<pin name="SENSOR_VN" x="-17.78" y="-7.62" length="short"/>
<pin name="IO34" x="-17.78" y="-10.16" length="short"/>
<pin name="IO35" x="-17.78" y="-12.7" length="short"/>
<pin name="IO32" x="-17.78" y="-15.24" length="short"/>
<pin name="IO33" x="-17.78" y="-17.78" length="short"/>
<pin name="IO25" x="-17.78" y="-20.32" length="short"/>
<pin name="IO26" x="-17.78" y="-22.86" length="short"/>
<pin name="IO27" x="-17.78" y="-25.4" length="short"/>
<pin name="IO14" x="-17.78" y="-27.94" length="short"/>
<pin name="IO12" x="-17.78" y="-30.48" length="short"/>
<pin name="GND@15" x="-5.08" y="-40.64" length="short" rot="R90"/>
<pin name="IO13" x="-2.54" y="-40.64" length="short" rot="R90"/>
<pin name="SD2" x="0" y="-40.64" length="short" rot="R90"/>
<pin name="SD3" x="2.54" y="-40.64" length="short" rot="R90"/>
<pin name="CMD" x="5.08" y="-40.64" length="short" rot="R90"/>
<pin name="GND@38" x="30.48" y="2.54" length="short" rot="R180"/>
<pin name="IO23" x="30.48" y="0" length="short" rot="R180"/>
<pin name="IO22" x="30.48" y="-2.54" length="short" rot="R180"/>
<pin name="TXD0" x="30.48" y="-5.08" length="short" rot="R180"/>
<pin name="RXD0" x="30.48" y="-7.62" length="short" rot="R180"/>
<pin name="IO21" x="30.48" y="-10.16" length="short" rot="R180"/>
<pin name="NC" x="30.48" y="-12.7" length="short" rot="R180"/>
<pin name="IO19" x="30.48" y="-15.24" length="short" rot="R180"/>
<pin name="IO18" x="30.48" y="-17.78" length="short" rot="R180"/>
<pin name="IO5" x="30.48" y="-20.32" length="short" rot="R180"/>
<pin name="IO17" x="30.48" y="-22.86" length="short" rot="R180"/>
<pin name="IO16" x="30.48" y="-25.4" length="short" rot="R180"/>
<pin name="IO4" x="30.48" y="-27.94" length="short" rot="R180"/>
<pin name="IO0" x="30.48" y="-30.48" length="short" rot="R180"/>
<pin name="IO2" x="17.78" y="-40.64" length="short" rot="R90"/>
<pin name="IO15" x="15.24" y="-40.64" length="short" rot="R90"/>
<pin name="SD1" x="12.7" y="-40.64" length="short" rot="R90"/>
<pin name="SDO" x="10.16" y="-40.64" length="short" rot="R90"/>
<pin name="CLK" x="7.62" y="-40.64" length="short" rot="R90"/>
<pin name="GND" x="7.62" y="-17.78" length="short" rot="R90"/>
<wire x1="-15.24" y1="-38.1" x2="-15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="15.24" x2="27.94" y2="15.24" width="0.254" layer="94"/>
<wire x1="27.94" y1="15.24" x2="27.94" y2="-38.1" width="0.254" layer="94"/>
<wire x1="27.94" y1="-38.1" x2="-15.24" y2="-38.1" width="0.254" layer="94"/>
<text x="-12.7" y="18.034" size="1.778" layer="95">&gt;Name</text>
<text x="-12.7" y="12.446" size="1.778" layer="96" align="top-left">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP-WROOM-32" prefix="U">
<gates>
<gate name="G$1" symbol="ESP-WROOM-32" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESP-WROOM-32">
<connects>
<connect gate="G$1" pin="3V3" pad="2"/>
<connect gate="G$1" pin="CLK" pad="20"/>
<connect gate="G$1" pin="CMD" pad="19"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@15" pad="15"/>
<connect gate="G$1" pin="GND@38" pad="38"/>
<connect gate="G$1" pin="IO0" pad="25"/>
<connect gate="G$1" pin="IO12" pad="14"/>
<connect gate="G$1" pin="IO13" pad="16"/>
<connect gate="G$1" pin="IO14" pad="13"/>
<connect gate="G$1" pin="IO15" pad="23"/>
<connect gate="G$1" pin="IO16" pad="27"/>
<connect gate="G$1" pin="IO17" pad="28"/>
<connect gate="G$1" pin="IO18" pad="30"/>
<connect gate="G$1" pin="IO19" pad="31"/>
<connect gate="G$1" pin="IO2" pad="24"/>
<connect gate="G$1" pin="IO21" pad="33"/>
<connect gate="G$1" pin="IO22" pad="36"/>
<connect gate="G$1" pin="IO23" pad="37"/>
<connect gate="G$1" pin="IO25" pad="10"/>
<connect gate="G$1" pin="IO26" pad="11"/>
<connect gate="G$1" pin="IO27" pad="12"/>
<connect gate="G$1" pin="IO32" pad="8"/>
<connect gate="G$1" pin="IO33" pad="9"/>
<connect gate="G$1" pin="IO34" pad="6"/>
<connect gate="G$1" pin="IO35" pad="7"/>
<connect gate="G$1" pin="IO4" pad="26"/>
<connect gate="G$1" pin="IO5" pad="29"/>
<connect gate="G$1" pin="NC" pad="32"/>
<connect gate="G$1" pin="RXD0" pad="34"/>
<connect gate="G$1" pin="SD1" pad="22"/>
<connect gate="G$1" pin="SD2" pad="17"/>
<connect gate="G$1" pin="SD3" pad="18"/>
<connect gate="G$1" pin="SDO" pad="21"/>
<connect gate="G$1" pin="SENSOR_VN" pad="5"/>
<connect gate="G$1" pin="SENSOR_VP" pad="4"/>
<connect gate="G$1" pin="TXD0" pad="35"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="LED1" library="REBANE" deviceset="LED" device="-3MM"/>
<part name="LED2" library="REBANE" deviceset="LED" device="-3MM"/>
<part name="LED3" library="REBANE" deviceset="LED" device="-3MM"/>
<part name="LED4" library="REBANE" deviceset="LED" device="-3MM"/>
<part name="SW1" library="REBANE" deviceset="TACTILE-SWITCH" device="-FSM4JSMA"/>
<part name="U$1" library="globaltop" deviceset="COMMON" device=""/>
<part name="U$2" library="sim800c" deviceset="SIM800C" device=""/>
<part name="U1" library="esp32" deviceset="ESP-WROOM-32" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="LED1" gate="LED" x="15.24" y="58.42"/>
<instance part="LED2" gate="LED" x="22.86" y="58.42"/>
<instance part="LED3" gate="LED" x="30.48" y="58.42"/>
<instance part="LED4" gate="LED" x="38.1" y="58.42"/>
<instance part="SW1" gate="SW" x="45.72" y="58.42" rot="R90"/>
<instance part="U$1" gate="G$1" x="27.94" y="30.48"/>
<instance part="U$2" gate="G$1" x="96.52" y="40.64"/>
<instance part="U1" gate="G$1" x="144.78" y="50.8"/>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
